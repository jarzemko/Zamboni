# WordPress MySQL database migration
#
# Generated: Tuesday 28. April 2015 19:01 UTC
# Hostname: localhost
# Database: `zamboni`
# --------------------------------------------------------

/*!40101 SET NAMES utf8 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Delete any existing table `wp_commentmeta`
#

DROP TABLE IF EXISTS `wp_commentmeta`;


#
# Table structure of table `wp_commentmeta`
#

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_commentmeta`
#

#
# End of data contents of table `wp_commentmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_comments`
#

DROP TABLE IF EXISTS `wp_comments`;


#
# Table structure of table `wp_comments`
#

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_comments`
#
INSERT INTO `wp_comments` ( `comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Pan WordPress', '', 'https://wordpress.org/', '', '2015-04-13 14:45:14', '2015-04-13 14:45:14', 'Cześć, to jest komentarz.\nAby skasować ten komentarz, zaloguj się i wyświetl komentarze tego wpisu. Wtedy zobaczysz opcje edycji oraz kasowania komentarzy.', 0, 'post-trashed', '', '', 0, 0) ;

#
# End of data contents of table `wp_comments`
# --------------------------------------------------------



#
# Delete any existing table `wp_links`
#

DROP TABLE IF EXISTS `wp_links`;


#
# Table structure of table `wp_links`
#

CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_links`
#

#
# End of data contents of table `wp_links`
# --------------------------------------------------------



#
# Delete any existing table `wp_options`
#

DROP TABLE IF EXISTS `wp_options`;


#
# Table structure of table `wp_options`
#

CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1003 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_options`
#
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://zamboni', 'yes'),
(2, 'home', 'http://zamboni', 'yes'),
(3, 'blogname', 'Zamboni', 'yes'),
(4, 'blogdescription', 'Kolejna witryna oparta na WordPressie', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'michal.wklim@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j F Y H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '', 'yes'),
(29, 'gzipcompression', '0', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:2:{i:0;s:41:"wordpress-importer/wordpress-importer.php";i:1;s:32:"wp-sync-db-master/wp-sync-db.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'advanced_edit', '0', 'yes'),
(37, 'comment_max_links', '2', 'yes'),
(38, 'gmt_offset', '', 'yes'),
(39, 'default_email_category', '1', 'yes'),
(40, 'recently_edited', '', 'no'),
(41, 'template', 'zamboni.theme', 'yes'),
(42, 'stylesheet', 'zamboni.theme', 'yes'),
(43, 'comment_whitelist', '1', 'yes'),
(44, 'blacklist_keys', '', 'no'),
(45, 'comment_registration', '0', 'yes'),
(46, 'html_type', 'text/html', 'yes'),
(47, 'use_trackback', '0', 'yes'),
(48, 'default_role', 'subscriber', 'yes'),
(49, 'db_version', '30133', 'yes'),
(50, 'uploads_use_yearmonth_folders', '1', 'yes'),
(51, 'upload_path', '', 'yes'),
(52, 'blog_public', '0', 'yes'),
(53, 'default_link_category', '2', 'yes'),
(54, 'show_on_front', 'posts', 'yes'),
(55, 'tag_base', '', 'yes'),
(56, 'show_avatars', '1', 'yes'),
(57, 'avatar_rating', 'G', 'yes'),
(58, 'upload_url_path', '', 'yes'),
(59, 'thumbnail_size_w', '150', 'yes'),
(60, 'thumbnail_size_h', '150', 'yes'),
(61, 'thumbnail_crop', '1', 'yes'),
(62, 'medium_size_w', '300', 'yes'),
(63, 'medium_size_h', '300', 'yes'),
(64, 'avatar_default', 'mystery', 'yes'),
(65, 'large_size_w', '1024', 'yes'),
(66, 'large_size_h', '1024', 'yes'),
(67, 'image_default_link_type', 'file', 'yes'),
(68, 'image_default_size', '', 'yes'),
(69, 'image_default_align', '', 'yes'),
(70, 'close_comments_for_old_posts', '0', 'yes'),
(71, 'close_comments_days_old', '14', 'yes'),
(72, 'thread_comments', '1', 'yes'),
(73, 'thread_comments_depth', '5', 'yes'),
(74, 'page_comments', '0', 'yes'),
(75, 'comments_per_page', '50', 'yes'),
(76, 'default_comments_page', 'newest', 'yes'),
(77, 'comment_order', 'asc', 'yes'),
(78, 'sticky_posts', 'a:3:{i:0;i:201;i:1;i:239;i:2;i:269;}', 'yes'),
(79, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_text', 'a:0:{}', 'yes'),
(81, 'widget_rss', 'a:0:{}', 'yes'),
(82, 'uninstall_plugins', 'a:0:{}', 'no'),
(83, 'timezone_string', 'Europe/Warsaw', 'yes'),
(84, 'page_for_posts', '0', 'yes'),
(85, 'page_on_front', '0', 'yes'),
(86, 'default_post_format', '0', 'yes'),
(87, 'link_manager_enabled', '0', 'yes'),
(88, 'initial_db_version', '30133', 'yes'),
(89, 'wp_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:62:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:9:"add_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(90, 'WPLANG', 'en_GB', 'yes'),
(91, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(92, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(93, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(97, 'cron', 'a:5:{i:1430275519;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1430286000;a:1:{s:20:"wp_maybe_auto_update";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1430318732;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1430331725;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(101, 'auth_key', '{p7H,@$Is((qt8r`/?Nb,ky9+PnWkIHOm:2{Pn{II{:2HqB(PEND+]~$3 ?YrCxY', 'yes'),
(102, 'auth_salt', '#p5,aK.K{UnPdAeI&S-Keg5WYUJ=].Cx;Z9%2V.z0cE|~.XzPIA@UiI:YLmE=Sc]', 'yes'),
(103, 'logged_in_key', 's1n+Tivl@k)o1.!CUsJ>`q-tC!PR>2sH6f0ic3Q1EChBAv2L-WwAqy1TC{@UEpAb', 'yes') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(104, 'logged_in_salt', '|R$Q!8kxwRY8AkhrhSv~][Cip@UZv=gN?t5 0lS}(8>&XAH]@HB]n>Y_U[tB(0eG', 'yes'),
(105, 'nonce_key', '7|sc&z`#cB0Y`i6tTu64rY,G0Ek.=FF|z,2.mPIwc_2@zQ*R.(S!$!PN/zKu`lF:', 'yes'),
(106, 'nonce_salt', 'ENJ67bT, (E(WRp*~YaU=H}[:li3Cbs=9n*>&=&^JT6R0zCE9]0x0oWy(lJgk}jr', 'yes'),
(114, 'can_compress_scripts', '1', 'yes'),
(134, 'recently_activated', 'a:0:{}', 'yes'),
(141, 'theme_mods_twentyfifteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1428942275;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}}}}', 'yes'),
(142, 'current_theme', '', 'yes'),
(143, 'theme_mods_zamboni.theme', 'a:2:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}}', 'yes'),
(144, 'theme_switched', '', 'yes'),
(353, 'taxonomy_metadata_category_2', 'a:1:{s:14:"category-color";s:7:"#ffffff";}', 'yes'),
(357, 'taxonomy_metadata_category_1', 'a:1:{s:14:"category-color";s:7:"#63e246";}', 'yes'),
(441, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(624, 'taxonomy_metadata_category_37', 'a:1:{s:14:"category-color";s:7:"#7c1818";}', 'yes'),
(685, 'app_options', 'a:5:{s:17:"app_main_sections";a:4:{i:0;s:6:"videos";i:1;s:9:"galleries";i:2;s:9:"instagram";i:3;s:7:"twitter";}s:16:"app_repeat_group";a:2:{i:0;a:3:{s:3:"url";s:19:"http://facebook.com";s:7:"icon_id";s:3:"318";s:4:"icon";s:56:"http://zamboni/wp-content/uploads/2015/04/facebook29.svg";}i:1;a:3:{s:3:"url";s:18:"http://twitter.com";s:7:"icon_id";s:3:"319";s:4:"icon";s:54:"http://zamboni/wp-content/uploads/2015/04/twitter2.svg";}}s:18:"app_twitter_amount";s:1:"0";s:24:"app_default_thumbnail_id";s:2:"13";s:21:"app_default_thumbnail";s:60:"http://zamboni/wp-content/uploads/2015/04/2-vita-vilcina.jpg";}', 'yes'),
(764, 'category_children', 'a:0:{}', 'yes'),
(913, 'wpsdb_error_log', '********************************************\n******  Log date: 2015/04/14 22:59:12 ******\n********************************************\n\nWPSDB Error: There was a problem with the AJAX request, we were expecting a serialized response, instead we received:<br />0\n\nAttempted to connect to: http://zamboni.jarzemko.com/wp-admin/admin-ajax.php\n\nArray\n(\n    [headers] => Array\n        (\n            [date] => Tue, 14 Apr 2015 22:50:07 GMT\n            [server] => Apache\n            [x-powered-by] => PHP/5.3.29\n            [accept-ranges] => none\n            [vary] => Accept-Encoding\n            [content-encoding] => gzip\n            [cache-control] => private, no-cache, no-store, proxy-revalidate, no-transform\n            [pragma] => no-cache\n            [content-length] => 21\n            [connection] => close\n            [content-type] => text/html\n        )\n\n    [body] => 0\n    [response] => Array\n        (\n            [code] => 200\n            [message] => OK\n        )\n\n    [cookies] => Array\n        (\n        )\n\n    [filename] => \n)\n\n\n********************************************\n******  Log date: 2015/04/14 22:59:19 ******\n********************************************\n\nWPSDB Error: There was a problem with the AJAX request, we were expecting a serialized response, instead we received:<br />0\n\nAttempted to connect to: http://zamboni.jarzemko.com/wp-admin/admin-ajax.php\n\nArray\n(\n    [headers] => Array\n        (\n            [date] => Tue, 14 Apr 2015 22:50:14 GMT\n            [server] => Apache\n            [x-powered-by] => PHP/5.3.29\n            [accept-ranges] => none\n            [vary] => Accept-Encoding\n            [content-encoding] => gzip\n            [cache-control] => private, no-cache, no-store, proxy-revalidate, no-transform\n            [pragma] => no-cache\n            [content-length] => 21\n            [connection] => close\n            [content-type] => text/html\n        )\n\n    [body] => 0\n    [response] => Array\n        (\n            [code] => 200\n            [message] => OK\n        )\n\n    [cookies] => Array\n        (\n        )\n\n    [filename] => \n)\n\n\n********************************************\n******  Log date: 2015/04/14 22:59:21 ******\n********************************************\n\nWPSDB Error: There was a problem with the AJAX request, we were expecting a serialized response, instead we received:<br />0\n\nAttempted to connect to: http://zamboni.jarzemko.com/wp-admin/admin-ajax.php\n\nArray\n(\n    [headers] => Array\n        (\n            [date] => Tue, 14 Apr 2015 22:50:15 GMT\n            [server] => Apache\n            [x-powered-by] => PHP/5.3.29\n            [accept-ranges] => none\n            [vary] => Accept-Encoding\n            [content-encoding] => gzip\n            [cache-control] => private, no-cache, no-store, proxy-revalidate, no-transform\n            [pragma] => no-cache\n            [content-length] => 21\n            [connection] => close\n            [content-type] => text/html\n        )\n\n    [body] => 0\n    [response] => Array\n        (\n            [code] => 200\n            [message] => OK\n        )\n\n    [cookies] => Array\n        (\n        )\n\n    [filename] => \n)\n\n\n********************************************\n******  Log date: 2015/04/14 22:59:23 ******\n********************************************\n\nWPSDB Error: There was a problem with the AJAX request, we were expecting a serialized response, instead we received:<br />0\n\nAttempted to connect to: http://zamboni.jarzemko.com/wp-admin/admin-ajax.php\n\nArray\n(\n    [headers] => Array\n        (\n            [date] => Tue, 14 Apr 2015 22:50:17 GMT\n            [server] => Apache\n            [x-powered-by] => PHP/5.3.29\n            [accept-ranges] => none\n            [vary] => Accept-Encoding\n            [content-encoding] => gzip\n            [cache-control] => private, no-cache, no-store, proxy-revalidate, no-transform\n            [pragma] => no-cache\n            [content-length] => 21\n            [connection] => close\n            [content-type] => text/html\n        )\n\n    [body] => 0\n    [response] => Array\n        (\n            [code] => 200\n            [message] => OK\n        )\n\n    [cookies] => Array\n        (\n        )\n\n    [filename] => \n)\n\n\n********************************************\n******  Log date: 2015/04/14 22:59:24 ******\n********************************************\n\nWPSDB Error: There was a problem with the AJAX request, we were expecting a serialized response, instead we received:<br />0\n\nAttempted to connect to: http://zamboni.jarzemko.com/wp-admin/admin-ajax.php\n\nArray\n(\n    [headers] => Array\n        (\n            [date] => Tue, 14 Apr 2015 22:50:18 GMT\n            [server] => Apache\n            [x-powered-by] => PHP/5.3.29\n            [accept-ranges] => none\n            [vary] => Accept-Encoding\n            [content-encoding] => gzip\n            [cache-control] => private, no-cache, no-store, proxy-revalidate, no-transform\n            [pragma] => no-cache\n            [content-length] => 21\n            [connection] => close\n            [content-type] => text/html\n        )\n\n    [body] => 0\n    [response] => Array\n        (\n            [code] => 200\n            [message] => OK\n        )\n\n    [cookies] => Array\n        (\n        )\n\n    [filename] => \n)\n\n\n********************************************\n******  Log date: 2015/04/14 22:59:25 ******\n********************************************\n\nWPSDB Error: There was a problem with the AJAX request, we were expecting a serialized response, instead we received:<br />0\n\nAttempted to connect to: http://zamboni.jarzemko.com/wp-admin/admin-ajax.php\n\nArray\n(\n    [headers] => Array\n        (\n            [date] => Tue, 14 Apr 2015 22:50:20 GMT\n            [server] => Apache\n            [x-powered-by] => PHP/5.3.29\n            [accept-ranges] => none\n            [vary] => Accept-Encoding\n            [content-encoding] => gzip\n            [cache-control] => private, no-cache, no-store, proxy-revalidate, no-transform\n            [pragma] => no-cache\n            [content-length] => 21\n            [connection] => close\n            [content-type] => text/html\n        )\n\n    [body] => 0\n    [response] => Array\n        (\n            [code] => 200\n            [message] => OK\n        )\n\n    [cookies] => Array\n        (\n        )\n\n    [filename] => \n)\n\n\n********************************************\n******  Log date: 2015/04/14 22:59:27 ******\n********************************************\n\nWPSDB Error: There was a problem with the AJAX request, we were expecting a serialized response, instead we received:<br />0\n\nAttempted to connect to: http://zamboni.jarzemko.com/wp-admin/admin-ajax.php\n\nArray\n(\n    [headers] => Array\n        (\n            [date] => Tue, 14 Apr 2015 22:50:21 GMT\n            [server] => Apache\n            [x-powered-by] => PHP/5.3.29\n            [accept-ranges] => none\n            [vary] => Accept-Encoding\n            [content-encoding] => gzip\n            [cache-control] => private, no-cache, no-store, proxy-revalidate, no-transform\n            [pragma] => no-cache\n            [content-length] => 21\n            [connection] => close\n            [content-type] => text/html\n        )\n\n    [body] => 0\n    [response] => Array\n        (\n            [code] => 200\n            [message] => OK\n        )\n\n    [cookies] => Array\n        (\n        )\n\n    [filename] => \n)\n\n\n', 'yes'),
(914, 'wpsdb_settings', 'a:7:{s:11:"max_request";i:2621440;s:3:"key";s:32:"+qOuMx2quESZgl5uAKROoqWf/uqITxlu";s:10:"allow_pull";b:1;s:10:"allow_push";b:1;s:8:"profiles";a:2:{i:0;a:18:{s:13:"save_computer";s:1:"1";s:9:"gzip_file";s:1:"1";s:13:"replace_guids";s:1:"1";s:12:"exclude_spam";s:1:"0";s:19:"keep_active_plugins";s:1:"0";s:13:"create_backup";s:1:"1";s:18:"exclude_post_types";s:1:"0";s:6:"action";s:4:"pull";s:15:"connection_info";s:61:"http://zamboni.jarzemko.com\r\ne0kMFyVADcUT4iKKVV0mU7XkSH/cP6OU";s:11:"replace_old";a:1:{i:1;s:22:"//zamboni.jarzemko.com";}s:11:"replace_new";a:1:{i:1;s:9:"//zamboni";}s:20:"table_migrate_option";s:24:"migrate_only_with_prefix";s:18:"exclude_transients";s:1:"1";s:13:"backup_option";s:23:"backup_only_with_prefix";s:22:"save_migration_profile";s:1:"1";s:29:"save_migration_profile_option";s:1:"0";s:18:"create_new_profile";s:20:"zamboni.jarzemko.com";s:4:"name";s:11:"development";}i:1;a:18:{s:13:"save_computer";s:1:"1";s:9:"gzip_file";s:1:"1";s:13:"replace_guids";s:1:"1";s:12:"exclude_spam";s:1:"0";s:19:"keep_active_plugins";s:1:"0";s:13:"create_backup";s:1:"0";s:18:"exclude_post_types";s:1:"0";s:6:"action";s:8:"savefile";s:15:"connection_info";s:0:"";s:11:"replace_old";a:1:{i:1;s:9:"//zamboni";}s:11:"replace_new";a:1:{i:1;s:13:"//zamboni.com";}s:20:"table_migrate_option";s:24:"migrate_only_with_prefix";s:18:"exclude_transients";s:1:"1";s:13:"backup_option";s:23:"backup_only_with_prefix";s:22:"save_migration_profile";s:1:"1";s:29:"save_migration_profile_option";s:1:"1";s:18:"create_new_profile";s:0:"";s:4:"name";s:10:"production";}}s:10:"verify_ssl";b:0;s:17:"blacklist_plugins";a:0:{}}', 'yes'),
(915, '_transient_random_seed', '200af20c8124c53a66c5c5d64f04d511', 'yes'),
(922, '_site_transient_timeout_browser_837ac6e56c8bf9987e4fbaa5079e156e', '1430687107', 'yes'),
(923, '_site_transient_browser_837ac6e56c8bf9987e4fbaa5079e156e', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"42.0.2311.90";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(978, '_site_transient_timeout_theme_roots', '1430249354', 'yes'),
(979, '_site_transient_theme_roots', 'a:1:{s:13:"zamboni.theme";s:7:"/themes";}', 'yes'),
(984, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:5:{i:0;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:65:"https://downloads.wordpress.org/release/en_GB/wordpress-4.2.1.zip";s:6:"locale";s:5:"en_GB";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:65:"https://downloads.wordpress.org/release/en_GB/wordpress-4.2.1.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.2.1";s:7:"version";s:5:"4.2.1";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";}i:1;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.2.1.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.2.1.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.2.1-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.2.1-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.2.1";s:7:"version";s:5:"4.2.1";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";}i:2;O:8:"stdClass":12:{s:8:"response";s:10:"autoupdate";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.2.1.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.2.1.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.2.1-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.2.1-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.2.1";s:7:"version";s:5:"4.2.1";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";s:13:"support_email";s:26:"updatehelp41@wordpress.org";s:9:"new_files";s:1:"1";}i:3;O:8:"stdClass":12:{s:8:"response";s:10:"autoupdate";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.1.4.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.1.4.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.1.4-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.1.4-new-bundled.zip";s:7:"partial";s:69:"https://downloads.wordpress.org/release/wordpress-4.1.4-partial-1.zip";s:8:"rollback";s:70:"https://downloads.wordpress.org/release/wordpress-4.1.4-rollback-1.zip";}s:7:"current";s:5:"4.1.4";s:7:"version";s:5:"4.1.4";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:5:"4.1.1";s:13:"support_email";s:26:"updatehelp41@wordpress.org";s:9:"new_files";s:0:"";}i:4;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.2.1.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.2.1.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.2.1-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.2.1-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:0:"";s:7:"version";s:0:"";s:11:"php_version";s:3:"4.3";s:13:"mysql_version";s:5:"4.1.2";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1430247566;s:15:"version_checked";s:5:"4.1.1";s:12:"translations";a:1:{i:0;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:5:"pl_PL";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-03-26 16:07:08";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.1.1/pl_PL.zip";s:10:"autoupdate";b:1;}}}', 'yes'),
(985, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1430247567;s:7:"checked";a:1:{s:13:"zamboni.theme";s:3:"1.0";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'yes'),
(986, '_site_transient_update_plugins', 'O:8:"stdClass":4:{s:12:"last_checked";i:1430247567;s:8:"response";a:0:{}s:12:"translations";a:0:{}s:9:"no_update";a:0:{}}', 'yes'),
(987, '_transient_timeout_feed_ac0b00fe65abe10e0c5b588f3ed8c7ca', '1430290899', 'no'),
(988, '_transient_feed_ac0b00fe65abe10e0c5b588f3ed8c7ca', 'a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"\n\n\n";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:49:"\n	\n	\n	\n	\n	\n	\n	\n	\n	\n	\n		\n		\n		\n		\n		\n		\n		\n		\n		\n	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:14:"WordPress News";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:26:"https://wordpress.org/news";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:14:"WordPress News";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:13:"lastBuildDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 27 Apr 2015 18:34:51 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"en-US";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:39:"http://wordpress.org/?v=4.3-alpha-32315";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:10:{i:0;a:6:{s:4:"data";s:48:"\n		\n		\n		\n		\n		\n				\n		\n		\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:32:"WordPress 4.2.1 Security Release";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:51:"https://wordpress.org/news/2015/04/wordpress-4-2-1/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:60:"https://wordpress.org/news/2015/04/wordpress-4-2-1/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 27 Apr 2015 18:34:51 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:3:{i:0;a:5:{s:4:"data";s:8:"Releases";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:8:"Security";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:3:"4.2";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:33:"http://wordpress.org/news/?p=3706";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:366:"WordPress 4.2.1 is now available. This is a critical security release for all previous versions and we strongly encourage you to update your sites immediately. A few hours ago, the WordPress team was made aware of a cross-site scripting vulnerability, which could enable commenters to compromise a site. The vulnerability was discovered by Jouko Pynnönen. [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Gary Pendergast";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:1010:"<p>WordPress 4.2.1 is now available. This is a <strong>critical security release</strong> for all previous versions and we strongly encourage you to update your sites immediately.</p>\n<p>A few hours ago, the WordPress team was made aware of a cross-site scripting vulnerability, which could enable commenters to compromise a site. The vulnerability was discovered by <a href="http://klikki.fi/">Jouko Pynnönen</a>.</p>\n<p>WordPress 4.2.1 has begun to roll out as an automatic background update, for sites that <a href="https://wordpress.org/plugins/background-update-tester/">support</a> those.</p>\n<p>For more information, see the <a href="https://codex.wordpress.org/Version_4.2.1">release notes</a> or consult the <a href="https://core.trac.wordpress.org/log/branches/4.2?rev=32311&amp;stop_rev=32300">list of changes</a>.</p>\n<p><a href="https://wordpress.org/download/">Download WordPress 4.2.1</a> or venture over to <strong>Dashboard → Updates</strong> and simply click &#8220;Update Now&#8221;.</p>\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:56:"https://wordpress.org/news/2015/04/wordpress-4-2-1/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:45:"\n		\n		\n		\n		\n		\n				\n		\n\n		\n		\n				\n			\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:26:"WordPress 4.2 “Powell”";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:42:"https://wordpress.org/news/2015/04/powell/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:51:"https://wordpress.org/news/2015/04/powell/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 23 Apr 2015 18:35:29 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:8:"Releases";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:3:"4.2";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:33:"http://wordpress.org/news/?p=3642";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:343:"Version 4.2 of WordPress, named &#8220;Powell&#8221; in honor of jazz pianist Bud Powell, is available for download or update in your WordPress dashboard. New features in 4.2 help you communicate and share, globally. An easier way to share content Clip it, edit it, publish it. Get familiar with the new and improved Press This. From [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Matt Mullenweg";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:29252:"<p>Version 4.2 of WordPress, named &#8220;Powell&#8221; in honor of jazz pianist <a href="https://en.wikipedia.org/wiki/Bud_Powell">Bud Powell</a>, is available for <a href="https://wordpress.org/download/">download</a> or update in your WordPress dashboard. New features in 4.2 help you communicate and share, globally.</p>\n<div id="v-e9kH4FzP-1" class="video-player"><embed id="v-e9kH4FzP-1-video" src="https://v0.wordpress.com/player.swf?v=1.04&amp;guid=e9kH4FzP&amp;isDynamicSeeking=true" type="application/x-shockwave-flash" width="692" height="388" title="Introducing WordPress 4.2 &quot;Powell&quot;" wmode="direct" seamlesstabbing="true" allowfullscreen="true" allowscriptaccess="always" overstretch="true"></embed></div>\n<hr />\n<h2 style="text-align: center">An easier way to share content</h2>\n<p><img class="alignnone size-full wp-image-3677" src="https://wordpress.org/news/files/2015/04/4.2-press-this-2.jpg" alt="Press This" width="1000" height="832" />Clip it, edit it, publish it. Get familiar with the new and improved Press This. From the Tools menu, add Press This to your browser bookmark bar or your mobile device home screen. Once installed you can share your content with lightning speed. Sharing your favorite videos, images, and content has never been this fast or this easy.</p>\n<hr />\n<h2 style="text-align: center">Extended character support</h2>\n<p><img class="alignnone size-full wp-image-3676" src="https://wordpress.org/news/files/2015/04/4.2-characters.png" alt="Character support for emoji, special characters" width="1000" height="832" />Writing in WordPress, whatever your language, just got better. WordPress 4.2 supports a host of new characters out-of-the-box, including native Chinese, Japanese, and Korean characters, musical and mathematical symbols, and hieroglyphs.</p>\n<p>Don’t use any of those characters? You can still have fun — emoji are now available in WordPress! Get creative and decorate your content with <img src="https://s.w.org/images/core/emoji/72x72/1f499.png" alt="', 'no'),
(989, '_transient_timeout_feed_mod_ac0b00fe65abe10e0c5b588f3ed8c7ca', '1430290899', 'no'),
(990, '_transient_feed_mod_ac0b00fe65abe10e0c5b588f3ed8c7ca', '1430247699', 'no'),
(991, '_transient_timeout_feed_d117b5738fbd35bd8c0391cda1f2b5d9', '1430290901', 'no') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(992, '_transient_feed_d117b5738fbd35bd8c0391cda1f2b5d9', 'a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"\n\n\n";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:61:"\n	\n	\n	\n	\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:16:"WordPress Planet";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:28:"http://planet.wordpress.org/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:2:"en";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:47:"WordPress Planet - http://planet.wordpress.org/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:50:{i:0;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:103:"WPTavern: Redux and Kirki Frameworks Join Forces to Provide Better Support for the WordPress Customizer";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:28:"http://wptavern.com/?p=42937";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:113:"http://wptavern.com/redux-and-kirki-frameworks-join-forces-to-provide-better-support-for-the-wordpress-customizer";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4620:"<p><a href="http://i0.wp.com/wptavern.com/wp-content/uploads/2015/04/redux-kirki.jpg" rel="prettyphoto[42937]"><img src="http://i0.wp.com/wptavern.com/wp-content/uploads/2015/04/redux-kirki.jpg?resize=876%2C407" alt="redux-kirki" class="aligncenter size-full wp-image-42949" /></a></p>\n<p>In light of the WordPress Theme Review team&#8217;s recent decision to <a href="http://wptavern.com/wordpress-org-now-requires-theme-authors-to-use-the-customizer-to-build-theme-options" target="_blank">enforce the use of the native customizer</a> for themes in the official directory, the folks behind the Redux and Kirki frameworks are joining forces to better support developers for the new requirement.</p>\n<p><a href="http://reduxframework.com/" target="_blank">Redux</a>, which is built on the WordPress Settings API, is one of the most widely used options frameworks for themes and plugins, with WordPress.org reporting 90,000+ active installs. It supports a multitude of field types, custom error handling, and custom field and validation types, but is not currently compatible with the Customizer API.</p>\n<p>That&#8217;s where <a href="http://kirki.org/" target="_blank">Kirki</a> is stepping in to offer a framework for <a href="http://wptavern.com/kirki-a-free-plugin-to-style-the-wordpress-customizer-and-add-advanced-controls" target="_blank">advanced controls using the customizer</a>. Kirki, created by <a href="http://aristeides.com/">Aristeides Stathopoulos</a>, makes it easy to style the customizer to be a more natural extension of your theme and add panels and sections with more than 20 different <a href="http://kirki.org/#field-types" target="_blank">field types</a>.</p>\n<p>Both open source frameworks and their developers will be <a href="http://redux.io/" target="_blank">working together</a> to offer &#8220;the most powerful WordPress frameworks under one roof.&#8221; They are currently working on making the data output the same as well as creating a converter API for Redux developers. The eventual goal is that Redux will cover both custom settings panels as well as the customizer, while Kirki will be focused purely on the customizer.</p>\n<p>Redux lead developer <a href="https://twitter.com/simplerain" target="_blank">Dovy Paukstys</a> was one of the most vocal opponents of the decision to make the customizer a requirement for WordPress.org theme options. His position is that it limits developers and cannot provide a complete replacement for the Settings API.</p>\n<p>&#8220;The announcement of April 22, 2015 regarding the requirements for WP.org theme submission bothered me,&#8221; Paukstys said. &#8220;I had a decision to make; work to make Redux work fully in the customizer, or reduce our community.</p>\n<p>&#8220;I then remembered the Kirki project and decided to ping Ari. We discussed the possibility of bringing Kirki into the Redux organization and progressing from there.&#8221; Kirki is joining Redux as part of the team, but it will be maintained as a separate framework.</p>\n<p>&#8220;Kirki will always be light, with a smaller footprint,&#8221; Paukstys said. &#8220;There are no plans to turn Kirki into the Swiss army knife that Redux is. However, Kirki will be modified slightly.&#8221;</p>\n<p>The Redux and Kirki teams plan to share concepts and development time in order to ensure that they can mirror the data output between the two frameworks.</p>\n<p>&#8220;I am also in the process of creating the Kirki API, which will allow Redux devs to take their current config and use it with Kirki, rather than Redux,&#8221; Paukstys said. This will enable developers who have built themes using Redux to easily port their theme options over to Kirki for compatibility with the customizer.</p>\n<p>Eventually, Redux will support both custom options panels using the Settings API and the customizer. In the meantime, Paukstys took the initiative to partner with Kirki to make sure Redux users won&#8217;t be hung out to dry with the new WordPress.org requirements.</p>\n<p>&#8220;Kirki is a great solution for Customizer only themes,&#8221; Paukstys said. &#8220;There&#8217;s room enough on our team for both frameworks. Both serve a unique audience.&#8221;</p>\n<p>As the WordPress Theme Review team seems firmly set on upholding its controversial decision regarding the customizer, Redux and other frameworks have no other choice but to fall in line.</p>\n<p>&#8220;This community is too divided,&#8221; Paukstys said. &#8220;We prefer working together, rather than working apart. We believe greater things will come in the future moving forward together, as a team.&#8221;</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 28 Apr 2015 17:51:30 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:24:"Matt: Who is Steve Jobs?";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:21:"http://ma.tt/?p=45000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:39:"http://ma.tt/2015/04/who-is-steve-jobs/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3898:"<p><a href="http://www.amazon.com/dp/0385347405"><img class="alignright" src="http://i1.wp.com/ecx.images-amazon.com/images/I/51jePfRiSOL.jpg?resize=300%2C459" alt="" /></a>I checked out the new book <a href="http://www.amazon.com/dp/B00N6PCWY8/"><em>Becoming Steve Jobs</em> by Brent Schlender and Rick Tetzeli</a> because there had been some interesting excerpts published to the web, and apparently those closest to Steve didn&#8217;t like <a href="http://www.amazon.com/dp/B004W2UBYW/">the Walter Isaacson book</a>, with Jony Ive saying &#8220;My regard [for Isaacson’s book] couldn’t be any lower.&#8221;</p>\n<p>Along with about a million other people I bought and read the authorized biography, and didn&#8217;t think it portrayed Jobs in a way that made me think any less of him, but there must have been some things in there that someone who knew him closely felt were so off that as a group they decided to coordinate and speak with a new author to set the record straight, as Eddy Cue said of the new <em>Becoming</em> book, &#8220;Well done and first to get it right.&#8221; I will never know who Steve Jobs really was, but it is interesting to triangulate and learn from different takes, especially Isaacson&#8217;s biography that Jobs himself endorsed but might not have read and this new one promoted by his closest friends, colleagues, and family.</p>\n<p>As an independent third party who doesn&#8217;t know any of the characters involved personally, I must say that I felt like I got a much worse impression of Steve Jobs from <em>Becoming</em> than from the authorized biography. It was great to hear the direct voices and anecdotes of so many people close to him that haven&#8217;t spoken much publicly like his wife Laurene &#8212; he was a very private man and his friends respect that. But the parts where Schlender/Tetzeli try to balance things out by acknowledging some of the rougher parts of Steve&#8217;s public life, especially the recent ones around options backdating, anti-poaching agreements, book pricing, (all overblown in my opinion) or even when trying to show his negotiating acumen with suppliers, Disney, or music labels, they make Jobs look like an insensitive jerk, which seems to be the opposite of what everyone involved was intending.</p>\n<p>The direct quotes in the book could not be kinder, and it&#8217;s clear from both books that Jobs was incredibly warm, caring, and thoughtful to those closest to him, but <em>Becoming</em> tries so hard to emphasize that it makes the contrast of some of his public and private actions seem especially callous. The personal anecdotes from the author are the best part: one of the most interesting parts of the book is actually when Jobs calls Schlender to invite him for a walk, as one of the people he reached out to and wanted to speak to before he passed, and Schlender &#8212; not knowing the context &#8212; actually chastises him for cutting off his journalistic access and other trivia, and then blows off the meeting, to his lifelong regret.</p>\n<p>It&#8217;s tragic, and it&#8217;s very <em>human</em>, and that&#8217;s what makes for great stories. No one suggests that Steve Jobs was a saint, nor did he need to be. His legacy is already well-protected both in the incredible results while he was alive, and even more so in what the team he built has accomplished since his passing, both periods which actually amaze and inspire me. <em>Becoming Steve Jobs</em> tries harder and accomplishes less to honor the man. It is worth reading if, like me, you gobble up every book around the technology leaders of the past 40 years and want a different take on a familiar tune, but if you were only to read one book about Jobs, and get the most positive impression of the man and his genius, I&#8217;d recommend <a href="http://www.amazon.com/dp/B004W2UBYW/">Isaacson&#8217;s <em>Steve Jobs</em></a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 28 Apr 2015 16:44:35 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Matt";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:87:"WPTavern: Poll: How Often Do You Read a WordPress Plugin’s Changelog Before Updating?";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:28:"http://wptavern.com/?p=42906";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:92:"http://wptavern.com/poll-how-often-do-you-read-a-wordpress-plugins-changelog-before-updating";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:840:"<p>As <a href="http://wptavern.com/do-wordpress-org-themes-need-a-changelog">the debate</a> on whether or not <a href="https://wordpress.org/themes/">WordPress.org hosted themes</a> should have changelogs continues, one line of thought is that regular users don&#8217;t read them. As a long time user of WordPress, I always read a plugin&#8217;s changelog before updating.</p>\n<p>A <strong>good</strong> changelog tells me what bugs have been fixed, new features that have been added, and security issues that have been addressed. It also gives me a timeline of changes I can refer to for troubleshooting. Let us know how often you read a WordPress plugin&#8217;s changelog before updating by participating in the following poll.</p>\nNote: There is a poll embedded within this post, please visit the site to participate in this post\'s poll.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 27 Apr 2015 20:16:34 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:73:"WPTavern: WordPress 4.2.1 Released to Patch Comment Exploit Vulnerability";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:28:"http://wptavern.com/?p=42873";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:83:"http://wptavern.com/wordpress-4-2-1-released-to-patch-comment-exploit-vulnerability";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2486:"<a href="http://i2.wp.com/wptavern.com/wp-content/uploads/2014/04/security-release.jpg" rel="prettyphoto[42873]"><img src="http://i2.wp.com/wptavern.com/wp-content/uploads/2014/04/security-release.jpg?resize=1024%2C505" alt="photo credit: Will Montague - cc" class="size-full wp-image-20655" /></a>photo credit: <a href="http://www.flickr.com/photos/willmontague/3813295674/">Will Montague</a> &#8211; <a href="http://creativecommons.org/licenses/by-nc/2.0/">cc</a>\n<p>This morning we reported on an <a href="http://wptavern.com/zero-day-xss-vulnerability-in-wordpress-4-2-currently-being-patched" target="_blank">XSS vulnerability in WordPress 4.2</a>, 4.1.2, 4.1.1, and 3.9.3, which allows an attacker to compromise a site via its comments. The security team quickly patched the vulnerability and <a href="https://wordpress.org/news/2015/04/wordpress-4-2-1/" target="_blank">released 4.2.1</a> within hours of being notified.</p>\n<p>WordPress&#8217; official statement on the security issue:</p>\n<blockquote><p>The WordPress team was made aware of a XSS issue a few hours ago that we will release an update for shortly. It is a core issue, but the number of sites vulnerable is much smaller than you may think because the vast majority of WordPress-powered sites run Akismet, which blocks this attack. When the fix is tested and ready in the coming hours WordPress users will receive an auto-update and should be safe and protected even if they don’t use Akismet.</p></blockquote>\n<p>That auto-update is now being rolled out to sites where updates have not been disabled. If you are unsure of whether or not your site can perform automatic background updates, Gary Pendergast linked to the <a href="https://wordpress.org/plugins/background-update-tester/" target="_blank">Background Update Tester</a> plugin in the security release. This is a core-supported plugin that will check your site for background update compatibility and explain any issues.</p>\n<p>Since <a href="https://wordpress.org/plugins/akismet/" target="_blank">Akismet</a> is active on more than a million websites, the number of affected users that were not protected is much smaller than it might have been otherwise.</p>\n<p>WordPress 4.2.1 is a critical security release for a widely publicized vulnerability that you do not want to ignore. Users are advised to update immediately. The background update may already have hit your site. If not, you can update manually by navigating to Dashboard → Updates.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 27 Apr 2015 19:46:07 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:30:"Matt: Cell Phones &amp; Cancer";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:21:"http://ma.tt/?p=44995";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:40:"http://ma.tt/2015/04/cell-phones-cancer/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:833:"<blockquote><p>The ability of radiation to cause cancer is dependent on whether or not the radiation is able to alter chemical bonds. This occurs when electrons involved in bonding in a molecule absorb radiation with enough energy to allow them to escape – this is called <a href="http://en.wikipedia.org/wiki/Ionization" target="_blank">ionization</a>. The thing is, whether or not radiation is ionizing is based solely on its energy, not on its number, and as we saw above, its energy is determined entirely from its frequency.</p></blockquote>\n<p>Cool article on WordPress.com about <a href="http://mitchkirby.com/2015/04/22/why-cell-phones-cant-cause-cancer-but-bananas-can/">Why Cell Phones Can’t Cause Cancer, But Bananas Can</a>, which I read while eating (and finishing) a banana. It covers dielectric heating too.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 27 Apr 2015 16:29:43 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Matt";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:51:"WPTavern: Do WordPress.org Themes Need a Changelog?";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:28:"http://wptavern.com/?p=42838";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:60:"http://wptavern.com/do-wordpress-org-themes-need-a-changelog";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3692:"<a href="http://i0.wp.com/wptavern.com/wp-content/uploads/2015/04/changelog.jpg" rel="prettyphoto[42838]"><img src="http://i0.wp.com/wptavern.com/wp-content/uploads/2015/04/changelog.jpg?resize=1024%2C525" alt="photo credit: time - (license)" class="size-full wp-image-42884" /></a>photo credit: <a href="http://www.flickr.com/photos/68419214@N04/6897782843">time</a> &#8211; <a href="https://creativecommons.org/licenses/by-sa/2.0/">(license)</a>\n<p>Over the weekend, Theme Review Team member Jose Castaneda posted a <a href="https://make.wordpress.org/themes/2015/04/26/changelog-proposal/" target="_blank">proposal to add change logs</a> to themes hosted on WordPress.org. The discussion has been on the table for years, but renewed interest in change logs is surfacing for the upcoming 4.3 and 4.4 release cycles.</p>\n<p>Adding changelogs to themes requires action on two related tickets: a <a href="https://meta.trac.wordpress.org/ticket/45" target="_blank">meta ticket</a> to add support for change logs on WordPress.org and a <a href="https://core.trac.wordpress.org/ticket/22810" target="_blank">core ticket</a> to expose the changelog file to users in the WordPress admin.</p>\n<p>Castaneda&#8217;s proposal requests that the team select a standard format for theme authors to follow in either the readme.txt file or a new changelog.txt file. From there the team would follow the core development release cycle to complete whatever steps necessary to get changelog support added to WordPress.org themes.</p>\n<p>Theme Review Team members are divided on whether or not change logs are beneficial to users, as they already have the ability to detect changes using a .diff file when authors submit updates. Others find change logs to be a more readable addition.</p>\n<p>&#8220;Personally, I find change logs to be incredibly helpful, even when using a .diff,&#8221; Theme Review Team admin Chip Bennett said. &#8220;The changelog is the human-readable summary of changes, that can really help grok the diff changes.&#8221;</p>\n<p>Justin Tadlock isn&#8217;t convinced that WordPress users would benefit from themes including change logs:</p>\n<blockquote><p>Honestly, I don’t see change logs as all that important from a user standpoint. While I don’t have any official stats, I’d wager that the vast majority of users don’t read change logs and, of those who do happen upon one, don’t understand most of what’s actually in the file.</p>\n<p>Change logs are, by and large, a developer tool. It’s a nice-to-have feature. I don’t care one way or another. I never read them. I doubt we&#8217;ll get great change logs from the majority of theme authors. We can’t even manage to get some semantic versioning down or basic inline PHP docs. We’ll probably see a lot of Git commit logs copied/pasted or my personal favorite, “Changed a bunch of stuff. Too busy building awesome s*** to care about tracking changes”.</p></blockquote>\n<p>Active discussion on the topic is taking place on the <a href="https://make.wordpress.org/themes/2015/04/26/changelog-proposal/" target="_blank">make.wordpress.org/themes</a> blog. If the team concludes that change logs are beneficial, the main question to answer is whether or not they should simply take up residence in the readme.txt file, like plugins do, or have their own separate file.</p>\n<p>Ultimately, the issue boils down to whether or not WordPress users read and appreciate changelogs, or if they are more beneficial for developers. As the Theme Review Team is primarily made up of developers, it would be valuable if average users who desire theme change logs could chime in on situations where the file might be helpful.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 27 Apr 2015 15:36:01 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:77:"WPTavern: Zero Day XSS Vulnerability in WordPress 4.2 Currently Being Patched";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:28:"http://wptavern.com/?p=42843";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:87:"http://wptavern.com/zero-day-xss-vulnerability-in-wordpress-4-2-currently-being-patched";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2686:"<p>Klikki Oy is <a href="http://klikki.fi/adv/wordpress2.html" target="_blank">reporting</a> a new comment XSS exploit vulnerability in WordPress 4.2, 4.1.2, 4.1.1, and 3.9.3, which allows an unauthenticated attacker to inject JavaScript into comments.</p>\n<blockquote><p>If triggered by a logged-in administrator, under default settings the attacker can leverage the vulnerability to execute arbitrary code on the server via the plugin and theme editors.</p>\n<p>Alternatively the attacker could change the administrator’s password, create new administrator accounts, or do whatever else the currently logged-in administrator can do on the target system.</p></blockquote>\n<p>This particular vulnerability is similar to one <a href="https://cedricvb.be/post/wordpress-stored-xss-vulnerability-4-1-2/" target="_blank">reported by Cedric Van Bockhaven in 2014</a>, which was patched in the most recent <a href="http://wptavern.com/wordpress-4-1-2-is-a-critical-security-release-immediate-update-recommended" target="_blank">WordPress 4.1.2 security release</a>. That particular vulnerability was related to four-byte characters being inserted into comments, causing premature truncation by MySQL.</p>\n<p>In this instance, an attacker posts an excessively long comment in order to trigger the MySQL TEXT type size limit, which truncates the comment as it is inserted into the database.</p>\n<blockquote><p>The truncation results in malformed HTML generated on the page. The attacker can supply any attributes in the allowed HTML tags, in the same way as with the two recently published stored XSS vulnerabilities affecting the WordPress core.</p>\n<p>In these two cases, the injected JavaScript apparently can&#8217;t be triggered in the administrative Dashboard so these exploits seem to require getting around comment moderation e.g. by posting one harmless comment first.</p></blockquote>\n<p>A patch from the WordPress security team should be forthcoming. At this time the team could not provide an ETA, but in the meantime there are a few things users can do to mitigate the risk.</p>\n<p>&#8220;Your best option is to install Akismet (which has already been configured to block this attack), or disable comments,&#8221; core contributor Gary Pendergast said in response to <a href="https://wordpress.slack.com/archives/core/p1430134328007948" target="_blank">inquiries on the WordPress #core Slack channel</a>. &#8220;JavaScript is blocked by wp_kses(). Akismet blocks this specific attack, which gets around wp_kses()’s protection.&#8221;</p>\n<p>WordPress users can also temporarily disable comments in the meantime until the patch has been issued by the WordPress security team.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 27 Apr 2015 12:10:22 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"Matt: Entanglement";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:21:"http://ma.tt/?p=44985";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:34:"http://ma.tt/2015/04/entanglement/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:243:"<p>If you&#8217;re curious about quantum entanglement (and a type of synesthesia) at all, check out <a href="http://www.npr.org/programs/invisibilia/382451600/entanglement">this week&#8217;s Invisibilia show on NPR called Entanglement</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sun, 26 Apr 2015 15:19:44 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Matt";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:15:"Matt: 100 Books";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:21:"http://ma.tt/?p=44983";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:31:"http://ma.tt/2015/04/100-books/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:413:"<p>You can&#8217;t go wrong with Amazon&#8217;s <a href="http://www.amazon.com/b?ie=UTF8&node=8192263011">100 Books To Read In A Lifetime</a>. I&#8217;ve only read a bit over a dozen of them, and some of those in school when I probably didn&#8217;t appreciate them. I&#8217;ve never had a time in my life when I thought, &#8220;You know, I&#8217;m reading too much.&#8221; It&#8217;s a weekend &#8212; read!</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 25 Apr 2015 23:38:17 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Matt";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:33:"Matt: Atlantic Earth Day Pictures";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:21:"http://ma.tt/?p=44980";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:49:"http://ma.tt/2015/04/atlantic-earth-day-pictures/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:185:"<p>The Atlantic <a href="http://www.theatlantic.com/photo/2015/04/earth-day-45/390864/">has a set of 45 pictures that are both beautiful and shocking</a> to commemorate Earth Day.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 25 Apr 2015 04:41:00 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Matt";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:10;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:79:"WPTavern: Automattic’s Dave Martin Publishes His 5 Step Remote Hiring Process";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:28:"http://wptavern.com/?p=42810";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:86:"http://wptavern.com/automattics-dave-martin-publishes-his-5-step-remote-hiring-process";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3389:"<a href="http://i0.wp.com/wptavern.com/wp-content/uploads/2014/05/automattic-offices.jpg" rel="prettyphoto[42810]"><img class="wp-image-22370 size-full" src="http://i0.wp.com/wptavern.com/wp-content/uploads/2014/05/automattic-offices.jpg?resize=1025%2C478" alt="photo credit: Peter Slutsky" /></a>photo credit: <a href="http://peterslutsky.com/2013/05/14/pics-touring-automattics-new-office/">Peter Slutsky</a>\n<p>If you&#8217;re thinking about applying to work for <a href="http://automattic.com/">Automattic</a>, you might want to <a href="http://davemart.in/2015/04/22/inside-automattics-remote-hiring-process/">read this article first</a>. Dave Martin, Creative Director at Automattic, published an in-depth look behind the scenes of the remote hiring process for the design and growth portion of the company. He explains the five step process in detail which gives future applicants a good idea of what to expect.</p>\n<p>It&#8217;s a fascinating read and I learned that every part of the process has a purpose. For instance, every trial project consists of work that would normally be completed by existing exmployees. Every question asked by Martin in the interview process has a purpose, whether it&#8217;s to glean information or get a feel for how the applicant communicates.</p>\n<p>The one area of the hiring process I&#8217;ve routinely seen scrutinized by those who don&#8217;t make the cut is the lack of specific feedback on why they&#8217;re not a good fit for a position. Dave Clements, who <a href="https://www.davidclements.me/2014/04/01/experience-trial-automattic/">almost made it to the final stage</a> of the hiring process, criticized the lack of detail from Automattic on why he wasn&#8217;t a good fit.</p>\n<blockquote><p>My only criticism of my whole process from start to finish is that I wish they would have gone into more detail into on why I was not a good fit for them. They had been so verbose and open up to that point about any question that I asked of them, but when I asked why they had come to the decision to not move forward, I was given a fairly generic response as they &#8216;couldn’t go into too much detail&#8217;.</p></blockquote>\n<p>Martin tries to do his best to highlight why someone is not a good fit, but the process is not easy and the number one goal is to hire the best people.</p>\n<blockquote><p>If things don’t end up working out, I’ll do my best to highlight why. At this point the applicant has invested quite a bit of time. I try to be as specific as possible as to why they are not going to proceed to a final interview.</p>\n<p>Telling people no is hard, but mistakenly bringing on the wrong people can be much worse. While you want to always be kind, and helpful to all applicants, your primary responsibility when hiring is to ensure that only the best people get hired. That is priority number one.</p></blockquote>\n<p>Whether you&#8217;re a distributed company or someone who&#8217;s interested in working for one, there is plenty to learn from the post. I also encourage you to read this <a href="https://hbr.org/2014/04/the-ceo-of-automattic-on-holding-auditions-to-build-a-strong-team">Harvard Business Review article</a> from 2014 featuring Matt Mullenweg, on holding auditions to build a strong team. If you&#8217;ve gone through the Automattic hiring process, let us know what it&#8217;s like in the comments.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 24 Apr 2015 23:12:35 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:11;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:44:"WPTavern: Story.am Relaunches, Now 100% Free";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:28:"http://wptavern.com/?p=42704";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:52:"http://wptavern.com/story-am-relaunches-now-100-free";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4448:"<p><a href="http://i0.wp.com/wptavern.com/wp-content/uploads/2015/04/story-am.jpg" rel="prettyphoto[42704]"><img src="http://i0.wp.com/wptavern.com/wp-content/uploads/2015/04/story-am.jpg?resize=774%2C337" alt="story-am" class="aligncenter size-full wp-image-42815" /></a></p>\n<p>Nick Haskins launched <a href="https://story.am/" target="_blank">Story.am</a> earlier this year as a hosted storytelling platform that offers all the features of <a href="http://wptavern.com/?s=Aesop+Story+Engine" target="_blank">Aesop Story Engine</a>. Initially, the platform was only available to paying customers, but this week Haskins announced that Story.am is <a href="https://story.am/story-am-re-launch/" target="_blank">now available to everyone for free</a>.</p>\n<p>The platform had not received much feedback in the several months it has been open to customers, so Haskins decided to remove all barriers to account creation.</p>\n<p>&#8220;We really want feedback on <a href="https://lasso.is/" target="_blank">Lasso</a>, our visual web editor on Story.AM,&#8221; he said. &#8220;By making it free, we hope to garner a lot of feedback, even if that feedback isn&#8217;t good. Often times, that type of feedback is the best.&#8221;</p>\n<p>In our <a href="http://wptavern.com/introducing-lasso-a-new-frontend-editor-for-wordpress" target="_blank">recent review of Lasso</a>, Jeff Chandler found that the product wasn&#8217;t quite polished enough for prime time but that it has potential. Haskins is hoping to build a broader network of Lasso users who will offer the feedback he needs to improve the editing experience.</p>\n<p>Immediately following his announcement that Story.am accounts are now available for free, Haskins was averaging one signup a minute.</p>\n<blockquote class="twitter-tweet" width="550"><p>Newsletter out, story.am now averaging one signup a minute for the last few minutes. &#10;&#10;The power of free.</p>\n<p>&mdash; Nick Haskins (@nphaskins) <a href="https://twitter.com/nphaskins/status/591359988309708800">April 23, 2015</a></p></blockquote>\n<p></p>\n<p>&#8220;Since the announcement yesterday evening there have been about 130 signups, so we&#8217;ve calmed down to about 5-7 signups an hour,&#8221; he said.</p>\n<p>Story.am is a multisite installation that was built to be elastic and ready to scale. In the future, Haskins will open up a Pro level that will offer additional features such as the ability to sell story subscriptions with Stripe and use your own domain.</p>\n<p>&#8220;The domain mapping and ability to sell story subscriptions are all in place and ready to go,&#8221; he said. &#8220;But rather than just releasing a Pro level straight away, I&#8217;m interested to see if what I THINK users want, is actually inline with what they REALLY want.&#8221;</p>\n<p>Haskins is taking notes of trends while monitoring the signups to get a better picture of how people plan to use Story.am.</p>\n<p>&#8220;I&#8217;m seeing a lot of what I would describe as &#8216;people who aren&#8217;t necessarily writers but want to tell stories,\'&#8221; he said. &#8220;The domains that are coming across include terms like comic, pastor, fish, school, etc. It&#8217;s quite interesting to see. We are tracking everything in great detail, so as time goes on we&#8217;ll begin to have some solid metrics.&#8221;</p>\n<p>Many Story.am users are using the platform in the education space, a trend which has continued since opening up the site to free accounts. The platform is also open to bloggers, but Haskins is not aiming to compete with WordPress.com.</p>\n<p>&#8220;I&#8217;d like to see folks use stories in their own unique ways, with their own flair, and I think this will be tough to come by on a &#8216;generalized&#8217; network. i.e, one that basically treats everyone as the same,&#8221; he said. &#8220;One thing I don&#8217;t want to purposefully do is compete with WordPress.com. I&#8217;d much rather work with them to bring our ideas and tools to their platform, in some way, shape, or form.&#8221;</p>\n<p>Story.am has no current ETA for launching Pro level features, but Haskins said that he will be gauging the demand and gathering feedback before moving on monetizing the platform. If you&#8217;re curious about how Aesop Story Engine components work with WordPress and want to try the new Lasso frontend editor, it&#8217;s now as easy as signing up for a free account on <a href="https://story.am/">Story.am</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 24 Apr 2015 22:18:11 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:12;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:75:"WPTavern: New Plugin Adds Quick Access to the “Press This” Posting Form";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:28:"http://wptavern.com/?p=42776";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:79:"http://wptavern.com/new-plugin-adds-quick-access-to-the-press-this-posting-form";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5312:"<p><a href="http://i1.wp.com/wptavern.com/wp-content/uploads/2015/04/press-this-new.png" rel="prettyphoto[42776]"><img class="aligncenter size-full wp-image-42649" src="http://i1.wp.com/wptavern.com/wp-content/uploads/2015/04/press-this-new.png?resize=986%2C531" alt="press-this-new" /></a></p>\n<p>One of the highlights of the recent <a href="http://wptavern.com/wordpress-4-2-powell-is-now-available-for-download" target="_blank">WordPress 4.2</a> release is the <a href="http://wptavern.com/pressthis-revamped-with-a-new-user-interface-and-minimalist-design" target="_blank">completely revamped Press This interface</a>. The feature&#8217;s intended purpose is to make it easy to share text, images, and videos from around the web in a quick post, but many users are newly captivated by Press This&#8217; new minimalist design for post creation.</p>\n<p>Once you try Press This for publishing, you may become some so partial to the posting form that you don&#8217;t want to go back to the standard post editor. It includes only the most essential formatting buttons and a button to add media, with all other extraneous selections for post formats, categories, and tags collapsed.</p>\n<p><a href="https://wordpress.org/plugins/press-this-new-post/" target="_blank">Press This New Post</a> is a new plugin, created by <a href="https://profiles.wordpress.org/drewapicture/" target="_blank">Drew Jaynes</a>, that gives you quick access to the Press This posting form from the &#8216;+ New&#8217; drop-down in the toolbar.</p>\n<p><a href="http://i2.wp.com/wptavern.com/wp-content/uploads/2015/04/press-this-new-post.jpg" rel="prettyphoto[42776]"><img class="aligncenter size-full wp-image-42787" src="http://i2.wp.com/wptavern.com/wp-content/uploads/2015/04/press-this-new-post.jpg?resize=888%2C405" alt="press-this-new-post" /></a></p>\n<p>The link takes you to the Press This editor where you can scan a URL or simply start writing a new post.</p>\n<p>In the plugin&#8217;s description, Jaynes refers to the easy access as &#8220;Quick Draft on steroids,&#8221; but it may also become a real substitute for WordPress&#8217; dearly-departed <a href="http://wptavern.com/wordpress-zen-mode-5-compelling-reasons-to-turn-on-distraction-free-writing" target="_blank">zen mode</a>. Many users are distracted by the sliding side menu and the fading of non-essential parts of the editor that was introduced in WordPress 4.1 when the <a href="http://wptavern.com/focus-project-and-session-ui-approved-for-merge-into-wordpress-4-1" target="_blank">Focus Project was merged into core</a>. These animations are guaranteed not to happen to you in Press This mode.</p>\n<p><a href="http://i1.wp.com/wptavern.com/wp-content/uploads/2015/04/pop-tarts.jpg" rel="prettyphoto[42776]"><img class="aligncenter size-full wp-image-42794" src="http://i1.wp.com/wptavern.com/wp-content/uploads/2015/04/pop-tarts.jpg?resize=974%2C534" alt="pop-tarts" /></a></p>\n<p>Jaynes is a regular user of the Press This post editor, which was one of his motivations for creating the quick access plugin.</p>\n<p>&#8220;I&#8217;ve already drafted three blog posts using it instead of the standard editor,&#8221; he said. &#8220;Obviously in most cases I finish up in the standard editor, but I really like the posting interface.</p>\n<p>&#8220;I think if we get to the point where Press This supports other post types and/or even other sites in a multisite network, it could really save a lot of time.&#8221;</p>\n<p>The Press This team is actively working on continuing to iterate the feature along those lines and both <a href="https://twitter.com/michaelarestad" target="_blank">Michael Arestad</a> and <a href="https://twitter.com/stephdau" target="_blank">Stephane Daury</a> have stepped up as core component maintainers.</p>\n<p>&#8220;I know for a fact that they both have future plans for improving and iterating it,&#8221; Jaynes said. &#8220;It’s actually really cool to see a continuation of the passion beyond a feature plugin getting merged.&#8221;</p>\n<p>Michael Arestad recently posted his thoughts on <a href="http://blog.michaelarestad.com/2015/04/23/pressed-it/" target="_blank">the future of Press This</a> and listed a host of features that the team is looking at adding:</p>\n<ul>\n<li>Split button (gonna be awesome)</li>\n<li>Some rearranging of components</li>\n<li>Improved tags UI</li>\n<li>Featured image</li>\n<li>Browser extensions</li>\n<li>Site switching</li>\n<li>Image flow improvements are in the works, which should drastically improve the media experience in both editors</li>\n<li>Improved NUX flow</li>\n</ul>\n<p>With its committed maintainers, the Press This feature is well-positioned to evolve to support more diverse content types, which may attract even more users than the bookmarklet does in the long run. If you&#8217;re still thinking of Press This as just a simple bookmarklet for re-posting content, you may want to revisit it. The feature has the potential to influence future iterations of the standard post editor, which suddenly seems a little cluttered.</p>\n<p>If you enjoy using Press This mode for creating new posts, the <a href="https://wordpress.org/plugins/press-this-new-post/" target="_blank">Press This New Post</a> plugin might be a handy addition to your site. Download it for free on WordPress.org.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 24 Apr 2015 20:06:43 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:13;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:73:"WPTavern: Insight into the Jamaican WordPress Community with Bianca Welds";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:28:"http://wptavern.com/?p=42752";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:83:"http://wptavern.com/insight-into-the-jamaican-wordpress-community-with-bianca-welds";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:6221:"<p><a href="http://i1.wp.com/wptavern.com/wp-content/uploads/2015/04/bianca_edi1.jpg" rel="prettyphoto[42752]"><img class="alignright size-medium wp-image-42760" src="http://i1.wp.com/wptavern.com/wp-content/uploads/2015/04/bianca_edi1.jpg?resize=300%2C226" alt="bianca_edi1" /></a>Last week, I met <a href="http://biancawelds.com/">Bianca Welds</a> who lives in Jamaica. She&#8217;s used WordPress for more than 10 years and has knowledge of the developing tech scene in Jamaica. In this interview, we learn how she discovered WordPress, the Jamaican WordPress community, and if the country will ever host a WordCamp.</p>\n<p><strong>How long have you used WordPress?</strong></p>\n<p>I just celebrated my 10th anniversary. I started using WordPress in 2005 and my first post was on April 2nd, 2005.</p>\n<p><strong>What is your WordPress origin story?</strong></p>\n<a href="http://i1.wp.com/wptavern.com/wp-content/uploads/2015/04/WPProfileOriginStory.png" rel="prettyphoto[42752]"><img class="size-full wp-image-42763" src="http://i1.wp.com/wptavern.com/wp-content/uploads/2015/04/WPProfileOriginStory.png?resize=736%2C227" alt="As seen on the WordPress profile page" /></a>As seen on the WordPress profile page\n<p>I was working at IBM at the time and finally decided I liked the idea of having my own <a href="http://biancawelds.com/">personal website</a>. The more I thought about it, the more I realized I wanted to have a blog as well as a static site. I knew I could build an HTML site myself, so I researched this blogging thing, and I found <a href="https://www.blogger.com/home">Blogspot</a>. Within a day of signing up, I became frustrated at not being able to customize it, so I looked for alternatives. I came across WordPress and fell in love, purchased a hosting account, installed it, and never looked back.</p>\n<p><strong>What is the tech scene like in Jamaica?</strong></p>\n<p>The tech scene is currently developing nicely. There has been a lot of slow foundation growth over the last decade, but in the last few years, we have seen some dramatic acceleration. The tech meetup, <a href="http://kingstonbeta.com/">Kingston Beta</a>, grew to hosting a regional conference called <a href="http://connectimass.com/media-2/community-events/connect-events-networks/caribbean-beta/">Caribbean Beta</a>.</p>\n<p>The Slash Roots Developer Community saw the formation of the <a href="http://slashroots.org/">Slash Roots Foundation</a> which does a lot of work in the Open Data space. It expanded to organize the <a href="http://developingcaribbean.org/">Developing the Caribbean</a> conference, and was instrumental in the formation of <a href="http://codeforthecaribbean.org/">Code for the Caribbean</a>.</p>\n<p>Startup events have been growing with our first Startup Weekend taking place in 2013. The Digital Jam Mobile Application competition was held for three years along with several other initiatives. The first <a href="http://venturecapitaljamaica.com/conference/about">Venture Capital Conference</a> was held in 2013 where the first formal angel investor group, <a href="http://www.firstangelsja.com/">First Angels</a>, was created. <a href="http://www.start-upjamaica.com/en/p/list/57094">StartUpJamaica</a> is our first accelerator and it launched last year with over 200 applications, where 36 teams participated in boot camps and training. This is a small sample of the things that are happening in our space.</p>\n<p><strong>Is there a vibrant WordPress community in Jamaica?</strong></p>\n<p>Vibrant on an individual level perhaps. There is no active WordPress community at present. There are a lot of WordPress sites being built and a lot of WordPress blogs being run, but they are more or less individual efforts with no real communication or collaboration to grow and develop a community.</p>\n<p>I have recently started putting out feelers to see if there is enough interest in starting a regular WordPress meetup. In the last week, I&#8217;ve had interest from about two dozen people.</p>\n<p><strong>To date, there has not been a WordCamp in Jamaica. Do you think there will ever be one and will you help organize it?</strong></p>\n<p>I hope there will be and I definitely want to be a part of it. The first goal though would be to get the meetups going and gather a core community, so that&#8217;s my focus now.</p>\n<p><strong>Have you ever attended a WordCamp? If not, which one will be your first?</strong></p>\n<p>Unfortunately, I have never attended any WordCamps. I am working on changing that in the near future by going to <a href="http://miami.wordcamp.org/2015/">WordCamp Miami</a> which is the nearest one to Jamaica.</p>\n<a href="http://i1.wp.com/wptavern.com/wp-content/uploads/2014/05/WCMiamiFeaturedImage.png" rel="prettyphoto[42752]"><img class="wp-image-22707 size-full" src="http://i1.wp.com/wptavern.com/wp-content/uploads/2014/05/WCMiamiFeaturedImage.png?resize=650%2C200" alt="WordCamp Miami Featured Image" /></a>WordCamp Miami Swag\n<p><strong>What do you like most about WordPress and what do you like the least?</strong></p>\n<p>My favorite thing about WordPress is its flexibility. While it may not be the perfect solution for every challenge, there are few things that cannot be done. My least favorite thing is how much there is to learn to truly take full advantage of its power.</p>\n<p><strong>If you wanted people to know something about Jamaica, what would it be?</strong></p>\n<p>The one thing I always try to share when I am <strong>the Jamaican</strong> in the crowd is that, Jamaica is so much more than beaches, weed and reggae. It definitely has those, but there are so many other aspects to our geography, our culture and our people who outsiders don&#8217;t yet fully grasp. But the world is learning.</p>\n<h2>Take the Jamaican WordPress Survey</h2>\n<p>Welds is trying to figure out the size and composition of the Jamaican WordPress community. Please help her out, especially if you&#8217;re a WordPress user living in Jamaica, by taking this <a href="http://lattitudestudios.com/wordpress-jamaica-survey/">short survey</a>. Information will remain confidential and will help Welds develop a better picture of the size and skill level of her local community.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 24 Apr 2015 17:02:21 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:14;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:104:"WPTavern: WPWeekly Episode 189 – Drew Jaynes on What it’s Like to Lead a WordPress Development Cycle";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:44:"http://wptavern.com?p=42751&preview_id=42751";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:107:"http://wptavern.com/wpweekly-episode-189-drew-jaynes-on-what-its-like-to-lead-a-wordpress-development-cycle";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3186:"<p>On this episode of WordPress Weekly, <a href="http://marcuscouch.com/">Marcus Couch</a> and I were joined by <a href="http://werdswords.com/">Drew Jaynes</a>, web engineer for <a href="http://10up.com/">10up,</a> and <a href="http://wptavern.com/drew-jaynes-to-lead-wordpress-4-2">release lead for WordPress 4.2</a>. Jaynes explains how a release lead is chosen, their responsibilities, and what their role is. Release leads are shepherds who work with multiple teams to keep development on track.</p>\n<p>Jaynes also explained how people can contribute to WordPress core through <a href="https://core.trac.wordpress.org/">Trac</a>. We discussed new features in WordPress 4.2 and what the benefits are to <a href="http://wptavern.com/wordpress-core-team-announces-release-leads-for-wordpress-4-3-and-4-4">selecting release leads ahead of time</a>. Last but not least, Jaynes helped us cover the week&#8217;s news.</p>\n<h2>Stories Discussed:</h2>\n<p><a href="http://wptavern.com/xss-vulnerability-affects-more-than-a-dozen-popular-wordpress-plugins">XSS Vulnerability Affects More Than a Dozen Popular WordPress Plugins</a><br />\n<a href="http://wptavern.com/xss-vulnerability-what-to-do-if-you-buy-or-sell-items-on-themeforest-and-codecanyon">XSS Vulnerability: What to do if You Buy or Sell Items on Themeforest and CodeCanyon</a><br />\n<a href="http://wptavern.com/wordpress-4-1-2-is-a-critical-security-release-immediate-update-recommended">WordPress 4.1.2 is a Critical Security Release, Immediate Update Recommended</a><br />\n<a href="http://wptavern.com/facebook-has-abandoned-its-official-wordpress-plugin">Facebook Has Abandoned Its Official WordPress Plugin</a></p>\n<h2>Plugins Picked By Marcus:</h2>\n<p><a href="https://wordpress.org/plugins/cosmick-star-rating/">Comstick Star Rating</a> conforms to the Google structured data algorithm. It allows you to capture customer reviews with a simple shortcode on any page. By inserting a single function into your theme&#8217;s header or footer, your rating will be displayed in Google search.</p>\n<p><a href="https://wordpress.org/plugins/employee-spotlight/">Employee Spotlight</a> displays employees, team members, founders, or just a single person in a four column circle grid. It comes with two sidebar widgets that display featured and recent employees selected in the editor.</p>\n<p><a href="https://wordpress.org/plugins/web-push-notifications/">Web Push Notifications</a> allows you to send push notifications to visitors who use Safari and Chrome.</p>\n<h2>WPWeekly Meta:</h2>\n<p><strong>Next Episode:</strong> Wednesday, April 29th 9:30 P.M. Eastern</p>\n<p><strong>Subscribe To WPWeekly Via Itunes: </strong><a href="https://itunes.apple.com/us/podcast/wordpress-weekly/id694849738" target="_blank">Click here to subscribe</a></p>\n<p><strong>Subscribe To WPWeekly Via RSS: </strong><a href="http://www.wptavern.com/feed/podcast" target="_blank">Click here to subscribe</a></p>\n<p><strong>Subscribe To WPWeekly Via Stitcher Radio: </strong><a href="http://www.stitcher.com/podcast/wordpress-weekly-podcast?refid=stpr" target="_blank">Click here to subscribe</a></p>\n<p><strong>Listen To Episode #189:</strong><br />\n</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 24 Apr 2015 06:30:08 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:15;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:65:"WPTavern: Why Some Sites Automatically Updated to WordPress 4.1.3";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:28:"http://wptavern.com/?p=42743";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:75:"http://wptavern.com/why-some-sites-automatically-updated-to-wordpress-4-1-3";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1948:"<p>Since <a href="http://wptavern.com/wordpress-4-2-powell-is-now-available-for-download">WordPress 4.2 was released</a>, some users are <a href="https://twitter.com/clindsey/status/591436003853750272">questioning</a> why their sites have automatically updated to WordPress 4.1.3. There&#8217;s no information about the release on the <a href="https://core.trac.wordpress.org/ticket/32051">Make WordPress Core</a> site or the official <a href="https://wordpress.org/news/">WordPress news blog</a>. However, <a href="http://codex.wordpress.org/Version_4.1.3">this Codex article</a> explains what&#8217;s in 4.1.3 and the reason it was released.</p>\n<blockquote><p>Fix database writes for esoteric character sets, broken in the WordPress 4.1.2 security release. Neither UTF-8 nor latin1 were affected. For more information, see <a class="external text" href="https://core.trac.wordpress.org/ticket/32051">ticket #32051</a>.</p></blockquote>\n<p>The ticket contains a lengthy technical discussion of a critical bug and what was done to fix it. In addition to 4.1.3, the patch was merged into the following versions:</p>\n<ul>\n<li>3.7.7</li>\n<li>3.8.7</li>\n<li>3.9.5</li>\n<li>4.0.3</li>\n</ul>\n<p>Since these are point releases, sites running WordPress 3.7 and higher will automatically update unless the server doesn&#8217;t support it or they&#8217;re disabled. If you&#8217;re running an old version of WordPress, I highly encourage you to update to 4.2. Not only does it have <a href="http://wptavern.com/wordpress-4-2-powell-is-now-available-for-download">some nifty new features</a>, but it also fixes <a href="https://core.trac.wordpress.org/query?status=accepted&status=assigned&status=closed&status=new&status=reopened&status=reviewing&type=defect+%28bug%29&version=%21&version=%21trunk&resolution=fixed&milestone=4.2&col=id&col=summary&col=milestone&col=status&col=type&col=owner&col=priority&order=priority" target="_blank">231 defects</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 24 Apr 2015 04:25:35 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jeff Chandler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:16;a:6:{s:4:"data";s:13:"\n	\n	\n	\n	\n	\n	\n";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:50:"Post Status: WordPress 4.2, “Powell”, released";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"https://poststatus.com/?p=12432";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:42:"https://poststatus.com/%e2%9d%a4%ef%b8%8f/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5937:"<p></p>\n<h3>The consistency of WordPress</h3>\n<p>WordPress 4.2,&nbsp;<a href="http://wordpress.org/news/2015/04/powell/">Powell</a>,&nbsp;marks the impressive sixth major version in a row on a four month average development cycle.</p>\n<ul>\n<li>WordPress 3.7: October 24th, 2013</li>\n<li>WordPress 3.8: December 12th, 2013</li>\n<li>WordPress 3.9: April 16th, 2014</li>\n<li>WordPress 4.0: September 4th, 2014</li>\n<li>WordPress 4.1: December 18th, 2014</li>\n<li>WordPress 4.2: April 23rd, 2015</li>\n</ul>\n<p>This streak of consistency in WordPress releases is not accidental. Groundwork was put in place organizationally, technically, and philosophically to help ensure consistent, iterative improvements for WordPress.</p>\n<p>The project has always been quite good at updates, if compared to competition. However, a few releases prior to 3.7 got sidetracked, distracted, or thrown off schedule if held to our own high standard.</p>\n<p>Concepts such as the introduction of <a href="https://make.wordpress.org/core/features-as-plugins/">feature plugins</a> have helped put sanity and routine into the release schedule, even without repeat release leads these last six versions (and none will repeat at least through 4.4).</p>\n<h4>Features of WordPress 4.2</h4>\n<h3>Press This</h3>\n<p><img class="aligncenter size-large wp-image-12436" src="https://poststatus.com/wp-content/uploads/2015/04/press-this-wp-752x517.png" alt="press-this-wp" width="752" height="517" /></p>\n<p>It’s quite possible even long term WordPress users have never heard of, much less used, Press This. However, the bookmarklet makes sharing and publishing others’ articles on your own blog quite simple.</p>\n<p>It was long overdue to either be cut from core or completely revamped. It was completely revamped under the “feature as plugin” model, led by Michael Arestad.</p>\n<p>I’ve been using the new Press This or a while now on my personal blog, and it’s really great. If your goal is to blog more regularly, and you like to curate or share what you’re reading/watching, you’ll love Press This. You can find the bookmarklet in the admin and primary Tools page.</p>\n<p>The editor is modern and honestly a great prototype for what could be a future full WordPress editor. The bookmarklet sits in my bookmarks, and it’s encouraged me to more often blog my thoughts on what I read — versus leave my thoughts with a single Tweet.</p>\n<p>I was at first on the fence as to whether Press This made sense to get a revamp. I’m now convinced it was a great decision, and the team that worked on most of the features was outstanding.</p>\n<p>There are more features coming to Press This. You can check some of them out <a href="http://blog.michaelarestad.com/2015/04/23/pressed-it/">on Michael’s blog post</a> celebrating its inclusion in 4.2. If you’ve never pressed anything with Press This, definitely give it a shot.</p>\n<h3>Customizer Theme Switcher</h3>\n<p>Another feature plugin that made it into 4.2 is the Customizer Theme Switcher. Relatively self-explanatory, this feature brings the theme choosing experience to the customizer.</p>\n<p>The project was lead by Nick Halsey, and you can find the core proposal for the feature <a href="https://make.wordpress.org/core/2015/02/11/customizer-theme-switcher-feature-plugin-merge-proposal/">on the Make WordPress blog</a>. Not many folks using WordPress as a full CMS will change themes that often, but for those that do, the move for selecting and testing themes to the customizer makes sense.</p>\n<h3>Shiny Updates</h3>\n<p><img class="aligncenter size-full wp-image-12438" src="https://poststatus.com/wp-content/uploads/2015/04/shiny-updates.png" alt="shiny-updates" width="707" height="391" /></p>\n<p>Shiny Updates allows for inline updates directly in the plugins admin screen, without a redirect to the funky plugin update progression page you’re probably quite familiar with.</p>\n<p>Shiny Updates is part of a larger effort for making both updates and installs better. Due to some potential UX issues and in order to stick with the release schedule, shiny installs was postponed. However, in a future release, the install and activation process for plugins will be a simpler process as well.</p>\n<h3>Utf8mb4 support to enable special characters and emoji</h3>\n<p><img class="aligncenter size-large wp-image-12437" src="https://poststatus.com/wp-content/uploads/2015/04/4.2-characters-752x626.png" alt="4.2-characters" width="752" height="626" /></p>\n<p>WordPress can now handle all sorts of special characters by default, including Chinese, but also various glyphs and other symbols. And yes, emoji.</p>\n<p>Perhaps the most discussed and also misunderstood feature of WordPress 4.2, Utf8mb4 makes WordPress more accessible in more languages, and that is awesome.</p>\n<p>Also, don’t kid yourself: everyone loves emoji. As WordPress is used more as a mobile app backend, this change will be especially welcome. Can you imagine an app that didn’t support emoji? Of course not. <img src="https://s.w.org/images/core/emoji/72x72/1f389.png" alt="', 'no') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(993, '_transient_timeout_feed_mod_d117b5738fbd35bd8c0391cda1f2b5d9', '1430290901', 'no'),
(994, '_transient_feed_mod_d117b5738fbd35bd8c0391cda1f2b5d9', '1430247701', 'no'),
(995, '_transient_timeout_feed_b9388c83948825c1edaef0d856b7b109', '1430290902', 'no'),
(996, '_transient_feed_b9388c83948825c1edaef0d856b7b109', 'a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"\n	\n";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:117:"\n		\n		\n		\n		\n		\n		\n				\n\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n\n	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:39:"WordPress Plugins » View: Most Popular";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:45:"https://wordpress.org/plugins/browse/popular/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:39:"WordPress Plugins » View: Most Popular";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"en-US";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 28 Apr 2015 18:57:32 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:25:"http://bbpress.org/?v=1.1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:30:{i:0;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:24:"Jetpack by WordPress.com";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:49:"https://wordpress.org/plugins/jetpack/#post-23862";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 20 Jan 2011 02:21:38 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"23862@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:28:"Your WordPress, Streamlined.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"Tim Moore";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:22:"WordPress SEO by Yoast";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:54:"https://wordpress.org/plugins/wordpress-seo/#post-8321";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 01 Jan 2009 20:34:44 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"8321@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:131:"Improve your WordPress SEO: Write better content and have a fully optimized WordPress site using Yoast&#039;s WordPress SEO plugin.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Joost de Valk";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:14:"Contact Form 7";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:55:"https://wordpress.org/plugins/contact-form-7/#post-2141";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 02 Aug 2007 12:45:03 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"2141@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:54:"Just another contact form plugin. Simple but flexible.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Takayuki Miyoshi";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:7:"Akismet";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:46:"https://wordpress.org/plugins/akismet/#post-15";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 09 Mar 2007 22:11:30 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:33:"15@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:98:"Akismet checks your comments against the Akismet Web service to see if they look like spam or not.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Matt Mullenweg";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:25:"Google Analytics by Yoast";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:71:"https://wordpress.org/plugins/google-analytics-for-wordpress/#post-2316";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 14 Sep 2007 12:15:27 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"2316@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:124:"Track your WordPress site easily with the latest tracking codes and lots added data for search result pages and error pages.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Joost de Valk";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:19:"All in One SEO Pack";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:59:"https://wordpress.org/plugins/all-in-one-seo-pack/#post-753";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 30 Mar 2007 20:08:18 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"753@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:126:"All in One SEO Pack is a WordPress SEO plugin to automatically optimize your WordPress blog for Search Engines such as Google.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:8:"uberdose";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"Wordfence Security";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:51:"https://wordpress.org/plugins/wordfence/#post-29832";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sun, 04 Sep 2011 03:13:51 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"29832@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:137:"Wordfence Security is a free enterprise class security and performance plugin that makes your site up to 50 times faster and more secure.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"Wordfence";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:33:"WooCommerce - excelling eCommerce";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:53:"https://wordpress.org/plugins/woocommerce/#post-29860";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 05 Sep 2011 08:13:36 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"29860@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:97:"WooCommerce is a powerful, extendable eCommerce plugin that helps you sell anything. Beautifully.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"WooThemes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:19:"Broken Link Checker";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:60:"https://wordpress.org/plugins/broken-link-checker/#post-2441";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 08 Oct 2007 21:35:04 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"2441@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:131:"This plugin will check your posts, comments and other content for broken links and missing images, and notify you if any are found.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Janis Elsts";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:16:"TinyMCE Advanced";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:57:"https://wordpress.org/plugins/tinymce-advanced/#post-2082";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 27 Jun 2007 15:00:26 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"2082@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:71:"Enables the advanced features of TinyMCE, the WordPress WYSIWYG editor.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Andrew Ozz";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:10;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:21:"WPtouch Mobile Plugin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:48:"https://wordpress.org/plugins/wptouch/#post-5468";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 01 May 2008 04:58:09 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"5468@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:67:"Make your WordPress website mobile-friendly with just a few clicks.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:17:"BraveNewCode Inc.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:11;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:34:"UpdraftPlus Backup and Restoration";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:53:"https://wordpress.org/plugins/updraftplus/#post-38058";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 21 May 2012 15:14:11 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"38058@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:148:"Backup and restoration made easy. Complete backups; manual or scheduled (backup to S3, Dropbox, Google Drive, Rackspace, FTP, SFTP, email + others).";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"David Anderson";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:12;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"WordPress Importer";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:60:"https://wordpress.org/plugins/wordpress-importer/#post-18101";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 20 May 2010 17:42:45 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"18101@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:101:"Import posts, pages, comments, custom fields, categories, tags and more from a WordPress export file.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Brian Colinger";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:13;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:33:"Google Analytics Dashboard for WP";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:75:"https://wordpress.org/plugins/google-analytics-dashboard-for-wp/#post-50539";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sun, 10 Mar 2013 17:07:11 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"50539@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:148:"Displays Google Analytics reports and real-time statistics in your WordPress Dashboard. Inserts the latest tracking code in every page of your site.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Alin Marcu";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:14;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:26:"Page Builder by SiteOrigin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:59:"https://wordpress.org/plugins/siteorigin-panels/#post-51888";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 11 Apr 2013 10:36:42 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"51888@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:111:"Build responsive page layouts using the widgets you know and love using this simple drag and drop page builder.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Greg Priday";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:15;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"WP Multibyte Patch";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:60:"https://wordpress.org/plugins/wp-multibyte-patch/#post-28395";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 14 Jul 2011 12:22:53 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"28395@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:71:"Multibyte functionality enhancement for the WordPress Japanese package.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"plugin-master";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:16;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:32:"P3 (Plugin Performance Profiler)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:53:"https://wordpress.org/plugins/p3-profiler/#post-32894";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 12 Dec 2011 23:11:10 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"32894@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:102:"See which plugins are slowing down your site.  This plugin creates a performance report for your site.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"GoDaddy.com";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:17;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:15:"ManageWP Worker";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:48:"https://wordpress.org/plugins/worker/#post-24528";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 18 Feb 2011 13:06:58 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"24528@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:103:"ManageWP is the ultimate WordPress productivity tool, allowing you to efficiently manage your websites.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:17:"Vladimir Prelovac";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:18;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:19:"The Events Calendar";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:61:"https://wordpress.org/plugins/the-events-calendar/#post-14790";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 16 Dec 2009 21:58:18 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"14790@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:128:"The Events Calendar is a carefully crafted, extensible plugin that lets you easily share your events. Beautiful. Solid. Awesome.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Peter Chester";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:19;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:16:"Disable Comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:58:"https://wordpress.org/plugins/disable-comments/#post-26907";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 27 May 2011 04:42:58 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"26907@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:134:"Allows administrators to globally disable comments on their site. Comments can be disabled according to post type. Multisite friendly.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"solarissmoke";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:20;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:11:"Redirection";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:52:"https://wordpress.org/plugins/redirection/#post-2286";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 10 Sep 2007 04:45:08 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"2286@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:144:"Redirection is a WordPress plugin to manage 301 redirections and keep track of 404 errors without requiring knowledge of Apache .htaccess files.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"John Godley";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:21;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:11:"Meta Slider";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:51:"https://wordpress.org/plugins/ml-slider/#post-49521";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 14 Feb 2013 16:56:31 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"49521@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:145:"Easy to use WordPress slider plugin. Create SEO optimised responsive slideshows with Nivo Slider, Flex Slider, Coin Slider and Responsive Slides.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Matcha Labs";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:22;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:14:"WP Super Cache";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:55:"https://wordpress.org/plugins/wp-super-cache/#post-2572";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 05 Nov 2007 11:40:04 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"2572@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:73:"A very fast caching engine for WordPress that produces static html files.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Donncha O Caoimh";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:23;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:7:"bbPress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:49:"https://wordpress.org/plugins/bbpress/#post-14709";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sun, 13 Dec 2009 00:05:51 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"14709@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:50:"bbPress is forum software, made the WordPress way.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:17:"John James Jacoby";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:24;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:15:"NextGEN Gallery";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:56:"https://wordpress.org/plugins/nextgen-gallery/#post-1169";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 23 Apr 2007 20:08:06 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"1169@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:121:"The most popular WordPress gallery plugin and one of the most popular plugins of all time with over 12 million downloads.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"Alex Rabe";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:25;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:19:"Google XML Sitemaps";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:64:"https://wordpress.org/plugins/google-sitemap-generator/#post-132";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 09 Mar 2007 22:31:32 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"132@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:105:"This plugin will generate a special XML sitemap which will help search engines to better index your blog.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Arne Brachhold";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:26;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:8:"WP Smush";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:51:"https://wordpress.org/plugins/wp-smushit/#post-7936";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 04 Dec 2008 00:00:37 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"7936@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:108:"Reduce image file sizes, improve performance and boost your SEO using the free WPMU DEV WordPress Smush API.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Alex Dunae";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:27;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:13:"Photo Gallery";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:55:"https://wordpress.org/plugins/photo-gallery/#post-63299";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 27 Jan 2014 15:58:41 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"63299@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:143:"Photo Gallery is an advanced plugin with a list of tools and options for adding and editing images for different views. It is fully responsive.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"webdorado";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:28;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:10:"BuddyPress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:52:"https://wordpress.org/plugins/buddypress/#post-10314";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 23 Apr 2009 17:48:44 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"10314@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:144:"BuddyPress helps you run any kind of social network on your WordPress, with member profiles, activity streams, user groups, messaging, and more.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Andy Peatling";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:29;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:22:"Advanced Custom Fields";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:64:"https://wordpress.org/plugins/advanced-custom-fields/#post-25254";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 17 Mar 2011 04:07:30 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"25254@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:68:"Customise WordPress with powerful, professional and intuitive fields";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"elliotcondon";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"href";s:46:"https://wordpress.org/plugins/rss/view/popular";s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";a:12:{s:6:"server";s:5:"nginx";s:4:"date";s:29:"Tue, 28 Apr 2015 19:01:41 GMT";s:12:"content-type";s:23:"text/xml; charset=UTF-8";s:10:"connection";s:5:"close";s:4:"vary";s:15:"Accept-Encoding";s:25:"strict-transport-security";s:11:"max-age=360";s:7:"expires";s:29:"Tue, 28 Apr 2015 19:32:32 GMT";s:13:"cache-control";s:0:"";s:6:"pragma";s:0:"";s:13:"last-modified";s:31:"Tue, 28 Apr 2015 18:57:32 +0000";s:15:"x-frame-options";s:10:"SAMEORIGIN";s:4:"x-nc";s:11:"HIT lax 250";}s:5:"build";s:14:"20130911020210";}', 'no'),
(997, '_transient_timeout_feed_mod_b9388c83948825c1edaef0d856b7b109', '1430290902', 'no'),
(998, '_transient_feed_mod_b9388c83948825c1edaef0d856b7b109', '1430247702', 'no'),
(999, '_transient_timeout_plugin_slugs', '1430334102', 'no'),
(1000, '_transient_plugin_slugs', 'a:1:{i:0;s:32:"wp-sync-db-master/wp-sync-db.php";}', 'no'),
(1001, '_transient_timeout_dash_4077549d03da2e451c8b5f002294ff51', '1430290902', 'no'),
(1002, '_transient_dash_4077549d03da2e451c8b5f002294ff51', '<div class="rss-widget"><ul><li><a class=\'rsswidget\' href=\'https://wordpress.org/news/2015/04/wordpress-4-2-1/\'>WordPress 4.2.1 Security Release</a> <span class="rss-date">27 April 2015</span><div class="rssSummary">WordPress 4.2.1 is now available. This is a critical security release for all previous versions and we strongly encourage you to update your sites immediately. A few hours ago, the WordPress team was made aware of a cross-site scripting vulnerability, which could enable commenters to compromise a site. The vulnerability was discovered by Jouko Pynnönen. [&hellip;]</div></li></ul></div><div class="rss-widget"><ul><li><a class=\'rsswidget\' href=\'http://wptavern.com/redux-and-kirki-frameworks-join-forces-to-provide-better-support-for-the-wordpress-customizer\'>WPTavern: Redux and Kirki Frameworks Join Forces to Provide Better Support for the WordPress Customizer</a></li><li><a class=\'rsswidget\' href=\'http://ma.tt/2015/04/who-is-steve-jobs/\'>Matt: Who is Steve Jobs?</a></li><li><a class=\'rsswidget\' href=\'http://wptavern.com/poll-how-often-do-you-read-a-wordpress-plugins-changelog-before-updating\'>WPTavern: Poll: How Often Do You Read a WordPress Plugin’s Changelog Before Updating?</a></li></ul></div><div class="rss-widget"><ul><li class=\'dashboard-news-plugin\'><span>Popular Plugin:</span> <a href=\'https://wordpress.org/plugins/siteorigin-panels/\' class=\'dashboard-news-plugin-link\'>Page Builder by SiteOrigin</a>&nbsp;<span>(<a href=\'plugin-install.php?tab=plugin-information&amp;plugin=siteorigin-panels&amp;_wpnonce=d8f9bed1ed&amp;TB_iframe=true&amp;width=600&amp;height=800\' class=\'thickbox\' title=\'Page Builder by SiteOrigin\'>Install</a>)</span></li></ul></div>', 'no') ;

#
# End of data contents of table `wp_options`
# --------------------------------------------------------



#
# Delete any existing table `wp_postmeta`
#

DROP TABLE IF EXISTS `wp_postmeta`;


#
# Table structure of table `wp_postmeta`
#

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB AUTO_INCREMENT=439 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_postmeta`
#
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 1, '_wp_trash_meta_status', 'publish'),
(3, 1, '_wp_trash_meta_time', '1428961758'),
(4, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:"1";}'),
(5, 2, '_wp_trash_meta_status', 'publish'),
(6, 2, '_wp_trash_meta_time', '1428961405'),
(7, 6, '_edit_last', '1'),
(8, 6, '_edit_lock', '1429292305:1'),
(9, 7, '_wp_attached_file', '2015/04/1-gerard-moonen.jpg'),
(10, 7, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1673;s:6:"height";i:2509;s:4:"file";s:27:"2015/04/1-gerard-moonen.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"1-gerard-moonen-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:27:"1-gerard-moonen-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:28:"1-gerard-moonen-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(11, 6, '_thumbnail_id', '7'),
(13, 9, '_edit_last', '1'),
(14, 9, '_edit_lock', '1429292215:1'),
(15, 10, '_wp_attached_file', '2015/04/1-alberto-restifo.jpg'),
(16, 10, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2509;s:6:"height";i:1673;s:4:"file";s:29:"2015/04/1-alberto-restifo.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"1-alberto-restifo-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:29:"1-alberto-restifo-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:30:"1-alberto-restifo-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(17, 9, '_thumbnail_id', '10'),
(19, 12, '_edit_last', '1'),
(20, 12, '_edit_lock', '1429445265:1'),
(21, 13, '_wp_attached_file', '2015/04/2-vita-vilcina.jpg'),
(22, 13, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3696;s:6:"height";i:2448;s:4:"file";s:26:"2015/04/2-vita-vilcina.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"2-vita-vilcina-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:26:"2-vita-vilcina-300x199.jpg";s:5:"width";i:300;s:6:"height";i:199;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:27:"2-vita-vilcina-1024x678.jpg";s:5:"width";i:1024;s:6:"height";i:678;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(23, 12, '_thumbnail_id', '13'),
(25, 15, '_edit_last', '1'),
(26, 15, '_edit_lock', '1429042989:1'),
(27, 16, '_wp_attached_file', '2015/04/2-bruno-marinho.jpg'),
(28, 16, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2509;s:6:"height";i:1673;s:4:"file";s:27:"2015/04/2-bruno-marinho.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"2-bruno-marinho-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:27:"2-bruno-marinho-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:28:"2-bruno-marinho-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(29, 15, '_thumbnail_id', '16'),
(31, 18, '_edit_lock', '1429268736:1'),
(32, 18, '_edit_last', '1'),
(33, 18, '_thumbnail_id', '16'),
(35, 20, '_edit_lock', '1429278499:1'),
(36, 20, '_edit_last', '1'),
(37, 20, '_thumbnail_id', '10'),
(40, 22, '_edit_lock', '1429293618:1'),
(41, 22, '_edit_last', '1'),
(43, 24, '_edit_lock', '1429739882:3'),
(44, 24, '_edit_last', '1'),
(45, 24, '_wp_page_template', 'default'),
(46, 26, '_edit_lock', '1429745091:3'),
(47, 26, '_edit_last', '1'),
(48, 26, '_wp_page_template', 'default'),
(49, 26, 'featured-categories', 'a:4:{i:0;s:2:"37";i:1;s:1:"6";i:2;s:2:"13";i:3;s:2:"58";}'),
(51, 28, '_menu_item_type', 'custom'),
(52, 28, '_menu_item_menu_item_parent', '0'),
(53, 28, '_menu_item_object_id', '28'),
(54, 28, '_menu_item_object', 'custom'),
(55, 28, '_menu_item_target', ''),
(56, 28, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(57, 28, '_menu_item_xfn', ''),
(58, 28, '_menu_item_url', 'http://zamboni/'),
(59, 28, '_menu_item_orphaned', '1429485755'),
(60, 29, '_menu_item_type', 'post_type'),
(61, 29, '_menu_item_menu_item_parent', '0'),
(62, 29, '_menu_item_object_id', '26'),
(63, 29, '_menu_item_object', 'page'),
(64, 29, '_menu_item_target', ''),
(65, 29, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(66, 29, '_menu_item_xfn', ''),
(67, 29, '_menu_item_url', ''),
(68, 29, '_menu_item_orphaned', '1429485755'),
(69, 30, '_menu_item_type', 'post_type'),
(70, 30, '_menu_item_menu_item_parent', '0'),
(71, 30, '_menu_item_object_id', '24'),
(72, 30, '_menu_item_object', 'page'),
(73, 30, '_menu_item_target', ''),
(74, 30, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(75, 30, '_menu_item_xfn', ''),
(76, 30, '_menu_item_url', ''),
(77, 30, '_menu_item_orphaned', '1429485755'),
(97, 37, '_edit_lock', '1429517573:1'),
(98, 37, '_edit_last', '1'),
(101, 41, '_edit_lock', '1429606558:1'),
(102, 41, '_edit_last', '1'),
(103, 41, '_thumbnail_id', '7'),
(104, 42, '_edit_lock', '1429518077:1'),
(105, 42, '_edit_last', '1'),
(106, 42, '_thumbnail_id', '13'),
(107, 43, '_edit_lock', '1429606576:1'),
(108, 43, '_edit_last', '1'),
(109, 43, '_oembed_6527fd2344da520f9a74cc31eeb819ac', '<iframe src="https://player.vimeo.com/video/124927649" width="500" height="281" frameborder="0" title="Lets Play" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'),
(110, 43, '_oembed_time_6527fd2344da520f9a74cc31eeb819ac', '1429518409'),
(111, 44, '_edit_lock', '1429518850:1'),
(112, 44, '_edit_last', '1'),
(113, 44, '_oembed_12f205eefc64cc7921d02cac3f264cc3', '<iframe src="https://player.vimeo.com/video/125235739" width="500" height="281" frameborder="0" title="Art&amp;Graft Studio Film&reg; : The Walk" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'),
(114, 44, '_oembed_time_12f205eefc64cc7921d02cac3f264cc3', '1429518555'),
(117, 46, '_edit_last', '4'),
(118, 46, '_wpas_done_all', '1'),
(119, 46, 'kopa_resolution_total_view', '24'),
(120, 46, '_thumbnail_id', '12'),
(121, 46, 'page_featured_type', ''),
(122, 46, 'page_video_id', ''),
(123, 46, '_yoast_wpseo_linkdex', '56'),
(124, 46, '_yoast_wpseo_focuskw', 'The Zamboni'),
(125, 46, '_yoast_wpseo_title', 'The Zamboni Voted BEST HUMOR MAGAZINE | The Zamboni'),
(126, 46, '_yoast_wpseo_metadesc', 'The Zamboni has recently been voted best humor magazine by a wide array of humor authorities...'),
(127, 46, '_wpcom_is_markdown', '1'),
(128, 91, '_edit_last', '4'),
(129, 91, '_oembed_56a61587f04013819e2390011bc2a191', '<div data-configid="1784398/10514569" style="width: 500px; height: 323px;" class="issuuembed"></div><script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>'),
(130, 91, '_oembed_time_56a61587f04013819e2390011bc2a191', '1418241428'),
(131, 91, '_thumbnail_id', '92'),
(132, 91, '_wpas_done_all', '1') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(133, 91, 'page_featured_type', ''),
(134, 91, 'page_video_id', ''),
(135, 91, '_wpcom_is_markdown', '1'),
(136, 91, '_oembed_e6d7ea7f326719aa372c42da01f591e6', '<div data-configid="1784398/10514997" style="width: 500px; height: 323px;" class="issuuembed"></div><script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>'),
(137, 91, '_oembed_time_e6d7ea7f326719aa372c42da01f591e6', '1418243740'),
(138, 91, '_oembed_6fd41d8012e7d5d87ecf53f72e75e9ba', '<div data-configid="1784398/10557146" style="width: 500px; height: 323px;" class="issuuembed"></div><script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>'),
(139, 91, '_oembed_time_6fd41d8012e7d5d87ecf53f72e75e9ba', '1418565180'),
(140, 91, '_yoast_wpseo_linkdex', '50'),
(141, 91, '_yoast_wpseo_focuskw', 'Outdated Issue'),
(142, 91, '_yoast_wpseo_title', 'Tufts Zamboni - The Outdated Issue; VOLUME XXVI -- 25th Anniversary Edition | The Zamboni'),
(143, 91, '_yoast_wpseo_metadesc', 'The Zamboni is just outgrowing early-adult angst and moving into yuppie/quarter-life crisis territory with The Outdated Issue!'),
(144, 170, '_wpcom_is_markdown', '1'),
(145, 170, '_edit_last', '4'),
(146, 170, '_wpas_done_all', '1'),
(147, 170, '_thumbnail_id', '18'),
(148, 170, 'page_featured_type', ''),
(149, 170, 'page_video_id', ''),
(150, 170, '_yoast_wpseo_linkdex', '53'),
(151, 170, '_yoast_wpseo_focuskw', 'The Zamboni'),
(152, 170, '_yoast_wpseo_title', 'Why is there nothing on this website? | The Zamboni'),
(153, 170, '_yoast_wpseo_metadesc', 'We\'re working on it! We just made it and it\'ll all be updated and up and running soon. The Zamboni just discovered the internet recently... xoxo The Zamboni'),
(154, 195, '_wpcom_is_markdown', '1'),
(155, 195, '_edit_last', '5'),
(156, 195, 'page_featured_type', ''),
(157, 195, 'page_video_id', ''),
(158, 195, '_yoast_wpseo_focuskw', 'Ice Cream Flavor'),
(159, 195, '_yoast_wpseo_title', 'New Study Can Predict "Which Ice Cream Flavor Are You?"'),
(160, 195, '_yoast_wpseo_metadesc', 'This new psychology study can predict which "Ice Cream Flavor Are You?" with 98% Accuracy.'),
(161, 195, '_yoast_wpseo_linkdex', '79'),
(162, 195, '_thumbnail_id', '197'),
(163, 195, '_wpas_done_all', '1'),
(164, 195, '_yoast_wpseo_opengraph-title', 'New Study can Predict “Which Ice Cream Flavor are you” with 98% Accuracy'),
(165, 195, '_yoast_wpseo_opengraph-description', 'Lindsay Williams, one of the first to be evaluated by researchers using this new test, says the results surprised her. “I wasn’t expecting to be Rocky Road, because I have always seen it as such a masculine ice cream,” Williams says, “But then I read that us Rocky Roads are people who compromise fairly and try to be nice to everyone, but that sometimes we can lash out when we feel hurt. That’s me, you know? I can relate to that.” She went on to say that Rocky Roads are known to “perform well under pressure,” but that sometimes they “feel overworked”, just like she does. “It’s funny because my boyfriend got Strawberry,” she adds, “which is my favorite ice cream flavor. Not a lot of people know that about me.”'),
(166, 195, '_yoast_wpseo_opengraph-image', 'http://www.tuftszamboni.com/wp-content/uploads/2015/04/ice-cream.jpg'),
(167, 201, '_wpcom_is_markdown', '1'),
(168, 201, 'page_featured_type', ''),
(169, 201, 'page_video_id', ''),
(170, 201, '_yoast_wpseo_focuskw', 'TEMS Disbands'),
(171, 201, '_yoast_wpseo_title', 'TEMS Disbands in Favor of Natural Selection'),
(172, 201, '_yoast_wpseo_metadesc', 'TEMS Disbands, Resolves to let natural selection run its course.'),
(173, 201, '_yoast_wpseo_linkdex', '76'),
(174, 201, '_wpas_done_all', '1'),
(175, 201, '_yoast_wpseo_opengraph-title', 'TEMS Disbands, Resolves to Let Natural Selection Run its Course'),
(176, 201, '_yoast_wpseo_opengraph-description', 'Considering that few students would willingly submit to sterilization, it looks as though TEMS really is gone for good. Drinking and drowning in one’s own vomit may soon be all the rage at Tufts'),
(177, 201, '_thumbnail_id', '209'),
(178, 206, '_wpcom_is_markdown', '1'),
(179, 206, '_edit_last', '5'),
(180, 206, 'page_featured_type', ''),
(181, 206, 'page_video_id', ''),
(182, 206, '_yoast_wpseo_focuskw', 'iSIS'),
(183, 206, '_yoast_wpseo_title', 'Obama Announces Plan to Defeat iSIS'),
(184, 206, '_yoast_wpseo_metadesc', 'President Barack Obama vowed to “degrade and ultimately destroy” the Integrated Student Information Service, more commonly known by its acronym, iSIS.'),
(185, 206, '_yoast_wpseo_linkdex', '82'),
(186, 206, '_yoast_wpseo_opengraph-title', 'Obama Announces Plan to Defeat iSIS'),
(187, 206, '_yoast_wpseo_opengraph-description', 'Employing tersely worded rhetoric, Obama called iSIS “a network of death” and “a terrorist organization, pure and simple.” The president cited its muddled dropdown menus, long loading times, and inaccessibility on online devices as specific reasons for the website’s complete annihilation. In a touching section of the speech, Obama focused on the tragic story of a young freshman whose hopes to take Chinese 1 were ended when iSIS failed to alert her of a conflict with Introduction to Community Health until it was too late. “As Americans,” Obama proclaimed, “we welcome our responsibility to lead.”'),
(188, 206, '_thumbnail_id', '211'),
(189, 206, '_wpas_done_all', '1'),
(190, 206, '_yoast_wpseo_opengraph-image', 'http://www.tuftszamboni.com/wp-content/uploads/2015/04/Obama-Speech.jpg'),
(191, 213, '_wpcom_is_markdown', '1'),
(192, 213, '_edit_last', '5'),
(193, 213, '_thumbnail_id', '215'),
(194, 213, 'page_featured_type', ''),
(195, 213, 'page_video_id', ''),
(196, 213, '_yoast_wpseo_focuskw', 'Al Qaeda'),
(197, 213, '_yoast_wpseo_title', 'Al Qaeda Wins Local Emmy'),
(198, 213, '_yoast_wpseo_metadesc', 'Al Qaeda, best known for their work in the “Osama bin Laden” viral video campaign, received a local Emmy for lifetime achievement in television.'),
(199, 213, '_yoast_wpseo_linkdex', '84'),
(200, 213, '_wpas_done_all', '1'),
(201, 214, '_wpcom_is_markdown', '1'),
(202, 214, '_edit_last', '5'),
(203, 214, '_thumbnail_id', '222'),
(204, 214, '_wpas_done_all', '1'),
(205, 214, '_yoast_wpseo_linkdex', '85'),
(206, 214, 'page_featured_type', ''),
(207, 214, 'page_video_id', ''),
(208, 214, '_yoast_wpseo_focuskw', 'Runners'),
(209, 214, '_yoast_wpseo_title', 'Runners: What are They Looking For?'),
(210, 214, '_yoast_wpseo_metadesc', 'I am asking if you’ve ever noticed the way that a runner constantly seems to be looking for something. Have you ever wondered what it’s looking for?'),
(211, 217, '_wpcom_is_markdown', '1'),
(212, 217, '_edit_last', '5'),
(213, 217, 'page_featured_type', ''),
(214, 217, 'page_video_id', ''),
(215, 217, '_thumbnail_id', '218'),
(216, 217, '_wpas_done_all', '1'),
(217, 217, '_yoast_wpseo_linkdex', '81'),
(218, 217, '_yoast_wpseo_focuskw', 'Fletcher Student'),
(219, 217, '_yoast_wpseo_title', 'Fletcher Student Burns Down Apartment, citing "tacky" paint-job'),
(220, 217, '_yoast_wpseo_metadesc', 'Fletcher Student Burns Down Apartment, citing "tacky" paint-job.'),
(221, 224, '_wpcom_is_markdown', '1'),
(222, 224, '_edit_last', '5'),
(223, 224, '_thumbnail_id', '226'),
(224, 224, '_wpas_done_all', '1'),
(225, 224, 'page_featured_type', ''),
(226, 224, 'page_video_id', ''),
(227, 224, '_yoast_wpseo_focuskw', 'Homeless'),
(228, 224, '_yoast_wpseo_title', 'Op Ed: We Need to Help the Davis Homeless'),
(229, 224, '_yoast_wpseo_metadesc', 'A message about our duty to help the Davis homeless (Sponsored by the Pizza Bites Against Tobacco Use Lobby)'),
(230, 224, '_yoast_wpseo_linkdex', '83'),
(231, 225, '_wpcom_is_markdown', '1'),
(232, 225, '_edit_last', '5') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(233, 225, '_thumbnail_id', '230'),
(234, 225, 'page_featured_type', ''),
(235, 225, 'page_video_id', ''),
(236, 225, '_yoast_wpseo_focuskw', 'New Night Club'),
(237, 225, '_yoast_wpseo_title', 'New Night Club Erected'),
(238, 225, '_yoast_wpseo_metadesc', 'Better get ready to step up your game, Boston, as a new night club will be entering your ranks early this winter.'),
(239, 225, '_yoast_wpseo_linkdex', '79'),
(240, 225, '_wpas_done_all', '1'),
(241, 228, '_wpcom_is_markdown', '1'),
(242, 228, '_edit_last', '5'),
(243, 228, '_thumbnail_id', '229'),
(244, 228, 'page_featured_type', ''),
(245, 228, 'page_video_id', ''),
(246, 228, '_yoast_wpseo_opengraph-title', 'TUPD Plans to Triple the Number of Safety Alert Emails'),
(247, 228, '_yoast_wpseo_opengraph-description', 'Anticipating that the campus soon to become gripped by a state of fear due to the increased notifications of crime, TUPD has also announced plans to expand the GoSafe ride service into regular school hours. So far, reception to the program has been largely positive. “I feel so much safer now,” declared sophomore Julia Nagle. “I’m definitely calling GoSafe at noon tomorrow to take me from Haskell to Tilton.”'),
(248, 228, '_yoast_wpseo_opengraph-image', 'http://www.tuftszamboni.com/wp-content/uploads/2015/04/tupd-police-car.jpg'),
(249, 228, '_yoast_wpseo_focuskw', 'TUPD'),
(250, 228, '_yoast_wpseo_title', 'TUPD Plans to Triple the Number of Safety Alert Emails'),
(251, 228, '_yoast_wpseo_metadesc', 'Tufts University students have grown accustomed to receiving emails each semester detailing various break-ins and other crimes.'),
(252, 228, '_yoast_wpseo_linkdex', '85'),
(253, 228, '_wpas_done_all', '1'),
(254, 232, '_wpcom_is_markdown', '1'),
(255, 232, '_edit_last', '5'),
(256, 232, 'page_video_id', ''),
(257, 232, 'page_featured_type', ''),
(258, 232, '_yoast_wpseo_focuskw', 'Tufts'),
(259, 232, '_yoast_wpseo_linkdex', '78'),
(260, 232, '_thumbnail_id', '237'),
(261, 232, '_wpas_done_all', '1'),
(262, 232, '_yoast_wpseo_title', 'Top 5 Places to Shit at Tufts - by Lucas Zerah'),
(263, 232, '_yoast_wpseo_metadesc', 'The definitive list of the top 5 places to shit at Tufts.'),
(264, 236, '_wpcom_is_markdown', '1'),
(265, 236, '_edit_last', '4'),
(266, 236, '_thumbnail_id', '238'),
(267, 236, '_wpas_done_all', '1'),
(268, 236, '_yoast_wpseo_linkdex', '63'),
(269, 236, 'page_featured_type', ''),
(270, 236, 'page_video_id', ''),
(271, 236, '_yoast_wpseo_opengraph-title', 'Well-Meaning IR Major Informs Muslim Student That She Is Wearing Open-Toed Shoes'),
(272, 236, '_yoast_wpseo_opengraph-description', 'Lent later reflected on Abdulrabb’s lack of gratitude, “it’s thankless work informing people of their ignorance, but someone’s got to do it.”'),
(273, 236, '_yoast_wpseo_opengraph-image', 'http://www.tuftszamboni.com/wp-content/uploads/2015/04/Fletch-Study-Group.jpg'),
(274, 236, '_yoast_wpseo_focuskw', 'Student'),
(275, 236, '_yoast_wpseo_title', 'IR Student informs Muslim Student of Her Immodesty'),
(276, 236, '_yoast_wpseo_metadesc', 'Abdulrabb allegedly did not notice that the sophomore male student was approaching her until he was just inches from her face.'),
(277, 239, '_wpcom_is_markdown', '1'),
(278, 239, '_thumbnail_id', '251'),
(279, 239, '_wpas_done_all', '1'),
(280, 239, '_yoast_wpseo_linkdex', '78'),
(281, 239, 'page_featured_type', ''),
(282, 239, 'page_video_id', ''),
(283, 239, '_yoast_wpseo_focuskw', 'Acorn Head'),
(284, 239, '_yoast_wpseo_title', '￼A Day in the Life of the Acorn Head Statue'),
(285, 239, '_yoast_wpseo_metadesc', 'A day in the life of the Tufts Acorn Head Statue. 6am: Dawn breaks and the brilliant colors of the sunrise fade to a soft light that is cast upon my visage.'),
(286, 249, '_wpcom_is_markdown', '1'),
(287, 249, '_edit_last', '4'),
(288, 249, 'page_featured_type', ''),
(289, 249, 'page_video_id', ''),
(290, 249, '_yoast_wpseo_focuskw', 'NASA'),
(291, 249, '_yoast_wpseo_title', 'NASA to Send 38-Year-Old Virgin to Space'),
(292, 249, '_yoast_wpseo_metadesc', 'While the move is controversial in the field, experts suspect that Spellman may be the key to finding extraterrestrial life.'),
(293, 249, '_yoast_wpseo_linkdex', '83'),
(294, 249, '_thumbnail_id', '250'),
(295, 249, '_wpas_done_all', '1'),
(296, 249, '_yoast_wpseo_opengraph-title', 'NASA to Send 38-Year-Old Virgin to Space'),
(297, 249, '_yoast_wpseo_opengraph-description', 'While the move is controversial in the field, experts suspect that Spellman may be the key to finding extraterrestrial life.'),
(298, 249, '_yoast_wpseo_opengraph-image', 'http://www.tuftszamboni.com/wp-content/uploads/2015/04/space-virgin.jpg'),
(299, 255, '_wpcom_is_markdown', '1'),
(300, 255, '_edit_last', '4'),
(301, 255, 'page_featured_type', ''),
(302, 255, 'page_video_id', ''),
(303, 255, '_yoast_wpseo_focuskw', 'Tufts'),
(304, 255, '_yoast_wpseo_title', 'BREAKING: Tufts Dining Hacks Students\' iCloud Accounts'),
(305, 255, '_yoast_wpseo_metadesc', 'Tufts Administration officials have announced that they are launching a fullscale investigation after reports that dining staff hacked into student iClouds.'),
(306, 255, '_yoast_wpseo_linkdex', '81'),
(307, 255, '_thumbnail_id', '194'),
(308, 255, '_wpas_done_all', '1'),
(309, 264, '_wpcom_is_markdown', '1'),
(310, 264, '_edit_last', '5'),
(311, 264, '_thumbnail_id', '265'),
(312, 264, '_wpas_done_all', '1'),
(313, 264, '_yoast_wpseo_linkdex', '64'),
(314, 264, 'page_featured_type', ''),
(315, 264, 'page_video_id', ''),
(316, 264, '_yoast_wpseo_focuskw', 'Advice by drunk girls'),
(317, 264, '_yoast_wpseo_title', 'DRUNK ADVICE BY DRUNK GIRL$ FOR DRUNK GIRL$'),
(318, 264, '_yoast_wpseo_metadesc', 'Should I take one more shot? I mean you drank a lot but that’s a Peppermint Patty shot so DO. THAT. SHIT.'),
(319, 269, '_wpcom_is_markdown', '1'),
(320, 269, '_wpas_done_all', '1'),
(321, 269, '_yoast_wpseo_linkdex', '45'),
(322, 269, 'page_featured_type', ''),
(323, 269, 'page_video_id', ''),
(324, 269, '_yoast_wpseo_focuskw', 'counting to 10'),
(325, 269, '_yoast_wpseo_title', 'The Zamboni’s Exclusive Guide to Counting to 10'),
(326, 269, '_yoast_wpseo_metadesc', 'Step 1:'),
(327, 269, '_thumbnail_id', '272'),
(328, 307, '_wpcom_is_markdown', '1'),
(329, 307, '_edit_last', '5'),
(330, 273, '_wpcom_is_markdown', '1'),
(331, 273, '_edit_last', '5'),
(332, 273, '_wpas_done_all', '1') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(333, 273, '_yoast_wpseo_linkdex', '75'),
(334, 273, 'page_featured_type', ''),
(335, 273, 'page_video_id', ''),
(336, 273, '_yoast_wpseo_focuskw', 'Alumni'),
(337, 273, '_yoast_wpseo_title', 'Alumni: Where Are They Now?'),
(338, 273, '_yoast_wpseo_metadesc', 'Tufts’ Most Accomplished ExPats. The Virgin Mary, class of 5 BC, is remarkably still just as committed to celibacy as Tufts University is.'),
(339, 273, '_thumbnail_id', '288'),
(340, 277, '_wpcom_is_markdown', '1'),
(341, 277, '_edit_last', '5'),
(342, 277, '_thumbnail_id', '278'),
(343, 277, '_wpas_done_all', '1'),
(344, 277, '_yoast_wpseo_linkdex', '73'),
(345, 277, 'page_featured_type', ''),
(346, 277, 'page_video_id', ''),
(347, 277, '_yoast_wpseo_focuskw', 'ABC parties'),
(348, 277, '_yoast_wpseo_metadesc', 'if you are looking to dress a little outside the box for your upcoming ABC party, The Zamboni has a list of costumes for you that are sure to slay.'),
(349, 280, '_wpcom_is_markdown', '1'),
(350, 280, '_edit_last', '5'),
(351, 280, '_thumbnail_id', '281'),
(352, 280, '_wpas_done_all', '1'),
(353, 280, '_yoast_wpseo_linkdex', '75'),
(354, 280, 'page_featured_type', ''),
(355, 280, 'page_video_id', ''),
(356, 280, '_yoast_wpseo_focuskw', 'Celebrity Followings'),
(357, 280, '_yoast_wpseo_title', 'Celebrity Followings: Just Another Day in the Office'),
(358, 280, '_yoast_wpseo_metadesc', 'When Mr. Obama finished up his half-mile loop of Pennsylvania Ave, he saw Mr. Clinton strutting through the memorial gardens, looking happy as ever.'),
(359, 283, '_wpcom_is_markdown', '1'),
(360, 283, '_edit_last', '1'),
(361, 283, '_wpas_done_all', '1'),
(362, 283, '_yoast_wpseo_linkdex', '81'),
(363, 283, 'page_featured_type', ''),
(364, 283, 'page_video_id', ''),
(365, 283, '_yoast_wpseo_focuskw', 'shrooms'),
(366, 283, '_yoast_wpseo_title', 'How I Discovered the DNA Double Helix While on Shrooms'),
(367, 283, '_yoast_wpseo_metadesc', 'Before my Fateful Shrooms Trip, nobody knew what DNA looked like. Generations of scientists had tried for years to crack the code, and many of them went mad'),
(368, 283, '_thumbnail_id', '16'),
(369, 20, '_wp_trash_meta_status', 'publish'),
(370, 20, '_wp_trash_meta_time', '1429580798'),
(371, 18, '_wp_trash_meta_status', 'publish'),
(372, 18, '_wp_trash_meta_time', '1429580798'),
(373, 22, '_wp_trash_meta_status', 'publish'),
(374, 22, '_wp_trash_meta_time', '1429580798'),
(375, 15, '_wp_trash_meta_status', 'publish'),
(376, 15, '_wp_trash_meta_time', '1429580798'),
(377, 12, '_wp_trash_meta_status', 'publish'),
(378, 12, '_wp_trash_meta_time', '1429580798'),
(379, 9, '_wp_trash_meta_status', 'publish'),
(380, 9, '_wp_trash_meta_time', '1429580798'),
(381, 6, '_wp_trash_meta_status', 'publish'),
(382, 6, '_wp_trash_meta_time', '1429580798'),
(383, 307, '_wp_trash_meta_status', 'draft'),
(384, 307, '_wp_trash_meta_time', '1429580798'),
(385, 283, '_edit_lock', '1430175671:1'),
(386, 206, '_edit_lock', '1429614083:1'),
(387, 280, '_edit_lock', '1429643806:1'),
(390, 313, '_menu_item_type', 'taxonomy'),
(391, 313, '_menu_item_menu_item_parent', '0'),
(392, 313, '_menu_item_object_id', '7'),
(393, 313, '_menu_item_object', 'category'),
(394, 313, '_menu_item_target', ''),
(395, 313, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(396, 313, '_menu_item_xfn', ''),
(397, 313, '_menu_item_url', ''),
(399, 314, '_menu_item_type', 'taxonomy'),
(400, 314, '_menu_item_menu_item_parent', '0'),
(401, 314, '_menu_item_object_id', '13'),
(402, 314, '_menu_item_object', 'category'),
(403, 314, '_menu_item_target', ''),
(404, 314, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(405, 314, '_menu_item_xfn', ''),
(406, 314, '_menu_item_url', ''),
(408, 315, '_menu_item_type', 'taxonomy'),
(409, 315, '_menu_item_menu_item_parent', '0'),
(410, 315, '_menu_item_object_id', '37'),
(411, 315, '_menu_item_object', 'category'),
(412, 315, '_menu_item_target', ''),
(413, 315, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(414, 315, '_menu_item_xfn', ''),
(415, 315, '_menu_item_url', ''),
(417, 316, '_menu_item_type', 'taxonomy'),
(418, 316, '_menu_item_menu_item_parent', '0'),
(419, 316, '_menu_item_object_id', '58'),
(420, 316, '_menu_item_object', 'category'),
(421, 316, '_menu_item_target', ''),
(422, 316, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(423, 316, '_menu_item_xfn', ''),
(424, 316, '_menu_item_url', ''),
(426, 317, '_edit_lock', '1429840789:3'),
(428, 283, 'yes', 'on'),
(431, 283, 'slider', 'a:1:{i:0;s:3:"143";}'),
(433, 283, 'author-alias', 'Test'),
(434, 318, '_wp_attached_file', '2015/04/facebook29.svg'),
(435, 319, '_wp_attached_file', '2015/04/twitter2.svg'),
(437, 283, 'cover-photo_id', '10'),
(438, 283, 'cover-photo', 'http://zamboni/wp-content/uploads/2015/04/1-alberto-restifo.jpg') ;

#
# End of data contents of table `wp_postmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_posts`
#

DROP TABLE IF EXISTS `wp_posts`;


#
# Table structure of table `wp_posts`
#

CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(20) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=321 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_posts`
#
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2015-04-13 14:45:14', '2015-04-13 14:45:14', 'Witaj w WordPressie. To jest Twój pierwszy wpis. Zmodyfikuj go lub usuń, a następnie rozpocznij blogowanie!', 'Witaj, świecie!', '', 'trash', 'open', 'open', '', 'witaj-swiecie', '', '', '2015-04-13 23:49:18', '2015-04-13 21:49:18', '', 0, 'http://zamboni/?p=1', 0, 'post', '', 1),
(2, 1, '2015-04-13 14:45:14', '2015-04-13 14:45:14', 'To jest przykładowa strona. Różni się ona od postu na blogu, ponieważ pozostanie ona ciągle w jednym miejscu i pokaże się w panelu nawigacyjnym na twojej stronie (w przypadku większości szablonów). Większość osób zaczyna od strony \'O mnie\' lub \'O nas\', która przedstawia ich potencjalnym odwiedzającym stronę. Może to być coś takiego:\n\n<blockquote>Cześć! Jestem kurierem na rowerze za dnia, nocą aktorem amatorem, a to jest mój blog. Mieszkam w Gdańsku, mam wspaniałego psa o imieniu Azor i uwielbiam pi&#241;a coladę. (Oraz to, kiedy łapie mnie deszcz.)</blockquote>\n\n...lub coś bardziej jak to:\n\n<blockquote>Firma XYZ została założona w 1971 roku i od tamtej pory wytwarza najlepszej jakości wijaster na rynku. Zlokalizowana w Pacanowie zatrudnia ponad 2000 osób i jest powodem samego dobrodziejstwa dla lokalnej społeczności.</blockquote>\n\nJako nowy użytkowni WordPressa, powinieneś odwiedzić <a href="http://zamboni/wp-admin/">swój panel administracyjny</a>, aby skasować tę stronę i stworzyć nowe, dostosowane do Twoich potrzeb strony. Miłej zabawy!', 'Przykładowa strona', '', 'trash', 'open', 'open', '', 'przykladowa-strona', '', '', '2015-04-13 23:43:25', '2015-04-13 21:43:25', '', 0, 'http://zamboni/?page_id=2', 0, 'page', '', 0),
(4, 1, '2015-04-13 23:49:18', '2015-04-13 21:49:18', 'Witaj w WordPressie. To jest Twój pierwszy wpis. Zmodyfikuj go lub usuń, a następnie rozpocznij blogowanie!', 'Witaj, świecie!', '', 'inherit', 'open', 'open', '', '1-revision-v1', '', '', '2015-04-13 23:49:18', '2015-04-13 21:49:18', '', 1, 'http://zamboni/?p=4', 0, 'revision', '', 0),
(5, 1, '2015-04-13 23:43:25', '2015-04-13 21:43:25', 'To jest przykładowa strona. Różni się ona od postu na blogu, ponieważ pozostanie ona ciągle w jednym miejscu i pokaże się w panelu nawigacyjnym na twojej stronie (w przypadku większości szablonów). Większość osób zaczyna od strony \'O mnie\' lub \'O nas\', która przedstawia ich potencjalnym odwiedzającym stronę. Może to być coś takiego:\n\n<blockquote>Cześć! Jestem kurierem na rowerze za dnia, nocą aktorem amatorem, a to jest mój blog. Mieszkam w Gdańsku, mam wspaniałego psa o imieniu Azor i uwielbiam pi&#241;a coladę. (Oraz to, kiedy łapie mnie deszcz.)</blockquote>\n\n...lub coś bardziej jak to:\n\n<blockquote>Firma XYZ została założona w 1971 roku i od tamtej pory wytwarza najlepszej jakości wijaster na rynku. Zlokalizowana w Pacanowie zatrudnia ponad 2000 osób i jest powodem samego dobrodziejstwa dla lokalnej społeczności.</blockquote>\n\nJako nowy użytkowni WordPressa, powinieneś odwiedzić <a href="http://zamboni/wp-admin/">swój panel administracyjny</a>, aby skasować tę stronę i stworzyć nowe, dostosowane do Twoich potrzeb strony. Miłej zabawy!', 'Przykładowa strona', '', 'inherit', 'open', 'open', '', '2-revision-v1', '', '', '2015-04-13 23:43:25', '2015-04-13 21:43:25', '', 2, 'http://zamboni/?p=5', 0, 'revision', '', 0),
(6, 1, '2015-04-14 20:22:43', '2015-04-14 18:22:43', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean interdum, enim vitae consequat efficitur, ex neque venenatis odio, vel consectetur quam ipsum eu mi. Vivamus facilisis, elit et volutpat commodo, nisl eros maximus quam, vestibulum aliquet est leo at lacus. Mauris ac dui in lacus lacinia laoreet eu non urna. Ut et lectus pharetra, tempus magna vitae, condimentum nibh. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque porttitor dui at elit pharetra, sed convallis velit tincidunt. Nam varius nisl scelerisque ipsum finibus, ac eleifend magna ullamcorper. Etiam in elit nec libero luctus consequat eget sit amet diam. Sed non pretium enim, et commodo odio. Nullam aliquam auctor augue vel pharetra.', 'Test', '', 'trash', 'open', 'open', '', 'test', '', '', '2015-04-21 03:46:38', '2015-04-21 01:46:38', '', 0, 'http://zamboni/?p=6', 0, 'post', '', 0),
(7, 1, '2015-04-14 20:22:36', '2015-04-14 18:22:36', '', '1-gerard-moonen', '', 'inherit', 'open', 'open', '', '1-gerard-moonen', '', '', '2015-04-14 20:22:36', '2015-04-14 18:22:36', '', 6, 'http://zamboni/wp-content/uploads/2015/04/1-gerard-moonen.jpg', 0, 'attachment', 'image/jpeg', 0),
(8, 1, '2015-04-14 20:22:43', '2015-04-14 18:22:43', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean interdum, enim vitae consequat efficitur, ex neque venenatis odio, vel consectetur quam ipsum eu mi. Vivamus facilisis, elit et volutpat commodo, nisl eros maximus quam, vestibulum aliquet est leo at lacus. Mauris ac dui in lacus lacinia laoreet eu non urna. Ut et lectus pharetra, tempus magna vitae, condimentum nibh. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque porttitor dui at elit pharetra, sed convallis velit tincidunt. Nam varius nisl scelerisque ipsum finibus, ac eleifend magna ullamcorper. Etiam in elit nec libero luctus consequat eget sit amet diam. Sed non pretium enim, et commodo odio. Nullam aliquam auctor augue vel pharetra.', 'Test', '', 'inherit', 'open', 'open', '', '6-revision-v1', '', '', '2015-04-14 20:22:43', '2015-04-14 18:22:43', '', 6, 'http://zamboni/?p=8', 0, 'revision', '', 0),
(9, 1, '2015-04-14 20:23:12', '2015-04-14 18:23:12', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean interdum, enim vitae consequat efficitur, ex neque venenatis odio, vel consectetur quam ipsum eu mi. Vivamus facilisis, elit et volutpat commodo, nisl eros maximus quam, vestibulum aliquet est leo at lacus. Mauris ac dui in lacus lacinia laoreet eu non urna. Ut et lectus pharetra, tempus magna vitae, condimentum nibh. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque porttitor dui at elit pharetra, sed convallis velit tincidunt. Nam varius nisl scelerisque ipsum finibus, ac eleifend magna ullamcorper. Etiam in elit nec libero luctus consequat eget sit amet diam. Sed non pretium enim, et commodo odio. Nullam aliquam auctor augue vel pharetra.', 'Test2', '', 'trash', 'open', 'open', '', 'test2', '', '', '2015-04-21 03:46:38', '2015-04-21 01:46:38', '', 0, 'http://zamboni/?p=9', 0, 'post', '', 0),
(10, 1, '2015-04-14 20:23:08', '2015-04-14 18:23:08', '', '1-alberto-restifo', '', 'inherit', 'open', 'open', '', '1-alberto-restifo', '', '', '2015-04-14 20:23:08', '2015-04-14 18:23:08', '', 9, 'http://zamboni/wp-content/uploads/2015/04/1-alberto-restifo.jpg', 0, 'attachment', 'image/jpeg', 0),
(11, 1, '2015-04-14 20:23:12', '2015-04-14 18:23:12', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean interdum, enim vitae consequat efficitur, ex neque venenatis odio, vel consectetur quam ipsum eu mi. Vivamus facilisis, elit et volutpat commodo, nisl eros maximus quam, vestibulum aliquet est leo at lacus. Mauris ac dui in lacus lacinia laoreet eu non urna. Ut et lectus pharetra, tempus magna vitae, condimentum nibh. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque porttitor dui at elit pharetra, sed convallis velit tincidunt. Nam varius nisl scelerisque ipsum finibus, ac eleifend magna ullamcorper. Etiam in elit nec libero luctus consequat eget sit amet diam. Sed non pretium enim, et commodo odio. Nullam aliquam auctor augue vel pharetra.', 'Test2', '', 'inherit', 'open', 'open', '', '9-revision-v1', '', '', '2015-04-14 20:23:12', '2015-04-14 18:23:12', '', 9, 'http://zamboni/?p=11', 0, 'revision', '', 0),
(12, 1, '2015-04-14 20:23:48', '2015-04-14 18:23:48', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean interdum, enim vitae consequat efficitur, ex neque venenatis odio, vel consectetur quam ipsum eu mi. Vivamus facilisis, elit et volutpat commodo, nisl eros maximus quam, vestibulum aliquet est leo at lacus. Mauris ac dui in lacus lacinia laoreet eu non urna. Ut et lectus pharetra, tempus magna vitae, condimentum nibh. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque porttitor dui at elit pharetra, sed convallis velit tincidunt. Nam varius nisl scelerisque ipsum finibus, ac eleifend magna ullamcorper. Etiam in elit nec libero luctus consequat eget sit amet diam. Sed non pretium enim, et commodo odio. Nullam aliquam auctor augue vel pharetra.', 'Test3', '', 'trash', 'open', 'open', '', 'test3', '', '', '2015-04-21 03:46:38', '2015-04-21 01:46:38', '', 0, 'http://zamboni/?p=12', 0, 'post', '', 0),
(13, 1, '2015-04-14 20:23:44', '2015-04-14 18:23:44', '', '2-vita-vilcina', '', 'inherit', 'open', 'open', '', '2-vita-vilcina', '', '', '2015-04-14 20:23:44', '2015-04-14 18:23:44', '', 12, 'http://zamboni/wp-content/uploads/2015/04/2-vita-vilcina.jpg', 0, 'attachment', 'image/jpeg', 0),
(14, 1, '2015-04-14 20:23:48', '2015-04-14 18:23:48', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean interdum, enim vitae consequat efficitur, ex neque venenatis odio, vel consectetur quam ipsum eu mi. Vivamus facilisis, elit et volutpat commodo, nisl eros maximus quam, vestibulum aliquet est leo at lacus. Mauris ac dui in lacus lacinia laoreet eu non urna. Ut et lectus pharetra, tempus magna vitae, condimentum nibh. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque porttitor dui at elit pharetra, sed convallis velit tincidunt. Nam varius nisl scelerisque ipsum finibus, ac eleifend magna ullamcorper. Etiam in elit nec libero luctus consequat eget sit amet diam. Sed non pretium enim, et commodo odio. Nullam aliquam auctor augue vel pharetra.', 'Test3', '', 'inherit', 'open', 'open', '', '12-revision-v1', '', '', '2015-04-14 20:23:48', '2015-04-14 18:23:48', '', 12, 'http://zamboni/?p=14', 0, 'revision', '', 0),
(15, 1, '2015-04-14 20:24:18', '2015-04-14 18:24:18', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean interdum, enim vitae consequat efficitur, ex neque venenatis odio, vel consectetur quam ipsum eu mi. Vivamus facilisis, elit et volutpat commodo, nisl eros maximus quam, vestibulum aliquet est leo at lacus. Mauris ac dui in lacus lacinia laoreet eu non urna. Ut et lectus pharetra, tempus magna vitae, condimentum nibh. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque porttitor dui at elit pharetra, sed convallis velit tincidunt. Nam varius nisl scelerisque ipsum finibus, ac eleifend magna ullamcorper. Etiam in elit nec libero luctus consequat eget sit amet diam. Sed non pretium enim, et commodo odio. Nullam aliquam auctor augue vel pharetra.', 'Test4', '', 'trash', 'open', 'open', '', 'test4', '', '', '2015-04-21 03:46:38', '2015-04-21 01:46:38', '', 0, 'http://zamboni/?p=15', 0, 'post', '', 0),
(16, 1, '2015-04-14 20:24:14', '2015-04-14 18:24:14', '', '2-bruno-marinho', '', 'inherit', 'open', 'open', '', '2-bruno-marinho', '', '', '2015-04-14 20:24:14', '2015-04-14 18:24:14', '', 15, 'http://zamboni/wp-content/uploads/2015/04/2-bruno-marinho.jpg', 0, 'attachment', 'image/jpeg', 0),
(17, 1, '2015-04-14 20:24:18', '2015-04-14 18:24:18', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean interdum, enim vitae consequat efficitur, ex neque venenatis odio, vel consectetur quam ipsum eu mi. Vivamus facilisis, elit et volutpat commodo, nisl eros maximus quam, vestibulum aliquet est leo at lacus. Mauris ac dui in lacus lacinia laoreet eu non urna. Ut et lectus pharetra, tempus magna vitae, condimentum nibh. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque porttitor dui at elit pharetra, sed convallis velit tincidunt. Nam varius nisl scelerisque ipsum finibus, ac eleifend magna ullamcorper. Etiam in elit nec libero luctus consequat eget sit amet diam. Sed non pretium enim, et commodo odio. Nullam aliquam auctor augue vel pharetra.', 'Test4', '', 'inherit', 'open', 'open', '', '15-revision-v1', '', '', '2015-04-14 20:24:18', '2015-04-14 18:24:18', '', 15, 'http://zamboni/?p=17', 0, 'revision', '', 0),
(18, 1, '2015-04-17 12:34:10', '2015-04-17 10:34:10', 'Lorem bla bla, test 1', 'Slider1', '', 'trash', 'open', 'open', '', 'slider1', '', '', '2015-04-21 03:46:38', '2015-04-21 01:46:38', '', 0, 'http://zamboni/?p=18', 0, 'post', '', 0),
(19, 1, '2015-04-17 12:34:10', '2015-04-17 10:34:10', 'Lorem bla bla, test 1', 'Slider1', '', 'inherit', 'open', 'open', '', '18-revision-v1', '', '', '2015-04-17 12:34:10', '2015-04-17 10:34:10', '', 18, 'http://zamboni/?p=19', 0, 'revision', '', 0),
(20, 1, '2015-04-17 13:08:36', '2015-04-17 11:08:36', 'Test slider 2', 'Slider2', '', 'trash', 'open', 'open', '', 'slider2', '', '', '2015-04-21 03:46:38', '2015-04-21 01:46:38', '', 0, 'http://zamboni/?p=20', 0, 'post', '', 0),
(21, 1, '2015-04-17 13:08:36', '2015-04-17 11:08:36', 'Test slider 2', 'Slider2', '', 'inherit', 'open', 'open', '', '20-revision-v1', '', '', '2015-04-17 13:08:36', '2015-04-17 11:08:36', '', 20, 'http://zamboni/?p=21', 0, 'revision', '', 0),
(22, 1, '2015-04-16 01:40:35', '2015-04-15 23:40:35', 'Oto on.', 'Strollowany nr 1', '', 'trash', 'open', 'open', '', 'strollowany-nr-1', '', '', '2015-04-21 03:46:38', '2015-04-21 01:46:38', '', 0, 'http://zamboni/?p=22', 0, 'post', '', 0),
(23, 1, '2015-04-17 19:41:31', '2015-04-17 17:41:31', 'Oto on.', 'Strollowany nr 1', '', 'inherit', 'open', 'open', '', '22-revision-v1', '', '', '2015-04-17 19:41:31', '2015-04-17 17:41:31', '', 22, 'http://zamboni/?p=23', 0, 'revision', '', 0),
(24, 1, '2015-04-19 11:26:25', '2015-04-19 09:26:25', '', 'Recent Stories', '', 'publish', 'open', 'open', '', 'recent-stories', '', '', '2015-04-19 11:26:25', '2015-04-19 09:26:25', '', 0, 'http://zamboni/?page_id=24', 0, 'page', '', 0),
(25, 1, '2015-04-19 11:26:25', '2015-04-19 09:26:25', '', 'Recent Stories', '', 'inherit', 'open', 'open', '', '24-revision-v1', '', '', '2015-04-19 11:26:25', '2015-04-19 09:26:25', '', 24, 'http://zamboni/?p=25', 0, 'revision', '', 0),
(26, 1, '2015-04-19 11:39:29', '2015-04-19 09:39:29', '', 'Categories', '', 'publish', 'open', 'open', '', 'categories', '', '', '2015-04-21 10:53:57', '2015-04-21 08:53:57', '', 0, 'http://zamboni/?page_id=26', 0, 'page', '', 0),
(27, 1, '2015-04-19 11:39:29', '2015-04-19 09:39:29', '', 'Categories', '', 'inherit', 'open', 'open', '', '26-revision-v1', '', '', '2015-04-19 11:39:29', '2015-04-19 09:39:29', '', 26, 'http://zamboni/?p=27', 0, 'revision', '', 0),
(28, 1, '2015-04-20 01:22:35', '0000-00-00 00:00:00', '', 'Strona główna', '', 'draft', 'open', 'open', '', '', '', '', '2015-04-20 01:22:35', '0000-00-00 00:00:00', '', 0, 'http://zamboni/?p=28', 1, 'nav_menu_item', '', 0),
(29, 1, '2015-04-20 01:22:35', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'open', 'open', '', '', '', '', '2015-04-20 01:22:35', '0000-00-00 00:00:00', '', 0, 'http://zamboni/?p=29', 1, 'nav_menu_item', '', 0),
(30, 1, '2015-04-20 01:22:35', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'open', 'open', '', '', '', '', '2015-04-20 01:22:35', '0000-00-00 00:00:00', '', 0, 'http://zamboni/?p=30', 1, 'nav_menu_item', '', 0),
(37, 1, '2015-04-20 09:59:08', '0000-00-00 00:00:00', '', 'Galeria1', '', 'draft', 'closed', 'closed', '', '', '', '', '2015-04-20 09:59:08', '2015-04-20 07:59:08', '', 0, 'http://zamboni/?post_type=gallery-post&#038;p=37', 0, 'gallery-post', '', 0),
(41, 1, '2015-04-20 10:15:43', '2015-04-20 08:15:43', '', 'Galeria1', '', 'publish', 'closed', 'closed', '', 'galeria1', '', '', '2015-04-20 10:15:43', '2015-04-20 08:15:43', '', 0, 'http://zamboni/?post_type=gallery-slider-post&#038;p=41', 0, 'gallery-slider-post', '', 0),
(42, 1, '2015-04-20 10:21:10', '2015-04-20 08:21:10', '', 'Galeria2', '', 'publish', 'closed', 'closed', '', 'galeria2', '', '', '2015-04-20 10:21:10', '2015-04-20 08:21:10', '', 0, 'http://zamboni/?post_type=gallery-slider-post&#038;p=42', 0, 'gallery-slider-post', '', 0),
(43, 1, '2015-04-20 10:26:56', '2015-04-20 08:26:56', 'https://vimeo.com/channels/staffpicks/124927649', 'Wideo1', '', 'publish', 'closed', 'closed', '', 'wideo1', '', '', '2015-04-20 10:26:56', '2015-04-20 08:26:56', '', 0, 'http://zamboni/?post_type=video-slider-post&#038;p=43', 0, 'video-slider-post', '', 0),
(44, 1, '2015-04-20 10:29:21', '2015-04-20 08:29:21', 'https://vimeo.com/channels/staffpicks/125235739', 'Wideo2', '', 'publish', 'closed', 'closed', '', 'wideo2', '', '', '2015-04-20 10:29:21', '2015-04-20 08:29:21', '', 0, 'http://zamboni/?post_type=video-slider-post&#038;p=44', 0, 'video-slider-post', '', 0),
(46, 3, '2014-08-26 21:42:27', '2014-08-27 01:42:27', 'Congratulations! The Zamboni has surely asserted itself as the greatest humor magazine and comedy group of all time in any sort of college or collegiate setting. Good job.\n\nSome who have voted The Zamboni at such pinnacle of quality are:\n\n<ul>\n    <li>Jake Gyllanhaal</li>\n    <li>Gake Jyllanhaal</li>\n    <li>Fake Middlefall</li>\n    <li>Beyonce</li>\n    <li>The Thing (the 1982 original movie)</li>\n    <li>The Thing (the 1951 story)</li>\n    <li>Gumby</li>\n    <li>Jay-Z</li>\n    <li>Jennifer Lopez</li>\n    <li>John Goodman</li>\n    <li>Harvey Dent</li>\n    <li>Jennifer Lopez again</li>\n    <li>Cary Grant</li>\n    <li>The Zamboni Editors</li>\n</ul>', 'The Zamboni Voted Best Humor Magazine of All Ever by Very Reputable and Important People!', '', 'publish', 'closed', 'open', '', 'the-zamboni-voted-best-humor-magazine-of-all-ever-by-very-reputable-and-important-people', '', '', '2014-08-26 21:42:27', '2014-08-27 01:42:27', '', 0, 'http://www.tuftszamboni.com/?p=6', 0, 'post', '', 0),
(91, 3, '2014-12-10 14:59:16', '2014-12-10 19:59:16', '<p style="text-align: left;">The Zamboni is just outgrowing early-adult angst and moving into yuppie/quarter-life crisis territory! Come join us as we realize we\'re no longer as young as we once were and that everything is much darker and more depressing than we could ever imagine with The Outdated Issue!</p>\n\n<div class="issuuembed" style="width: 900px; height: 732px;" data-configid="1784398/10514569"></div>\n\n<script src="//e.issuu.com/embed.js" async="true" type="text/javascript"></script>', 'Vol. XXVI, No. 1 - The Outdated Issue (25th Anniversary Issue!)', '', 'publish', 'closed', 'open', '', 'vol-xxvi-no-1-outdated-issue-25th-anniversary-issue', '', '', '2014-12-10 14:59:16', '2014-12-10 19:59:16', '', 0, 'http://www.tuftszamboni.com/?p=91', 0, 'post', '', 0),
(170, 3, '2014-12-14 21:42:16', '2014-12-15 02:42:16', 'We\'re working on it! We just made it and it\'ll all be updated and up and running soon. The Zamboni just discovered the internet very recently and is using the latest version of Netscape to try and update this Web.0 FrontPage as soon as possible!\n\nxoxo\nThe Zamboni', 'WHY IS THERE NOTHING ON THIS WEBSITE? | The Zamboni', '', 'publish', 'closed', 'open', '', 'nothing-website', '', '', '2014-12-14 21:42:16', '2014-12-15 02:42:16', '', 0, 'http://www.tuftszamboni.com/?p=170', 0, 'post', '', 0),
(195, 4, '2015-04-11 18:54:10', '2015-04-11 22:54:10', '<div class="page" title="Page 5">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\nby Judith Grossman\n\nThe psychology community was abuzz with excited chatter today over a new personality type assessment fresh out of the distinguished labs at Harvard. The new test, which claims almost flawless accuracy, is designed to allow researchers to determine which type of ice cream a given person is. “It’s really going to revolutionize the way we study the human brain,” says Carmen Sanchez, one of the project’s senior researchers. “We have been looking at people using outdated models like Myers-Briggs, and I think the new generation of psychologists is ready to make this change.”\n\nLindsay Williams, one of the first to be evaluated by researchers using this new test, says the results surprised her. “I wasn’t expecting to be Rocky Road, because I have always seen it as such a masculine ice cream,” Williams says, “But then I read that us Rocky Roads are people who compromise fairly and try to be nice to everyone, but that sometimes we can lash out when we feel hurt. That’s me, you know? I can relate to that.” She went on to say that Rocky Roads are known to “perform well under pressure,” but that sometimes they “feel overworked”, just like she does. “It’s funny because my boyfriend got Strawberry,” she adds, “which is my favorite ice cream flavor. Not a lot of people know that about me.”\n\n&nbsp;\n\n</div>\n<div class="column">\n\n[caption id="attachment_197" align="alignnone" width="479"]<a href="http://www.tuftszamboni.com/wp-content/uploads/2015/04/ice-cream.jpg"><img class=" wp-image-197" src="http://www.tuftszamboni.com/wp-content/uploads/2015/04/ice-cream-300x230.jpg" alt="Madeline and Safiya are both cookie dough!" width="479" height="366" /></a> Madeline and Safiya are both cookie dough![/caption]\n\n&nbsp;\n\nSanchez and her team are not done leading the way for the psychology community. Also announced today was a new collaboration with the history department of UCLA to determine which era of history a subject should have lived in. “It’s groundbreaking research, and it’s too early to say anything for sure,” Sanchez says. “We have also been looking into a collaboration with our sociology department ‘Which TV Sitcom Friend Group is Most Like Yours?’, but at this time they fail to see the need for the study.”\n\n&nbsp;\n\n<em>Judith Grossman is a Tufts Zamboni Contributor and Author who lives in Houston, TX ironically with her two small dogs. She is most like Chocolate ice cream because she sometimes gets overlooked, but she has great ideas and is always ready to comfort her friends. She is single and looking, DL white 130 5’4 C u host ;)</em>\n\n</div>\n</div>\n</div>\n</div>', 'New Study can Predict “Which Ice Cream Flavor are you” with 98% Accuracy', '', 'publish', 'closed', 'open', '', 'new-study-can-predict-which-ice-cream-flavor-are-you-with-98-accuracy-by-judith-grossman', '', '', '2015-04-11 18:54:10', '2015-04-11 22:54:10', '', 0, 'http://www.tuftszamboni.com/?p=195', 0, 'post', '', 0),
(201, 4, '2015-04-11 19:07:59', '2015-04-11 23:07:59', 'by Aviva Schmitz\n\nTufts Emergency Medical Services president Ivan Proust shocked and enraged the Tufts community today by announcing that TEMS has officially ceased all operations.\n\n“We have been puked on for the last time,” proclaimed Proust. “We’re tired of running around all night taking care of assholes who can’t handle their alcohol.”\n\nCertainly, Proust and his TEMS peers have reason to be tired. In the past two weeks alone, TEMS has responded to 326 alcohol-related calls, with 122 of those calls necessitating an on-scene liver transplant. With no spare livers readily available, TEMS responders must often sacrifice a lobe of their own.\n\n“I swear to God, there are probably a hundred freshman walking around with a piece of my liver inside of them,” complained TEMS member Sara Holt. “Next time someone needs my liver, I’ll rip the whole thing out and shove it down their throat.” Asphyxiation by liver aside, TEMS\nmembers also stated that longstanding ethical concerns contributed to their decision to disband. It was responder Jack Fishell who first brought the immorality of TEMS intervention to the team’s attention.\n\n“I joined TEMS two years ago just so I could make out with hot chicks during mouth-to-mouth resuscitation and feel their tits during chest compressions, but I always had a sense that what I was doing was wrong,” said Fishell. “I felt that we were saving people who didn’t deserve to be saved when the world is overpopulated anyway.”\n\nFor years, Fishell didn’t dare voice his new perspective to other TEMS members, for fear that they would disagree with him. However, when Fishell and his fellow TEMS responder Joshua Mule attended a party together last semester, Fishell drunkenly confessed his secret to Mule. Although the night ended with the two men TEMS-ing each other, both miraculously retained memories of their conversation. Mule agreed with Fishell, and together they presented their ideas to the rest of the TEMS crew the next day. While their views met with some initial resistance, within hours all team members had embraced natural selection. For them, the end of TEMS now comes as a welcome and long-awaited event.\n\nWhen asked if there is any possibility of TEMS’s reinstatement, Proust replied, “Absolutely not. We simply cannot continue to flout the universe’s natural progression.”\n\nHolt echoed Proust’s sentiments, though in a decidedly grittier fashion: “Sure, we could reinstate TEMS, but only if you’d agree to have your dick chopped off while you get your stomach pumped.”\n\nConsidering that few students would willingly submit to sterilization, it looks as though TEMS really is gone for good. Drinking and drowning in one’s own vomit may soon be all the rage at Tufts.', 'TEMS Disbands, Resolves to Let Natural Selection Run Its Course', '', 'publish', 'closed', 'open', '', 'tems-disbands-resolves-to-let-natural-selection-run-its-course-by-aviva-schmitz', '', '', '2015-04-11 19:07:59', '2015-04-11 23:07:59', '', 0, 'http://www.tuftszamboni.com/?p=201', 0, 'post', '', 0),
(206, 4, '2015-04-11 19:47:33', '2015-04-11 23:47:33', '<a href="http://www.tuftszamboni.com/wp-content/uploads/2015/04/Obama-Speech.jpg"><img class="alignnone  wp-image-211" src="http://www.tuftszamboni.com/wp-content/uploads/2015/04/Obama-Speech-300x193.jpg" alt="Obama Speech" width="474" height="305" /></a>\n\nby Sam Weitzman\n\nWASHINGTON— President Barack Obama vowed to “degrade and ultimately destroy” the Integrated Student Information Service, more commonly known by its acronym, iSIS. In a speech to the nation late that evening, the president pledged\nto bring an end to Tufts University’s new online portal for student life.\n\nEmploying tersely worded rhetoric, Obama called iSIS “a network of death” and “a terrorist organization, pure and simple.” The president cited its muddled dropdown menus, long loading times, and inaccessibility on online devices as specific reasons for the website’s complete annihilation. In a touching section of the speech, Obama focused on the tragic story of a young freshman whose hopes to take Chinese 1 were ended when iSIS failed to alert her of a conflict with Introduction to Community Health until it was too late. “As Americans,” Obama proclaimed, “we welcome our responsibility to lead.”\n\nAlthough exact details about the administration’s plans of action are still forthcoming, the president made it clear that he intends to assemble a coalition of the willing at an emergency meeting later this week in Brussels. Privately, senior administration members confirmed that Obama would continue U.S. drone strikes against high value targets in Davis Square, such as those hipsters working on compsci in Diesel, and has already placed special operations assault teams in Medford on standby.\n\nFor now, public opinion seems to be on the side of the administration. A poll published by the Pew YikYak Center found that over sixty percent of respondents agree with the statement that “iSIS sucks more than the walk to Mail Services,” although whether this bump in support for state action is only temporary remains to be seen.\n\nSpeaking to reporters after the speech, White House press secretary Josh Earnest labeled iSIS “a threat to innocent people everywhere” and “impossible to use unless you’re Stephen fucking Hawking.” These comments echoed earlier statements from Secretary of State John Kerry, who vowed that “those responsible for this heinous, vicious atrocity of a website will be held accountable.”\n\nPresident Anthony Monaco, a long-rumored financier of iSIS’ more radical elements, could not be reached for comment.', 'Obama Announces Plan to Defeat iSIS', '', 'publish', 'closed', 'open', '', 'obama-announces-plan-to-defeat-isis-by-sam-weitzman', '', '', '2015-04-11 19:47:33', '2015-04-11 23:47:33', '', 0, 'http://www.tuftszamboni.com/?p=206', 0, 'post', '', 0),
(213, 4, '2015-04-11 19:55:09', '2015-04-11 23:55:09', 'by Ryan Hastings-Echo\n\nSOMERVILLE, MA—Mixed congratulations through the entertainment world Wednesday as Al Qaeda, best known for their work in the “Osama bin Laden” viral video campaign, received a local Emmy for lifetime achievement in television.\n\nThe Lifetime Achievement Award, usually reserved for those The Academy believes have their best work behind them, comes as a huge shock to the anti-American terrorist group. “We had no idea people felt this way about our work,” said Sayed Rahdmatullah Hashemi, former Taliban envoy to the United States. “We have been striving always to put our best foot forward, but the loss of such a talented leading man in bin Laden was admittedly a large blow.”\n\n“The militant fundamentalist and sketch comedy group has not produced any noteworthy videos since the bin Laden campaign,” said Stuart E. Jones, US ambassador to Jordan and local Emmy guest judge. “It was time to recognize what they had accomplished and move on.” He cites the group’s low budget filming and editing practices as a reason for their decline in popularity; however, he says he “[respects] their innovative use of the press to popularize their earlier work.”\n\nWhen asked what Al Qaeda will do moving forward, Hashemi was reluctant to answer; however, he mentioned that the Taliban has made an offer to purchase Al Qaeda for an undisclosed sum, and that higher ups in Al Queda were “seriously considering” their offer. “We share common goals both as international ter- rorist syndicates and as artists,” said Mullah Muhammad Hasan Rehmani, newly appointed Taliban representative, in a statement. “We are excited to welcome al queda into the Taliban family.”\n\nBoth the Taliban and Al Qaeda are currently holding open casting calls for fresh talent. Think you have what it takes? Apply online or in person in Jordan, Yemen, Pakistan, and Afghanistan.', 'Al Qaeda Wins Local Emmy', '', 'publish', 'closed', 'open', '', 'al-qaeda-wins-local-emmy-by-ryan-hastings-echo', '', '', '2015-04-11 19:55:09', '2015-04-11 23:55:09', '', 0, 'http://www.tuftszamboni.com/?p=213', 0, 'post', '', 0),
(214, 5, '2015-04-11 20:07:24', '2015-04-12 00:07:24', '<div class="page" title="Page 17">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\nby Asha Norman-Hunt\n\nHave you ever stopped to watch a runner? I’m not talking about watching its strangely sinewy chicken legs swish back and forth under its way-too-short shorts, or the aggressive breaths it takes as if to say “Who’s sweating? NOT ME!” I’m not even talking about that annoying little jogging-in-place thing it does at every curb (we get it. It’s waiting for the light and burning calories at the same time. I’ll just watch while I eat this Dunkin’ Donuts), I am asking if you’ve ever noticed the way that a runner constantly seems to be looking for something. Have you ever wondered what it’s looking for?\n\nWhile the swivel of its head could just be a very awkward way to look at the scenery, it is much more likely that the lonely 6am Sunday runner is looking for someone to share in the brisk cool air with (because let\'s face it—who the fuck wants to run on Sunday morning?)\n\nSophie Lattes, Tufts track and field member and 10 mile runner (OW!) says that moving her head like a confused owl helps her meet new people in the morning when her other friends are asleep in their beds (YES).\n\n“It’s nice to see other runners around me. I mean I don’t know them or anything but I can creepily just run with them, right?Well, more like, follow them. That’s cool, right?”\n\n</div>\n</div>\n<div class="layoutArea">\n<div class="column">\n\nAnother strange trait of the untamed runner is its ability to appear exactly where it has no place being. Ever been out for a nice morning drive? The sun is shining, the birds are singing and you swivel to avoid a sweaty, oncoming human, barreling at you and gasping for air? Of course you have. Because runners think they are cars with slow as fuck wheels so they can’t tell the difference between sidewalk and asphalt. So the next time you’re out, observe the runner in its natural state. The Zamboni encourages you to watch its homing beacon head swivel and its inability to not get hit by a car and thank God for your morbid obesity.\n<div class="page" title="Page 17">\n\n[caption id="attachment_222" align="alignnone" width="300"]<a href="http://www.tuftszamboni.com/wp-content/uploads/2015/04/Runners.jpg"><img class="wp-image-222 size-medium" src="http://www.tuftszamboni.com/wp-content/uploads/2015/04/Runners-300x200.jpg" alt="Runners" width="300" height="200" /></a> A pack of runners searching for the nearest IHOP[/caption]\n\n</div>\n</div>\n</div>\n</div>\n</div>', 'Runners: What are They Looking For?', '', 'publish', 'closed', 'open', '', 'runners-what-are-they-looking-for-by-asha-norman-hunt', '', '', '2015-04-11 20:07:24', '2015-04-12 00:07:24', '', 0, 'http://www.tuftszamboni.com/?p=214', 0, 'post', '', 0),
(217, 4, '2015-04-11 20:08:39', '2015-04-12 00:08:39', 'by Connor des Rochers\n\nMEDFORD, MA—Where there once stood a beautifully dilapidated colonial house at 75 Ossipee Road, now only a charred shell remains. On September 29, first year Fletcher student Petunia Ogilvie set fire to her own apartment, citing irreconcilable differences with the wall color in her living room and bedroom.\n\nOne week prior to the incendiary incident, Ms. Ogilvie and her landlord, Igor Strauss, had discussed repainting during the apartment’s renovation. While she requested a nice eggshell col- or to go with her tan drapes and black bureau, she was horrified to find the space had been given a fresh coat of peach paint.\n\nWhen asked about the traumatic incident, Ogilvie recounted, “So I walked in expecting a matte dreamland and all of the sudden it’s like BAM! Somebody threw up tapioca pudding all over\nmy walls or some shit. And it’s like, too close to beige for me to have complained, but like this really fucked with my overall color scheme. I felt like I was living in a Golden Girls-themed hells- cape!”\n\nAfter a few nights with the “unbearable” walls, Petunia decided to take matters and matches into her own hands and give the apart- ment a scorched earth color scheme instead. On the night of the 29th, she gave all of her roommates, except for Linda Fink, plenty of time to evacuate the premises and then proceeded to reduce the house to a hollow, burnt out shell.\n\n&nbsp;\n\n<a href="http://www.tuftszamboni.com/wp-content/uploads/2015/04/the-thing-kate-flamethrower2.jpg"><img class="alignnone  wp-image-218" src="http://www.tuftszamboni.com/wp-content/uploads/2015/04/the-thing-kate-flamethrower2-300x200.jpg" alt="the-thing-kate-flamethrower2" width="981" height="425" /></a>\n\n“Linda is a troll who doesn’t do her dishes. She won’t be missed,” stated all three of the surviving roommates in separate interviews. The Somerville chief of police seemed to agree, and has decided to press charges against Ms. Ogilvie for her act of arson only.\n\nWhile neighbors are complaining about the smoking wreck, call- ing it an “eyesore” and a “hazard to children,” some local members of the community are viewing this event as an opportunity. Local meth lord Barry Davidson has expressed interest in converting the space into a local drug den for his homeless gang of addicts. “Our community has been look- ing for a space to call our own for quite some time, and we are excited about the opportunity to become a part of the Tufts family. The space has such a raw quality to it, and I really see a future for our drug culture to flourish in such a creative environment.”\n\nWhen asked if burning down her apartment was worth the trouble, Ogilvie stated, “At least the prison walls are a soothing grey. That will definitely match my drapes!”\n\nZamboni reporters were unable to reach landlord Igor Strauss for comment on the tacky and retro-in-a-bad-way color snafu, as he is currently on a three week “family vacation” in Crimea.', 'Fletcher student burns down apartment, citing “tacky” paint job', '', 'publish', 'closed', 'open', '', 'fletcher-student-burns-down-apartment-citing-tacky-paint-job-by-connor-des-rochers', '', '', '2015-04-11 20:08:39', '2015-04-12 00:08:39', '', 0, 'http://www.tuftszamboni.com/?p=217', 0, 'post', '', 0),
(224, 5, '2015-04-11 20:19:27', '2015-04-12 00:19:27', '<div class="page" title="Page 17">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n<div class="column">\n\nDear Tufts Community,\n\n</div>\nThere are times when I am proud to wear baby blue and brown. Instances like quidditch matches, weekend celebrations with only 2014’s best Top 40’s hits like Robyn Rihanna Fenty’s “Pon de Replay”, eating gluten free steak and eggs, and reading Sylvia Plath before baking brownies. I have been a member of VOX, Habitat for Humanity, Liberty In North Korea, Palestinians for Ukraine, Freedom in Macau, Jumbos Against Gouda, Stand for Jennifer Lawrence, No on Proposition 45, Invisible Children, and, most importantly,Tobacco Free Initiative since I became a student at our fine institution. As I write this, however, I shed tears all over my Macbook Pro with shame and embarrassment for our school.\n<p class="alignnone size-medium wp-image-226">While I was in Davis Square buying Totino’s Pizza Bites and drinking chocolate, I observed the homeless that populate the area. These members of our society are ignored constantly for no good reason. They are human beings; the homeless are living, breathing people who can get 20% off the new Totino’s Bold Buffalo Style Chicken Rolls. Or, if they would like to take a trip down south, they should try the Jalapeno Popper Rolls, exclusively from Totino’s.But they were not enjoying the ooey, gooey goodness of a pizza roll. Instead, I found our homeless smoking cigarette after cigarette. We live in 2014; it’s been almost 50 years since the Surgeon General’s warning about the dangers of tobacco use. And yet, our neighbors in Davis are smoking more than ever, pushing themselves closer to the brink of death.</p>\n\n</div>\n<div class="column">\n\nI come to you, Tufts students, to rise up against the genocide that is being supported through the silence of the ad- ministration! We must do what all good and altruistic organizations do and raise money to make Davis Square a smoke-free area! We must work tirelessly to make the homeless of Somerville put down their packs so they can rejoice in sobriety of tobacco and breathe a little better! With your help, we can set up pizza parties around campus starring Totino’s Canadian Bacon Party Pizza and possibly, if everyone gets on board, we can have a Totino’s Presents: Tufts Against Homeless Tobacco Use concert with pop singers like Pharrell Williams of Despicable Me 2 and Jason Derulo. With your help, we can make the difference!!!\n\nFrom,\n\nAnonymous (‘16)\n\n</div>\n</div>\n</div>\n</div>', 'Op Ed: We Need to Help the Davis Homeless (Sponsored by the Pizza Bites Against Tobacco Use Lobby)', '', 'publish', 'closed', 'open', '', 'op-ed-we-need-to-help-the-davis-homeless-sponsored-by-the-pizza-bites-against-tobacco-use-lobby', '', '', '2015-04-11 20:19:27', '2015-04-12 00:19:27', '', 0, 'http://www.tuftszamboni.com/?p=224', 0, 'post', '', 0),
(225, 4, '2015-04-11 20:15:19', '2015-04-12 00:15:19', 'by Emily Garber\n\nBOSTON, MA—Better get ready to step up your game, Boston, as a new night club will be entering your ranks early this winter. The ever-famous Phallus Palace, of Dallas, TX, has been confirmed to be opening up a satellite branch downtown. “It will be an interesting adjustment,” comments Phallus Palace’s Alice Fingerton, “But I definitely think that there is a place in Boston for our famous southern charm.”\n\nHowever, the opening hasn’t been eagerly anticipated by other area club owners. “Southern charm?” said a huffy Daniel McShortstack, of the infamous Spleendor, “Word is that at Phallus Palace, malice is the policy. If you ask me, Phallus Palace’ll be ballast for this club scene: dead weight.”\n\nBallast or not, this new club appears to be bringing in a lot of great events before Christmas. According to Fingerton, “We’ll have deals and theme nights for the ages, like our incredibly popular (and collectable!) Phallus Palace chalices! You drink it, you keep it! Or our night for the older folks, Phallus Palace Dialysis night, where we even hand out specially ordered Phallus Palace Cialis.” Fingerton also continued to hint at other events that are still under wraps, such as Phallus Palace Talisman nights, for the gentlemen. “Plus, soon Boston will be no stranger to what we in Dallas refer to as a Phallus Palace callus,” Fingerton finished with a giggle.\n\nThis winter, you can decide for yourself if Phallus Palace will inspire a new genre of nightclub or if its chubby will slowly fade like notorious failures such as Darned Socks and Minutia. Coming soon!', 'New Night Club Erected', '', 'publish', 'closed', 'open', '', 'new-night-club-erected-by-emily-garber', '', '', '2015-04-11 20:15:19', '2015-04-12 00:15:19', '', 0, 'http://www.tuftszamboni.com/?p=225', 0, 'post', '', 0),
(228, 4, '2015-04-11 20:23:49', '2015-04-12 00:23:49', 'by Brian Rose\n\nTHURSDAY — Tufts University students have grown accustomed to receiving emails each semester detailing various break-ins and other crimes occurring in the surrounding area – only some of which actually involve the university or its students. Still, the student population, voted the most safety-conscious in the nation according to a recent Zamboni poll, was thrilled by the announcement this week that the Tufts University Police Department plans to triple the number of safety alerts it emails to students this fall. However, in order to reach the new quota students have been clamoring for, TUPD has now lowered the standard by which safety alert emails will be sent out.\n\nIn the past, students have only been notified when there are notable incidents, such as home invasions, muggings, or cars spontaneously combusting before football games. Now, according to TUPD Sergeant Neil Hagerty, students will begin receiving breaking news emails alerts about a winder range of threatening events and activities such as “Lawn grass reaching dangerously high levels at 268 Boston Avenue,” “Biology professor sneezes, twice,” and “Loner reading book outside Brown &amp; Brew could be planning evil deed.”\n\n“Students made it clear to us they were unsatisfied with the previous number of email alerts, and we at TUPD pride ourselves on being a student-friendly organization,” said Hagerty. “We now welcome news tips from students, faculty members, fictional characters from Target-bought romance novels, parties of six or greater, Tumblr pages dedicated to Sherlock slash fiction, and beings of higher power.”\n\n&nbsp;\n\n<a href="http://www.tuftszamboni.com/wp-content/uploads/2015/04/tupd-police-car.jpg"><img class="alignnone  wp-image-229" src="http://www.tuftszamboni.com/wp-content/uploads/2015/04/tupd-police-car-300x111.jpg" alt="tupd police car" width="560" height="207" /></a>\n\n&nbsp;\n\nAnticipating that the campus soon to become gripped by a state of fear due to the increased notifications of crime, TUPD has also announced plans to expand the GoSafe ride service into regular school hours.\n\nSo far, reception to the program has been largely positive. “I feel so much safer now,” declared sophomore Julia Nagle. “I’m definitely calling GoSafe at noon tomorrow to take me from Haskell to Tilton.”\n\nDespite the changes to the email alert system, an unnamed university official said students will continue to not be notified of sexual assaults on campus.', 'TUPD Plans to Triple the Number of Safety Alert Emails', '', 'publish', 'closed', 'open', '', 'tupd-plans-to-triple-the-number-of-safety-alert-emails-by-brian-rose', '', '', '2015-04-11 20:23:49', '2015-04-12 00:23:49', '', 0, 'http://www.tuftszamboni.com/?p=228', 0, 'post', '', 0),
(232, 5, '2015-04-11 20:29:05', '2015-04-12 00:29:05', '<div class="page" title="Page 16">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\nby Lucas Zerah\n\n5) Houston Hall First Floor\n\nDon’t be fooled by this Freshman dorm; Houston is home to some of the best toilets on campus. However, through extensive research, I have found that there is a certain atmosphere to the men’s restroom on the first floor that beats the rest. The puddles of shower water and asparagus-scented piss compliment the shit-covered toilet paper that hangs off of the toilet seat. Need something to drink while you’re doing your business? Every Houston bathroom is stocked with two day-old St. Ides Malt Liquor, nonbreakable bottle of course.\n\n4) Haskell Hall Third Floor\n\nYes, we literally mean you can shit while walking the halls of Haskell!!! Ever since the RAs won their appeal to TCU for in-hall defecation last year, pooping in the halls has caught on like wildfire! The Third Floor has taken this trend a step further by making a ritual out of communal crapping. Every third Thursday of the month, all of the students gather for the exorcism of a chosen chicken. As the chicken is compelled by the power of our lord, the third floor Jumbos evacuate their bowels all over the floor! It is a sight to behold.\n\n</div>\n<div class="column">\n\n3) At a GIM for any Club\n\nIt doesn’t matter which club, as long as you need to do number two! Here at Tufts, bold and quirky personalities are adored. However, some might get the nerves and not shine as bright as they could. Break the ice by plopping one down at the meeting. People will rejoice in awe and respect, for your bold and quirky attitude!\n\n2) Carmichael Carvery Station\n\nNo one will ever know the difference...\n\n1) Inside your Mind\n\nLet’s face it: every time you squat down to take the Browns to the Super Bowl, you waste precious time that can be used for self-reflection. The best shit that can be taken at Tufts is the one that looks inside the shitter, not the shit. I recommend smoking 75x salvia before pooping at any of the before-mentioned places for a truly introspective and surreal evacuation of both the colon and reality as we know it.\n\n</div>\n</div>\n</div>\n</div>', 'Top 5 Places to Shit at Tufts', '', 'publish', 'closed', 'open', '', 'top-5-places-to-shit-at-tufts-by-lucas-zerah', '', '', '2015-04-11 20:29:05', '2015-04-12 00:29:05', '', 0, 'http://www.tuftszamboni.com/?p=232', 0, 'post', '', 0),
(236, 4, '2015-04-11 20:33:44', '2015-04-12 00:33:44', 'by Ethan Hartzell\n\n&nbsp;\n\nMEDFORD – Having just left his Tuesday Principles of Economics Recitation, Tufts sophomore Josh Lent approached freshman student and Connecticut-native Aisha Abdulrabb, who was diagonally crossing Talbot Avenue to return to her residence hall. Abdulrabb allegedly did not notice that the sophomore male was approaching her until he was just inches from her face.\n\n“Hey! Um, I just wanted to let you know, just in case you didn’t realize, but your toes and a good portion of the top of your feet are totally visible to me. I identify as male and use the He pronoun series... I don’t know if you have that in Arabic... I hope it doesn’t offend you too much that I, a male, saw you like this, and I wanted to let you know so you could fix it. Have a terrific day!” are Lent’s reported words.\n\nLent later reflected on Abdulrabb’s lack of gratitude, “it’s thankless work informing people of their ignorance, but someone’s got to do it.”\n\n&nbsp;\n\n<a href="http://www.tuftszamboni.com/wp-content/uploads/2015/04/Fletch-Study-Group.jpg"><img class="alignnone wp-image-238 size-medium" src="http://www.tuftszamboni.com/wp-content/uploads/2015/04/Fletch-Study-Group-300x225.jpg" alt="Fletch Study Group" width="517" height="389" /></a>\n\n<h6>                                                                                              "Active Global Citizenship"</h6>', 'Well-Meaning IR Major Informs Muslim Student That She Is Wearing Open-Toed Shoes', '', 'publish', 'closed', 'open', '', 'well-meaning-ir-major-informs-muslim-student-that-she-is-wearing-open-toed-shoes-by-ethan-hartzell', '', '', '2015-04-11 20:33:44', '2015-04-12 00:33:44', '', 0, 'http://www.tuftszamboni.com/?p=236', 0, 'post', '', 0) ;
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(239, 5, '2015-04-11 20:48:36', '2015-04-12 00:48:36', '<div class="page" title="Page 15">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n<div class="page" title="Page 15">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\nby Rachel Rappaport\n\n&nbsp;\n\n<strong>6am:</strong> Dawn breaks and the brilliant colors of the sunrise fade to a soft light that is cast upon my visage. The air is cool and the dewy grass around me provides a cushion for my head and my thoughts. I reflect upon the beauty of Mother Earth and the stillness of a calm morning and—wtf turn off that fukin lawnmower what the fuck is wrong with u r u kiddin me its 6 in the fuckin morning im goin 2 kill u stop wakin everybody up u stupid lil bitch its to early 4 this shit\n<div class="page" title="Page 15">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\n<strong>10:30am:</strong> Happiness swirls around me as cheery crowds head off to start their day of learn- ing. I hope their brains can expand with new knowledge and ideas. Oh, how I watch the eager bounce in their step. Poetry fills my mind. Perhaps, a haiku!\n<div class="page" title="Page 15">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n<p style="text-align: center;"><em>Longing for the walks                                                                                                                                                                               Of my forest childhood                                                                                                                                                                                                                            With mother acorn</em></p>\n<p style="text-align: center;"><em>Nestled in the leaves                                                                                                                                                                                            Snow falling like Earth’s good grace                                                                                                                                                                                Sun low in the winter sky</em></p>\n<p style="text-align: center;"><em>          Reminiscing slow    </em><em>                                                                                                                                                                                      Yearning the clutch of my youth                                                                                                                                                  Someone just scratch my balls</em></p>\n\n<div class="page" title="Page 15">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\n<strong>4:30pm:</strong> As the afternoon sun rests low in the sky, boots crunch over fallen leaves and autumn beckons with its warm glow and sweet scent. Above me a baby bird is born; I hear its cry. Oh how beautiful life is! Oh how fragile! Oh how—who the fuck are you don’t fuckn stand next to me??? Ur blockin my view what r you doing here?? Ugh sersiouly R u fukin kidding me dont put that shitty ass chalk drawing here gtfo no i wont fuckin vote u for senate who do you think u r no ones gonna vote ur punk ass for snate gtfo go fuk urself shit my nose itches ugh my nose...\n<div class="page" title="Page 15">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\n<strong>11pm:</strong> The night is calm and quiet save for the soft, crisp voices of birds settling down and the slight whisper of the wind through the leaves. This most peaceful time allows me ponder on my SYMPTOMS OF GONORRHEA, INCLUDING DISCOLORED DISCHARGE FROM THE PE- NIS, PAIN OR BURNING DURING URINATION, AND SWOLLEN TESTICLES OR THROAT GLANDS. IF YOU EXPERIENCE ANY OF THESE SYMPTOMS CONTACT YOUR LOCAL HEALTHCARE PROVIDER IMMEDIATELY TO GET TESTED.\n\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>', '￼A Day in the Life of the Acorn Head Statue', '', 'publish', 'closed', 'open', '', 'a-day-in-the-life-of-the-acorn-head-statue-by-rachel-rappaport', '', '', '2015-04-11 20:48:36', '2015-04-12 00:48:36', '', 0, 'http://www.tuftszamboni.com/?p=239', 0, 'post', '', 0),
(249, 4, '2015-04-11 20:50:15', '2015-04-12 00:50:15', '<a href="http://www.tuftszamboni.com/wp-content/uploads/2015/04/space-virgin.jpg"><img class="alignnone size-medium wp-image-250" src="http://www.tuftszamboni.com/wp-content/uploads/2015/04/space-virgin-300x260.jpg" alt="space virgin" width="300" height="260" /></a>\n\nby Ryan Hastings-Echo\n\nIn a surprising break from tradition, representatives from NASA announced Tuesday that they will be sending Hugh Spellman, a balding, unemployed virgin, to the international space station with the next launch. While the move is controversial in the field, experts suspect that Spellman may be the key to finding extraterrestrial life. “We have recently come to realize that our methodology in seeking contact with aliens is faulty,” said the representative. “All of our current astronauts are at the peak of intellectual and physical fitness—really the kinds of people we, at NASA, have never been able to approach at parties. Quite frankly, we believe that the aliens may be intimidated.”\n\nWhen asked how he felt about being the ambassador for the human race, Spellman was hesitantly optimistic. “I almost backed out when I learned that the aliens may not look like anatomically correct green women,” he said, “but my mom really talked me into it. I feel like I owed her something nice since I couldn’t finish law school.”\n\nTo prepare for his six-month stay in the international space station, which orbits the earth once every 90 minutes, Spellman has been placed on a strict training regimen. “My mom is trying to make me healthier meals, and I ordered a shake weight,” Spellman said. “It should be here in four to six business days. I’ve already lost two pounds.”\n\nDave Sanders of NASA described the rigorous selection process used by his team to determine which one of over 500 applicants would be selected for the mission. Prospective astronauts were first subjected to an intense psychological test to determine who would be the most suited as ambassador of all of Earth by NASA’s extremely high standards. “The man or woman selected had to prefer the original ‘Star Trek’ to ‘The Next Generation,’ but also had to believe that Picard was superior to Kirk,” said Sanders. “Spellman was a standout from the beginning because of his ability to burp, verbatim, the entire first episode of ‘Battlestar Galactica.’” Applicants were then given a test to discover which of them could bear the isolation and detachment many astronauts feel in space. “We lost most of our most promising candidates when we told them they would be missing the premier of the new ‘Spider- man’ movie. At that point, Spellman was the obvious choice.”\n\nDespite the optimism of many at NASA, there are some who urge caution when dealing with extraterrestrial life. Manny Breer is an astrophysicist with NASA and an amateur philatelist. “In ‘The Amazing Spider Man’ issue 48, the aliens who came to Earth were hostile and saw human beings as food,” he told reporters. In response to concerns shared by Breer and many other employees, NASA released a statement outlining that, in ‘X-men’ issue 57 and ‘Batman’ Issue 174, aliens were proven to be friendly and helpful. They also urge naysayers to recognize the fact that Superman, Man of Steel, is from the planet Krypton. “I don’t see what the big deal is,” said Breer, “Batman is obviously superior to Super- man.” When asked to explain NASA’s official stance on Batman vs. Superman, NASA representatives could not be reached for comment.', 'NASA to Send 38-Year-Old Virgin to Space', '', 'publish', 'closed', 'open', '', 'nasa-to-send-38-year-old-virgin-to-space-by-ryan-hastings-echo', '', '', '2015-04-11 20:50:15', '2015-04-12 00:50:15', '', 0, 'http://www.tuftszamboni.com/?p=249', 0, 'post', '', 0),
(255, 4, '2015-04-11 20:58:08', '2015-04-12 00:58:08', 'by Jake Halpin\n\nMedford--Tufts Administration officials have announced that they are launching a full-scale investigation into Carmichael Dining Hall proceedings after reports that members of the dining hall staff may have hacked into student iCloud accounts.\n\nBoston-Area news outlets began airing the story this morning, after Medford police revealed that 3 chefs had been brought into custody at around 2 am this morning. Reports say that the alleged chef-hackers stole hundreds of embarrassing high school photos and Photo Booth selfies, intending to use them to blackmail students into writing positive reviews of the dining hall cuisine.\n\nThe dining hall, which in recent years has been losing much ground to downhill rival Dewick MacPhie, made headlines at the end of the summer for their high-profile hiring of WikiLeaks co¬founder Julian Assange, who announced he would come out of hiding to run the prestigious uphill restaurant. In a highly controversial 60 Minutes interview, Carmichael President Peter Kourafalos admitted that a major consideration in Mr. Assange’s hiring was the money the dining hall would save due to Assange’s “Work-Study” status.\n\nIn an impromptu question and answer on the front steps of Gifford House, Tufts president Anthony Monaco said, “It is my hope that Tufts students will take this as a learning experience, and that after the fear of having those “chubby-phase” selfies being spread across social media, students will grow up and realize there’s a lot more to be concerned about than god¬damn air-conditioning.”', 'BREAKING: Tufts Dining Hacks Student iCloud Accounts', '', 'publish', 'closed', 'open', '', 'breaking-tufts-dining-hacks-student-icloud-accounts-by-jake-halpin', '', '', '2015-04-11 20:58:08', '2015-04-12 00:58:08', '', 0, 'http://www.tuftszamboni.com/?p=255', 0, 'post', '', 0),
(264, 5, '2015-04-11 21:10:34', '2015-04-12 01:10:34', '<div class="page" title="Page 14">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\nby Big Momma\n\n<strong>Where’s the bathroom?</strong>\n\nIt’s to the right and then to the left then to the right then straight then still straight then up the stairs then down the hallway then right then it’s the second door on your left.\n\n<strong>Is he cute?</strong>\n\nI mean he’s kinda cute but like he’s SO killing the Swampland theme so like you totally should.\n\n<strong>How do I use a condom?</strong>\n\nOk so like you take the wrapper off and then you like grab the slimy part of it and you put it or roll it or something onto the boner and then SCREW IT you’re on birth control, you’ll be fine.\n\n<strong>Should I take one more shot?</strong>\n\nI mean you drank a lot but that’s a Peppermint Patty shot so DO. THAT. SHIT. This one time I took one of those and like they totally missed my mouth pouring the syrup and I had chocolate sauce EVERYWHERE ugh it was so gross and it ruined my makeup.\n\n<strong>How many tater tots can I eat at 3 am?</strong>\n\nNo seriously like my makeup looked SO good but then that fucking chocolate sauce ruined it.\n\n<strong>What do I do about a guy who won’t stop talking to me?</strong>\n\nJust make out with another guy. It’s totally the best way to show him you’re not interested\n\n<strong>Can I steal this exit sign without people noticing?</strong>\n\nThat fireball is making you fucking invisible just go for it.\n\n</div>\n</div>\n</div>\n</div>', 'DRUNK ADVICE BY DRUNK GIRL$ FOR DRUNK GIRL$', '', 'publish', 'closed', 'open', '', 'drunk-advice-by-drunk-girls-for-drunk-girls', '', '', '2015-04-11 21:10:34', '2015-04-12 01:10:34', '', 0, 'http://www.tuftszamboni.com/?p=264', 0, 'post', '', 0),
(269, 5, '2015-04-11 21:14:31', '2015-04-12 01:14:31', '<div class="page" title="Page 14">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\nby Ben Pall\n\nStep 1\n\nStep 2\n\nStep 3\n\nStep 4\n\nStep 5\n\nStep 6\n\nStep 7\n\nStep 8\n\nStep 9\n\nStep 10\n\n</div>\n</div>\n</div>\n</div>', 'The Zamboni’s Exclusive, No-Nonsense, How-to Guide to Counting to 10', '', 'publish', 'closed', 'open', '', 'the-zambonis-exclusive-no-nonsense-how-to-guide-to-counting-to-10', '', '', '2015-04-11 21:14:31', '2015-04-12 01:14:31', '', 0, 'http://www.tuftszamboni.com/?p=269', 0, 'post', '', 0),
(273, 5, '2015-04-11 21:28:56', '2015-04-12 01:28:56', '<div class="page" title="Page 13">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\n<strong>The Virgin Mary</strong>\n\nThe Virgin Mary, class of 5 BC, is remarkably still just as committed to celibacy as Tufts University is.\n\n“That steel trap remains shut,” confirms her son Jesus Christ, class of AD 16. “In fact, she’s even begun promoting celibacy among others.” Although it remains unclear why the Virgin Mary’s son would be abreast of his mother’s sexual exploits, the information he provides is consistent with accounts from other sources.\n\n“She breaks into the Health Center to steal their condoms,” says Mr. Christ. Indeed, Health Center workers have been faced with a string of break-ins, during which nothing but condoms were taken. (pro chastity activist)\n\nJesus Christ also reveals that the Virgin Mary has begun to use her mysterious powers to infect sexually active students with chlamydia. “It’s awesome,” he says. “Some dude will just be walking by, and my mom will look at him, and WHAM! He’s got chlamydia.” Presumably, the Virgin Mary hopes that the discomfort associated with the disease will dissuade students from engaging in sexual activity, though it remains to be seen whether this strategy has been effective.\n\n“There is no guarantee that students will curtail all sexual activity upon receiving chlamydia from the Virgin Mary. It is my concern that her actions will simply cause the spread of chlamydia throughout the Tufts Campus,” says Health Center director Michelle Bowdler.\n\nAt Bowdler’s urging, posters have appeared around campus warning students of the signs of divine chlamydia and urging them to seek treatment. This canvasing of the campus is a visible testament to the fact that the Virgin Mary continues to influence the Tufts community millennia after her gradation. She thus joins the rank of many other notable alumni who have left lasting impressions on our campus.\n\n---\n<em>Zamboni reporter Aviva Schmitz contributed while on assignment in Nazareth</em>\n\n</div>\n<div class="column">\n\n<strong>Josef Stalin</strong>\n\nTufts University alumnus Joseph Vissarionovich Stalin (Class of 1914) has been called many names since becoming a Jumbo. Some of these names include “beanie baby visionary,” “immortal,” and of course “Brosef Ballin’.” Though he has dedicated the past few years of his post-college career to social justice work, in his early professional life Stalin pursued his love of music, culminating with his role as the producer of the award-less, introspective collection titled “Now That’s WhatI Call Music: Volume 4.” Throughout his career, Stalin has been a staunch supporter of classist structures, and his writings on such matters can be found in Tufts’ only magazine written entirely in binary, “|01001010|01101111|01110011|01100101|01110000|01101000|00100000|01010011|01110100|01100001|01101100|01101001|011 01110|00001101.” He holds three world records, two of which are undocumented, and is a generous donor to the Tufts Students Rescue Minorities From A Safe Distance organization. This intrepid Jumbo has appeared on multiple local access talk shows, informing communities about crisis matters such as the appropriation of sloths, kitten farming, and cheese. Tufts is honored to share the accomplishments of Joseph Stalin.\n\n<a href="http://www.tuftszamboni.com/wp-content/uploads/2015/04/886928227_orig.jpg"><img class="alignnone size-medium wp-image-275" src="http://www.tuftszamboni.com/wp-content/uploads/2015/04/886928227_orig-236x300.jpg" alt="Stalin" width="236" height="300" /></a>\n<div class="page" title="Page 13">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\nStalin, a beater for the Tufflepuffs, poses for his 1914 Memory Book portrait\n\n---\n\n<em>Information smuggled out of Siberia by Zamboni undercover reporter Pinar Yasar</em>\n\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>', 'Alumni: Where Are They Now? A Look at Tufts’ Most Accomplished ExPats', '', 'publish', 'closed', 'open', '', 'alumni-where-are-they-now-a-look-at-tufts-most-accomplished-expats', '', '', '2015-04-11 21:28:56', '2015-04-12 01:28:56', '', 0, 'http://www.tuftszamboni.com/?p=273', 0, 'post', '', 0),
(277, 5, '2015-04-11 21:37:49', '2015-04-12 01:37:49', '<div class="page" title="Page 12">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\nby Megan MacCallum\n\nPerhaps you’ve heard of the current Greek Life Trend—the ABC Party. This party, more commonly known as an Anything But Clothes Party, does not actually feature individuals arriving nude (though they may often end the night that way), but rather encour- ages the party-goers to make an outfit for the evening out of anything other than clothing.\n\nIf you have heard of these parties, you’re probably familiar with the typical approach most sorority girls take: trash-bag skirts, pillow- case tunics, and shower-curtain bodycon dresses. However, if you are looking to dress a little outside the box (literally you could wear a box) for your upcoming ABC party, The Zamboni has a list of costumes for you that are sure to slay.\n\n</div>\n</div>\n<div class="layoutArea">\n<div class="column">\n\n<strong>Tortilla Tits and Naughty Bits:</strong>\n\nShow up to the party wearing nothing but tortillas on your “private parts.” Because tortillas absorb water easily, once you get into the sweaty bump and grind of the fiesta, the tortilla should adhere itself to your skin fairly well. The downside of this costume, is when the late-night munchies kick in, your outfit could disappear. Or is this an upside...\n\n<strong>Natural Fibers:</strong>\n\nIf you are a lady with particularly long hair, or a gentleman with a particularly long beard, or even better, an individual with particularly long hair and a particularly long beard (read: Dumbledore), you may want to consider showing up to the party wearing just your own hair. If you condition well and often, the silken feeling of your own natural hair will make your skin tingle and the accessible breeze can make your other parts tingle as well.\n\n<strong>Declined:</strong>\n\nIf you don’t fall under the category of a “$uper Rich Kid,” and are actually in lots of debt, tape all of your current cards over your body to create the clothing item of your choice. Spread them all as thin as possible to get yourself covered.\n\n<strong>Stolen Mink Stole:</strong>\n\n</div>\n<div class="column">\n\nThis is a costume for risk-takers. Chances are, you don’t actu- ally own a mink stole of your own. If you do, you are not only a douche, but you are also irrelevant because a mink stole is technically an item of clothing and you couldn’t wear it to an ABC party anyway. However, you can locate the nearest mink farm near you, sneak there in the dead of night, and attempt to wrangle as many minkii as possible. If you rubber-band them together, you can create a beautiful live mink stole to wear to the party. This will not only get you noticed, but will also ward off any unwanted attention, all while making a statement about the cruel fur industry itself. (Note: Minkii are expensive. If you do not have access to a mink or two, ferrets can make a nice substitute.)\n\n<strong>$uper Rich Kids:</strong>\n\nTape Daddy’s old credit cards into one plastic dress or a pair of highly uncomfortable shorts.\n\n<strong>ABC costume:</strong>\n\nAs you know ABC can also stand for “Already Been Chewed.” We encourage you to chew as much gum as you can, or pick it off the sidewalks, and desks nearest you, and form a gooey, but adhesive ABC costume for your ABC party\n\n</div>\n</div>\n</div>\n</div>', 'Zamboni’s Guide to ABC Parties', '', 'publish', 'closed', 'open', '', 'zambonis-guide-to-abc-parties', '', '', '2015-04-11 21:37:49', '2015-04-12 01:37:49', '', 0, 'http://www.tuftszamboni.com/?p=277', 0, 'post', '', 0),
(280, 5, '2015-04-11 21:45:39', '2015-04-12 01:45:39', '<div class="page" title="Page 11">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\nby Justin Lanzberg\n\nUpon his return to the Oval Office from the kitchen galley and a heartwarming breakfast, President Barack Hussein Obama realized that something was amiss. Second in command Joseph Robinette Biden lay face down on the floor, sedated and de-clothed, with donkey painted on his head, which seemed to have been forcibly shaved, and with an inscription of the Declaration of Independence emblazoned on his back. The tattoos hadn’t yet dried, and Mr. Joseph R. Biden’s white shoulder blades had taken on an inflamed, sunset hue. President Obama’s first impulse was to turn Mr. Biden over on his back to see if an invisible treasure map lay on his stomach like Nicholas Cage did with the actual Declaration of Independence in “National Treasure”... But he thought against it because he knew that the cameras would capture him.... And messing with a crime scene of this stature could have the potential to surpass his good friend William Clinton’s 90’s blunder in infamy.\n\nUnsure of how to proceed, Mr. Obama then decided to take his bi-daily jog so he could clear his head and ponder how to handle the misfortune of his compatriot. On the run, he realized that Governor Willard Mitt Romney is a raging ass, hates civil asses all the same, and despises Mr. Biden’s smug grin, which was on full display during the campaign debates with Paul Ryan. But beyond that, he couldn’t draw any viable ties.\n\nWhen Mr. Obama finished up his half-mile loop of Pennsylvania Ave, he saw Mr. Clinton strutting through the memorial gardens, looking happy as ever. He spotted his daughter playing Carmelo Anthony in Ping-Pong off the east wing.\n\n</div>\n<div class="column">\n\nThrough the kitchen window he watched President Vladimir Vladimirovich Putin steal a cookie from his top shelf oatmeal raisin stash, which he thought he had hidden perfectly. And he also spotted House Speaker John Andrew Boehner crawling on the ground outside the Oval Office window, straining to see inside. But none of it seemed unusual.\n\nMr. Obama headed back towards his quarters and grabbed his favorite shower towel with a gold inscription of Mr. Biden’s beaming face and a speech bubble saying, “Love is unparalleled.” He passed his bed and noticed the bed sheets had been ruffled up, but again thought nothing of it. Something about the shower was off, however, which he immediately realized upon entering. It was the showerhead, cocked a few degrees to the side. It was as if he had fallen asleep for ten years and had missed all of what had happened, and then was debriefed on the period, in extreme detail, in under a minute.\n\nHe had gone on a sugar rage with Mr. Biden, Mr. Clinton, Mr. Putin and Mr. Boehner. They had bought 75 pounds of straight sugar cubes, liquefied them with water and drank them in a chug- ging contest. The sugar high hit them about five minutes later, and when it did, at 2am, when all else was quiet on the white house lawn, all hell broke loose. Mr. Biden immediately gravitated to his guilty pleasure in turning on “National Treasure 2: Book of Secrets” with Mr. Vladimirovich Putin. Sadly, Biden’s sugar high tolerance wasn’t as great as that of the others and he fell asleep halfway through. Mr. Putin nonetheless pressed on like a true pro and watched the entire movie. Towards the end, out of his newfound love for American Nationalism, Republicanism and Nicholas Cage movies, Mr. Putin hired a tattoo artist, ordered him to shave Mr. Biden’s head, paint a donkey on it and emblazon the Declaration of Independence across his back. Mr. Putin, with the greatest sugar tolerance of the entire group, continued on throughout the night into the morning in search of further sweets...\n\n</div>\n<div class="column">\n\nBoehner’s sugar high transported him back in time to his childhood years. He used to think of himself as a 007-esque spy kid, just appreciably diminished both in height and his capacity to be surreptitious. So, when Mr. Obama found John crawling outside the Oval Office window, his only conspiratorial thought was to infiltrate the door. Mr. Clinton had disappeared into the Kennedy gardens soon after his high set in. After hours of futile searching among the topiary trees for his buddy, Mr. Obama found Mr. Clinton sniffing teapots in the China room at 5am.\n\nSo, the showerhead, which Mr. Obama had reached for as he lost his footing while playing “name the papal pervert” with Mr. Clinton at 5:30am, had in fact been the key. Mr. Obama’s daughter and Carmelo Anthony playing table tennis was nothing out of the ordinary\n\n</div>\n</div>\n</div>\n</div>', 'Celebrity Followings: Just Another Day in the Office', '', 'publish', 'closed', 'open', '', 'celebrity-followings-just-another-day-in-the-office', '', '', '2015-04-11 21:45:39', '2015-04-12 01:45:39', '', 0, 'http://www.tuftszamboni.com/?p=280', 0, 'post', '', 0),
(283, 5, '2015-04-11 21:53:11', '2015-04-12 01:53:11', 'by Anthony P. “Tony” Monaco\r\n\r\nIn the years that I have been a member of the Tufts community, I have been truly astonished and impressed by a student body that is passionately involved in academics. Whether it’s mixing new alternatives to fossil fuels, flinging up zip lines and flash mobs, or crunching numbers in the computer lab, Tufts students are passionately engaged in their academic world. But sometimes, being an active Tufts citizen can get in the way of acknowledging my personal accomplishments. So, I thought I’d share my proudest moment of my career as a geneticist: discovering the double helix.\r\n\r\nBefore my Fateful Shrooms Trip, nobody knew what DNA looked like. Generations of scientists had tried for years to crack the code, and many of them went mad in the process. It was the 70s, times were changing, and a lot of new ideas were floating around the scientific community back then. Some people thought that DNA covered the cell like a Milk-Dud covers stale caramel or an M&amp;M covers chocolate. Otherwise, it was hypothesized that DNA was actually microscopic creatures, and that perhaps there were other small creatures elsewhere in the body. People were questioning their beliefs and experimenting--in the laboratory.\r\n\r\nSo one day, a few friends of mine and I bought about an ounce of Peruvian Godseeds and drove up to Big Sur\r\nin our van, Icarus. Just like you Jumbos on an average weekend, we were already a little bit cryodomed and were looking to fully disassociate; we headed up into our mountain cabin, munched down the blitzcandies, and started to drop out.\r\n\r\nAt first, I felt of the way many Tufts students feel during their first days on campus...I was sweating profusely, my pupils were dilated, and I was having trouble distin-guishing reality from hallucination. After watching the purple sunset bathe the mountains while contemplating\r\n\r\nYes’s seminal 1972 album Close to the Edge, I began to grapple with the problem that had defeated Leibniz, Fermi, and even Einstein before him: the structure of the DNA molecule. I felt my body disintegrate, and I began to expand my conscious awareness beyond my own corporeal boundaries and into the surrounding wilderness. Suddenly, I realized that human life is a constant struggle between the primal animal that seeks to conserve itself, and the higher conscious mind that desires the nullification of physical constraints. The double helix followed very naturally from this battle, and appeared in the sky above the Santa Lucia mountains.\r\n\r\nI published my work and revolutionized the field of genetics, like many of you Jumbos will after graduation,\r\n\r\nStay active, stay involved with global issues. But don’t forget to seek out meaningful experiences in your own life, and to always pursue those things that interest you. Like Shrooms. You never know, you might make a career out of it. I sure did.\r\n\r\n[caption id="attachment_285" align="alignnone" width="317"]<a href="http://www.tuftszamboni.com/wp-content/uploads/2015/04/Screen-Shot-2015-04-11-at-9.56.58-PM.png"><img class="wp-image-285" src="http://www.tuftszamboni.com/wp-content/uploads/2015/04/Screen-Shot-2015-04-11-at-9.56.58-PM-225x300.png" alt="Screen Shot 2015-04-11 at 9.56.58 PM" width="317" height="423" /></a> Monaco\'s handwritten notes, written upon his breakthrough realization[/caption]', 'How I Discovered the DNA Double Helix While Tripping on Shrooms', '', 'publish', 'closed', 'open', '', 'how-i-discovered-the-dna-double-helix-while-tripping-on-shrooms', '', '', '2015-04-28 00:55:57', '2015-04-27 22:55:57', '', 0, 'http://www.tuftszamboni.com/?p=283', 0, 'post', '', 0),
(307, 5, '2015-04-12 14:04:24', '2015-04-12 12:04:24', '<div class="page" title="Page 5">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\n&nbsp;\n\nWashington, DC- A recent study from Columbia University has shown a positive correlation between American citizens burning their breakfast toast and Obama. The study, led by Dr. Sec Ondamend-Ment of the Columbia Astrology department and published last month in the Primary Source journal, examined the breakfasts of 1200 American families. The results showed that after the election of Obama, the likelihood of toast burning while in the toaster increased by 500%.\n\nThis new information comes as a shock and a relief to many Amer- icans who have been struggling with burnt toast for several years. “I always had a suspicion it was Obama’s fault that my toast burned every morning,” said Buck Hammerville of Burgin, Kentucky. “I used to think that it was the gays or the immigrants, or maybe the fact that I set my toaster to ‘high’ and left the room for 20 minutes.”\n<div class="page" title="Page 5">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\nAddressing the logistics of the study, Dr. Ondamendment notes that the results were replicated across three different sample groups to ensure accuracy. “All participants placed two slices of white bread, a truly delicious American product that has been unfairly targeted by the first lady’s ridiculous and unfounded health initia- tive, in their American-made toasters. Incidence of burning was recorded and later compared to the Obama variable. Our very sci- entific results clearly show a link between burnt toast and some- thing Obama did.”\n\nAccording to Dr. Ondamend-Ment, upcoming studies will address the possibility of links between increased immigration and un- even microwave heating, as well as a connection between cheating spouses and Benghazi.\n\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>', '', '', 'trash', 'closed', 'open', '', '307', '', '', '2015-04-21 03:46:38', '2015-04-21 01:46:38', '', 0, 'http://www.tuftszamboni.com/?p=307', 0, 'post', '', 0),
(308, 3, '2015-04-21 03:46:38', '2015-04-21 01:46:38', '<div class="page" title="Page 5">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\n&nbsp;\n\nWashington, DC- A recent study from Columbia University has shown a positive correlation between American citizens burning their breakfast toast and Obama. The study, led by Dr. Sec Ondamend-Ment of the Columbia Astrology department and published last month in the Primary Source journal, examined the breakfasts of 1200 American families. The results showed that after the election of Obama, the likelihood of toast burning while in the toaster increased by 500%.\n\nThis new information comes as a shock and a relief to many Amer- icans who have been struggling with burnt toast for several years. “I always had a suspicion it was Obama’s fault that my toast burned every morning,” said Buck Hammerville of Burgin, Kentucky. “I used to think that it was the gays or the immigrants, or maybe the fact that I set my toaster to ‘high’ and left the room for 20 minutes.”\n<div class="page" title="Page 5">\n<div class="section">\n<div class="layoutArea">\n<div class="column">\n\nAddressing the logistics of the study, Dr. Ondamendment notes that the results were replicated across three different sample groups to ensure accuracy. “All participants placed two slices of white bread, a truly delicious American product that has been unfairly targeted by the first lady’s ridiculous and unfounded health initia- tive, in their American-made toasters. Incidence of burning was recorded and later compared to the Obama variable. Our very sci- entific results clearly show a link between burnt toast and some- thing Obama did.”\n\nAccording to Dr. Ondamend-Ment, upcoming studies will address the possibility of links between increased immigration and un- even microwave heating, as well as a connection between cheating spouses and Benghazi.\n\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>', '', '', 'inherit', 'open', 'open', '', '307-revision-v1', '', '', '2015-04-21 03:46:38', '2015-04-21 01:46:38', '', 307, 'http://zamboni/?p=308', 0, 'revision', '', 0),
(310, 1, '2015-04-21 23:15:08', '2015-04-21 21:15:08', '\nby Anthony P. “Tony” Monaco\n\n</div>\n</div>\n</div>\n</div>\nIn the years that I have been a member of the Tufts community, I have been truly astonished and impressed by a student body that is passionately involved in academics. Whether it’s mixing new alternatives to fossil fuels, flinging up zip lines and flash mobs, or crunching numbers in the computer lab, Tufts students are passionately engaged in their academic world. But sometimes, being an active Tufts citizen can get in the way of acknowledging my personal accomplishments. So, I thought I’d share my proudest moment of my career as a geneticist: discovering the double helix.\n\nBefore my Fateful Shrooms Trip, nobody knew what DNA looked like. Generations of scientists had tried for years to crack the code, and many of them went mad in the process. It was the 70s, times were changing, and a lot of new ideas were floating around the scientific community back then. Some people thought that DNA covered the cell like a Milk-Dud covers stale caramel or an M&amp;M covers chocolate. Otherwise, it was hypothesized that DNA was actually microscopic creatures, and that perhaps there were other small creatures elsewhere in the body. People were questioning their beliefs and experimenting--in the laboratory.\n\nSo one day, a few friends of mine and I bought about an ounce of Peruvian Godseeds and drove up to Big Sur\nin our van, Icarus. Just like you Jumbos on an average weekend, we were already a little bit cryodomed and were looking to fully disassociate; we headed up into our mountain cabin, munched down the blitzcandies, and started to drop out.\n\n</div>\n<div class="column">\n\nAt first, I felt of the way many Tufts students feel during their first days on campus...I was sweating profusely, my pupils were dilated, and I was having trouble distin-guishing reality from hallucination. After watching the purple sunset bathe the mountains while contemplating\n\nYes’s seminal 1972 album Close to the Edge, I began to grapple with the problem that had defeated Leibniz, Fermi, and even Einstein before him: the structure of the DNA molecule. I felt my body disintegrate, and I began to expand my conscious awareness beyond my own corporeal boundaries and into the surrounding wilderness. Suddenly, I realized that human life is a constant struggle between the primal animal that seeks to conserve itself, and the higher conscious mind that desires the nullification of physical constraints. The double helix followed very naturally from this battle, and appeared in the sky above the Santa Lucia mountains.\n\nI published my work and revolutionized the field of genetics, like many of you Jumbos will after graduation,\n\nStay active, stay involved with global issues. But don’t forget to seek out meaningful experiences in your own life, and to always pursue those things that interest you. Like Shrooms. You never know, you might make a career out of it. I sure did.\n\n[caption id="attachment_285" align="alignnone" width="317"]<a href="http://www.tuftszamboni.com/wp-content/uploads/2015/04/Screen-Shot-2015-04-11-at-9.56.58-PM.png"><img class="wp-image-285" src="http://www.tuftszamboni.com/wp-content/uploads/2015/04/Screen-Shot-2015-04-11-at-9.56.58-PM-225x300.png" alt="Screen Shot 2015-04-11 at 9.56.58 PM" width="317" height="423" /></a> Monaco\'s handwritten notes, written upon his breakthrough realization[/caption]\n\n&nbsp;\n\n</div>\n</div>\n</div>\n</div>', 'How I Discovered the DNA Double Helix While Tripping on Shrooms', '', 'inherit', 'open', 'open', '', '283-autosave-v1', '', '', '2015-04-21 23:15:08', '2015-04-21 21:15:08', '', 283, 'http://zamboni/?p=310', 0, 'revision', '', 0),
(311, 1, '2015-04-21 23:03:11', '2015-04-21 21:03:11', '<div class="page" title="Page 10">\r\n<div class="section">\r\n<div class="layoutArea">\r\n<div class="column">\r\n<div class="page" title="Page 10">\r\n<div class="section">\r\n<div class="layoutArea">\r\n<div class="column">\r\n\r\nby Anthony P. “Tony” Monaco\r\n\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\nIn the years that I have been a member of the Tufts community, I have been truly astonished and impressed by a student body that is passionately involved in academics. Whether it’s mixing new alternatives to fossil fuels, flinging up zip lines and flash mobs, or crunching numbers in the computer lab, Tufts students are passionately engaged in their academic world. But sometimes, being an active Tufts citizen can get in the way of acknowledging my personal accomplishments. So, I thought I’d share my proudest moment of my career as a geneticist: discovering the double helix.\r\n\r\nBefore my Fateful Shrooms Trip, nobody knew what DNA looked like. Generations of scientists had tried for years to crack the code, and many of them went mad in the process. It was the 70s, times were changing, and a lot of new ideas were floating around the scientific community back then. Some people thought that DNA covered the cell like a Milk-Dud covers stale caramel or an M&amp;M covers chocolate. Otherwise, it was hypothesized that DNA was actually microscopic creatures, and that perhaps there were other small creatures elsewhere in the body. People were questioning their beliefs and experimenting--in the laboratory.\r\n\r\nSo one day, a few friends of mine and I bought about an ounce of Peruvian Godseeds and drove up to Big Sur\r\nin our van, Icarus. Just like you Jumbos on an average weekend, we were already a little bit cryodomed and were looking to fully disassociate; we headed up into our mountain cabin, munched down the blitzcandies, and started to drop out.\r\n\r\n</div>\r\n<div class="column">\r\n\r\nAt first, I felt of the way many Tufts students feel during their first days on campus...I was sweating profusely, my pupils were dilated, and I was having trouble distin-guishing reality from hallucination. After watching the purple sunset bathe the mountains while contemplating\r\n\r\nYes’s seminal 1972 album Close to the Edge, I began to grapple with the problem that had defeated Leibniz, Fermi, and even Einstein before him: the structure of the DNA molecule. I felt my body disintegrate, and I began to expand my conscious awareness beyond my own corporeal boundaries and into the surrounding wilderness. Suddenly, I realized that human life is a constant struggle between the primal animal that seeks to conserve itself, and the higher conscious mind that desires the nullification of physical constraints. The double helix followed very naturally from this battle, and appeared in the sky above the Santa Lucia mountains.\r\n\r\nI published my work and revolutionized the field of genetics, like many of you Jumbos will after graduation,\r\n\r\nStay active, stay involved with global issues. But don’t forget to seek out meaningful experiences in your own life, and to always pursue those things that interest you. Like Shrooms. You never know, you might make a career out of it. I sure did.\r\n\r\n[caption id="attachment_285" align="alignnone" width="317"]<a href="http://www.tuftszamboni.com/wp-content/uploads/2015/04/Screen-Shot-2015-04-11-at-9.56.58-PM.png"><img class="wp-image-285" src="http://www.tuftszamboni.com/wp-content/uploads/2015/04/Screen-Shot-2015-04-11-at-9.56.58-PM-225x300.png" alt="Screen Shot 2015-04-11 at 9.56.58 PM" width="317" height="423" /></a> Monaco\'s handwritten notes, written upon his breakthrough realization[/caption]\r\n\r\n&nbsp;\r\n\r\n</div>\r\n</div>\r\n</div>\r\n</div>', 'How I Discovered the DNA Double Helix While Tripping on Shrooms', '', 'inherit', 'open', 'open', '', '283-revision-v1', '', '', '2015-04-21 23:03:11', '2015-04-21 21:03:11', '', 283, 'http://zamboni/?p=311', 0, 'revision', '', 0),
(312, 1, '2015-04-21 23:15:38', '2015-04-21 21:15:38', 'by Anthony P. “Tony” Monaco\r\n\r\nIn the years that I have been a member of the Tufts community, I have been truly astonished and impressed by a student body that is passionately involved in academics. Whether it’s mixing new alternatives to fossil fuels, flinging up zip lines and flash mobs, or crunching numbers in the computer lab, Tufts students are passionately engaged in their academic world. But sometimes, being an active Tufts citizen can get in the way of acknowledging my personal accomplishments. So, I thought I’d share my proudest moment of my career as a geneticist: discovering the double helix.\r\n\r\nBefore my Fateful Shrooms Trip, nobody knew what DNA looked like. Generations of scientists had tried for years to crack the code, and many of them went mad in the process. It was the 70s, times were changing, and a lot of new ideas were floating around the scientific community back then. Some people thought that DNA covered the cell like a Milk-Dud covers stale caramel or an M&amp;M covers chocolate. Otherwise, it was hypothesized that DNA was actually microscopic creatures, and that perhaps there were other small creatures elsewhere in the body. People were questioning their beliefs and experimenting--in the laboratory.\r\n\r\nSo one day, a few friends of mine and I bought about an ounce of Peruvian Godseeds and drove up to Big Sur\r\nin our van, Icarus. Just like you Jumbos on an average weekend, we were already a little bit cryodomed and were looking to fully disassociate; we headed up into our mountain cabin, munched down the blitzcandies, and started to drop out.\r\n\r\nAt first, I felt of the way many Tufts students feel during their first days on campus...I was sweating profusely, my pupils were dilated, and I was having trouble distin-guishing reality from hallucination. After watching the purple sunset bathe the mountains while contemplating\r\n\r\nYes’s seminal 1972 album Close to the Edge, I began to grapple with the problem that had defeated Leibniz, Fermi, and even Einstein before him: the structure of the DNA molecule. I felt my body disintegrate, and I began to expand my conscious awareness beyond my own corporeal boundaries and into the surrounding wilderness. Suddenly, I realized that human life is a constant struggle between the primal animal that seeks to conserve itself, and the higher conscious mind that desires the nullification of physical constraints. The double helix followed very naturally from this battle, and appeared in the sky above the Santa Lucia mountains.\r\n\r\nI published my work and revolutionized the field of genetics, like many of you Jumbos will after graduation,\r\n\r\nStay active, stay involved with global issues. But don’t forget to seek out meaningful experiences in your own life, and to always pursue those things that interest you. Like Shrooms. You never know, you might make a career out of it. I sure did.\r\n\r\n[caption id="attachment_285" align="alignnone" width="317"]<a href="http://www.tuftszamboni.com/wp-content/uploads/2015/04/Screen-Shot-2015-04-11-at-9.56.58-PM.png"><img class="wp-image-285" src="http://www.tuftszamboni.com/wp-content/uploads/2015/04/Screen-Shot-2015-04-11-at-9.56.58-PM-225x300.png" alt="Screen Shot 2015-04-11 at 9.56.58 PM" width="317" height="423" /></a> Monaco\'s handwritten notes, written upon his breakthrough realization[/caption]', 'How I Discovered the DNA Double Helix While Tripping on Shrooms', '', 'inherit', 'open', 'open', '', '283-revision-v1', '', '', '2015-04-21 23:15:38', '2015-04-21 21:15:38', '', 283, 'http://zamboni/?p=312', 0, 'revision', '', 0),
(313, 3, '2015-04-21 23:22:10', '2015-04-21 21:22:10', ' ', '', '', 'publish', 'open', 'open', '', '313', '', '', '2015-04-21 23:22:10', '2015-04-21 21:22:10', '', 0, 'http://zamboni/?p=313', 1, 'nav_menu_item', '', 0),
(314, 3, '2015-04-21 23:22:10', '2015-04-21 21:22:10', ' ', '', '', 'publish', 'open', 'open', '', '314', '', '', '2015-04-21 23:22:10', '2015-04-21 21:22:10', '', 0, 'http://zamboni/?p=314', 2, 'nav_menu_item', '', 0),
(315, 3, '2015-04-21 23:22:10', '2015-04-21 21:22:10', ' ', '', '', 'publish', 'open', 'open', '', '315', '', '', '2015-04-21 23:22:10', '2015-04-21 21:22:10', '', 0, 'http://zamboni/?p=315', 3, 'nav_menu_item', '', 0),
(316, 3, '2015-04-21 23:22:10', '2015-04-21 21:22:10', ' ', '', '', 'publish', 'open', 'open', '', '316', '', '', '2015-04-21 23:22:10', '2015-04-21 21:22:10', '', 0, 'http://zamboni/?p=316', 4, 'nav_menu_item', '', 0),
(317, 3, '2015-04-24 03:53:12', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-04-24 03:53:12', '0000-00-00 00:00:00', '', 0, 'http://zamboni/?p=317', 0, 'post', '', 0),
(318, 1, '2015-04-26 23:08:16', '2015-04-26 21:08:16', '', 'facebook29', '', 'inherit', 'open', 'open', '', 'facebook29', '', '', '2015-04-26 23:08:16', '2015-04-26 21:08:16', '', 0, 'http://zamboni/wp-content/uploads/2015/04/facebook29.svg', 0, 'attachment', 'image/svg+xml', 0),
(319, 1, '2015-04-26 23:09:13', '2015-04-26 21:09:13', '', 'twitter2', '', 'inherit', 'open', 'open', '', 'twitter2', '', '', '2015-04-26 23:09:13', '2015-04-26 21:09:13', '', 0, 'http://zamboni/wp-content/uploads/2015/04/twitter2.svg', 0, 'attachment', 'image/svg+xml', 0),
(320, 1, '2015-04-28 21:01:34', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-04-28 21:01:34', '0000-00-00 00:00:00', '', 0, 'http://zamboni/?p=320', 0, 'post', '', 0) ;

#
# End of data contents of table `wp_posts`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_relationships`
#

DROP TABLE IF EXISTS `wp_term_relationships`;


#
# Table structure of table `wp_term_relationships`
#

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_term_relationships`
#
INSERT INTO `wp_term_relationships` ( `object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(6, 1, 0),
(9, 1, 0),
(12, 1, 0),
(15, 1, 0),
(18, 1, 0),
(18, 2, 0),
(20, 1, 0),
(20, 2, 0),
(22, 1, 0),
(26, 6, 0),
(26, 13, 0),
(26, 37, 0),
(26, 58, 0),
(46, 1, 0),
(91, 6, 0),
(91, 7, 0),
(91, 8, 0),
(170, 1, 0),
(195, 7, 0),
(195, 9, 0),
(195, 10, 0),
(195, 11, 0),
(195, 12, 0),
(195, 13, 0),
(195, 14, 0),
(195, 15, 0),
(195, 16, 0),
(195, 17, 0),
(201, 7, 0),
(201, 13, 0),
(206, 7, 0),
(206, 13, 0),
(206, 18, 0),
(206, 19, 0),
(206, 20, 0),
(206, 21, 0),
(213, 7, 0),
(213, 13, 0),
(213, 22, 0),
(213, 23, 0),
(213, 24, 0),
(213, 25, 0),
(213, 26, 0),
(213, 27, 0),
(213, 28, 0),
(213, 29, 0),
(213, 30, 0),
(213, 31, 0),
(213, 32, 0),
(213, 33, 0),
(213, 34, 0),
(213, 35, 0),
(214, 7, 0),
(214, 36, 0),
(214, 37, 0),
(214, 38, 0),
(214, 39, 0),
(214, 40, 0),
(214, 41, 0),
(217, 7, 0),
(217, 13, 0),
(217, 42, 0),
(217, 43, 0),
(217, 44, 0),
(217, 45, 0),
(217, 46, 0),
(217, 47, 0),
(217, 48, 0),
(217, 49, 0),
(217, 50, 0),
(217, 51, 0),
(217, 52, 0),
(217, 53, 0),
(217, 54, 0),
(217, 55, 0),
(224, 7, 0),
(224, 56, 0),
(224, 57, 0),
(224, 58, 0),
(224, 59, 0),
(224, 60, 0),
(224, 61, 0),
(225, 7, 0),
(225, 13, 0),
(225, 62, 0),
(225, 63, 0),
(225, 64, 0),
(225, 65, 0),
(225, 66, 0),
(225, 67, 0),
(225, 68, 0),
(225, 69, 0),
(225, 70, 0),
(225, 71, 0),
(228, 7, 0),
(228, 13, 0),
(228, 72, 0),
(228, 73, 0),
(228, 74, 0) ;
INSERT INTO `wp_term_relationships` ( `object_id`, `term_taxonomy_id`, `term_order`) VALUES
(228, 75, 0),
(228, 76, 0),
(228, 77, 0),
(228, 78, 0),
(232, 7, 0),
(232, 37, 0),
(232, 77, 0),
(232, 79, 0),
(232, 80, 0),
(232, 81, 0),
(232, 82, 0),
(236, 7, 0),
(236, 13, 0),
(236, 54, 0),
(236, 83, 0),
(236, 84, 0),
(236, 85, 0),
(236, 86, 0),
(236, 87, 0),
(236, 88, 0),
(236, 89, 0),
(236, 90, 0),
(236, 91, 0),
(239, 7, 0),
(239, 37, 0),
(239, 92, 0),
(239, 93, 0),
(239, 94, 0),
(239, 95, 0),
(239, 96, 0),
(249, 7, 0),
(249, 13, 0),
(249, 97, 0),
(249, 98, 0),
(249, 99, 0),
(249, 100, 0),
(249, 101, 0),
(249, 102, 0),
(249, 103, 0),
(255, 1, 0),
(255, 104, 0),
(255, 105, 0),
(255, 106, 0),
(255, 107, 0),
(255, 108, 0),
(255, 109, 0),
(255, 110, 0),
(255, 111, 0),
(255, 112, 0),
(264, 7, 0),
(264, 37, 0),
(264, 113, 0),
(264, 114, 0),
(264, 115, 0),
(264, 116, 0),
(269, 7, 0),
(269, 37, 0),
(269, 117, 0),
(269, 118, 0),
(269, 119, 0),
(273, 7, 0),
(273, 37, 0),
(273, 77, 0),
(273, 120, 0),
(273, 121, 0),
(273, 122, 0),
(273, 123, 0),
(273, 124, 0),
(277, 7, 0),
(277, 37, 0),
(277, 125, 0),
(277, 126, 0),
(277, 127, 0),
(277, 128, 0),
(277, 129, 0),
(277, 130, 0),
(280, 7, 0),
(280, 37, 0),
(280, 131, 0),
(280, 132, 0),
(280, 133, 0),
(280, 134, 0),
(280, 135, 0),
(280, 136, 0),
(280, 137, 0),
(283, 7, 0),
(283, 19, 0),
(283, 37, 0),
(283, 138, 0),
(283, 139, 0),
(283, 140, 0),
(283, 141, 0),
(283, 142, 0),
(283, 143, 0),
(307, 1, 0),
(313, 4, 0),
(314, 4, 0),
(315, 4, 0),
(316, 4, 0) ;

#
# End of data contents of table `wp_term_relationships`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_taxonomy`
#

DROP TABLE IF EXISTS `wp_term_taxonomy`;


#
# Table structure of table `wp_term_taxonomy`
#

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_term_taxonomy`
#
INSERT INTO `wp_term_taxonomy` ( `term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 3),
(2, 2, 'category', '', 0, 0),
(4, 4, 'nav_menu', '', 0, 4),
(6, 6, 'category', '', 0, 1),
(7, 7, 'category', '', 0, 20),
(8, 8, 'category', '', 0, 1),
(9, 9, 'post_tag', '', 0, 1),
(10, 10, 'post_tag', '', 0, 1),
(11, 11, 'post_tag', '', 0, 1),
(12, 12, 'post_tag', '', 0, 1),
(13, 13, 'category', '', 0, 9),
(14, 14, 'post_tag', '', 0, 1),
(15, 15, 'post_tag', '', 0, 1),
(16, 16, 'post_tag', '', 0, 1),
(17, 17, 'post_tag', '', 0, 1),
(18, 18, 'post_tag', '', 0, 1),
(19, 19, 'post_tag', '', 0, 2),
(20, 20, 'post_tag', '', 0, 1),
(21, 21, 'post_tag', '', 0, 1),
(22, 22, 'post_tag', '', 0, 1),
(23, 23, 'post_tag', '', 0, 1),
(24, 24, 'post_tag', '', 0, 1),
(25, 25, 'post_tag', '', 0, 1),
(26, 26, 'post_tag', '', 0, 1),
(27, 27, 'post_tag', '', 0, 1),
(28, 28, 'post_tag', '', 0, 1),
(29, 29, 'post_tag', '', 0, 1),
(30, 30, 'post_tag', '', 0, 1),
(31, 31, 'post_tag', '', 0, 1),
(32, 32, 'post_tag', '', 0, 1),
(33, 33, 'post_tag', '', 0, 1),
(34, 34, 'post_tag', '', 0, 1),
(35, 35, 'post_tag', '', 0, 1),
(36, 36, 'post_tag', '', 0, 1),
(37, 37, 'category', '', 0, 9),
(38, 38, 'post_tag', '', 0, 1),
(39, 39, 'post_tag', '', 0, 1),
(40, 40, 'post_tag', '', 0, 1),
(41, 41, 'post_tag', '', 0, 1),
(42, 42, 'post_tag', '', 0, 1),
(43, 43, 'post_tag', '', 0, 1),
(44, 44, 'post_tag', '', 0, 1),
(45, 45, 'post_tag', '', 0, 1),
(46, 46, 'post_tag', '', 0, 1),
(47, 47, 'post_tag', '', 0, 1),
(48, 48, 'post_tag', '', 0, 1),
(49, 49, 'post_tag', '', 0, 1),
(50, 50, 'post_tag', '', 0, 1),
(51, 51, 'post_tag', '', 0, 1),
(52, 52, 'post_tag', '', 0, 1),
(53, 53, 'post_tag', '', 0, 1),
(54, 54, 'post_tag', '', 0, 2),
(55, 55, 'post_tag', '', 0, 1),
(56, 56, 'post_tag', '', 0, 1),
(57, 57, 'post_tag', '', 0, 1),
(58, 58, 'category', '', 0, 1),
(59, 59, 'post_tag', '', 0, 1),
(60, 60, 'post_tag', '', 0, 1),
(61, 61, 'post_tag', '', 0, 1),
(62, 62, 'post_tag', '', 0, 1),
(63, 63, 'post_tag', '', 0, 1),
(64, 64, 'post_tag', '', 0, 1),
(65, 65, 'post_tag', '', 0, 1),
(66, 66, 'post_tag', '', 0, 1),
(67, 67, 'post_tag', '', 0, 1),
(68, 68, 'post_tag', '', 0, 1),
(69, 69, 'post_tag', '', 0, 1),
(70, 70, 'post_tag', '', 0, 1),
(71, 71, 'post_tag', '', 0, 1),
(72, 72, 'post_tag', '', 0, 1),
(73, 73, 'post_tag', '', 0, 1),
(74, 74, 'post_tag', '', 0, 1),
(75, 75, 'post_tag', '', 0, 1),
(76, 76, 'post_tag', '', 0, 1),
(77, 77, 'post_tag', '', 0, 3),
(78, 78, 'post_tag', '', 0, 1),
(79, 79, 'post_tag', '', 0, 1),
(80, 80, 'post_tag', '', 0, 1),
(81, 81, 'post_tag', '', 0, 1),
(82, 82, 'post_tag', '', 0, 1),
(83, 83, 'post_tag', '', 0, 1),
(84, 84, 'post_tag', '', 0, 1),
(85, 85, 'post_tag', '', 0, 1),
(86, 86, 'post_tag', '', 0, 1),
(87, 87, 'post_tag', '', 0, 1),
(88, 88, 'post_tag', '', 0, 1),
(89, 89, 'post_tag', '', 0, 1),
(90, 90, 'post_tag', '', 0, 1),
(91, 91, 'post_tag', '', 0, 1),
(92, 92, 'post_tag', '', 0, 1),
(93, 93, 'post_tag', '', 0, 1),
(94, 94, 'post_tag', '', 0, 1),
(95, 95, 'post_tag', '', 0, 1),
(96, 96, 'post_tag', '', 0, 1),
(97, 97, 'post_tag', '', 0, 1),
(98, 98, 'post_tag', '', 0, 1),
(99, 99, 'post_tag', '', 0, 1),
(100, 100, 'post_tag', '', 0, 1),
(101, 101, 'post_tag', '', 0, 1),
(102, 102, 'post_tag', '', 0, 1) ;
INSERT INTO `wp_term_taxonomy` ( `term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(103, 103, 'post_tag', '', 0, 1),
(104, 104, 'post_tag', '', 0, 1),
(105, 105, 'post_tag', '', 0, 1),
(106, 106, 'post_tag', '', 0, 1),
(107, 107, 'post_tag', '', 0, 1),
(108, 108, 'post_tag', '', 0, 1),
(109, 109, 'post_tag', '', 0, 1),
(110, 110, 'post_tag', '', 0, 1),
(111, 111, 'post_tag', '', 0, 1),
(112, 112, 'post_tag', '', 0, 1),
(113, 113, 'post_tag', '', 0, 1),
(114, 114, 'post_tag', '', 0, 1),
(115, 115, 'post_tag', '', 0, 1),
(116, 116, 'post_tag', '', 0, 1),
(117, 117, 'post_tag', '', 0, 1),
(118, 118, 'post_tag', '', 0, 1),
(119, 119, 'post_tag', '', 0, 1),
(120, 120, 'post_tag', '', 0, 1),
(121, 121, 'post_tag', '', 0, 1),
(122, 122, 'post_tag', '', 0, 1),
(123, 123, 'post_tag', '', 0, 1),
(124, 124, 'post_tag', '', 0, 1),
(125, 125, 'post_tag', '', 0, 1),
(126, 126, 'post_tag', '', 0, 1),
(127, 127, 'post_tag', '', 0, 1),
(128, 128, 'post_tag', '', 0, 1),
(129, 129, 'post_tag', '', 0, 1),
(130, 130, 'post_tag', '', 0, 1),
(131, 131, 'post_tag', '', 0, 1),
(132, 132, 'post_tag', '', 0, 1),
(133, 133, 'post_tag', '', 0, 1),
(134, 134, 'post_tag', '', 0, 1),
(135, 135, 'post_tag', '', 0, 1),
(136, 136, 'post_tag', '', 0, 1),
(137, 137, 'post_tag', '', 0, 1),
(138, 138, 'post_tag', '', 0, 1),
(139, 139, 'post_tag', '', 0, 1),
(140, 140, 'post_tag', '', 0, 1),
(141, 141, 'post_tag', '', 0, 1),
(142, 142, 'post_tag', '', 0, 1),
(143, 143, 'slider', '', 0, 1) ;

#
# End of data contents of table `wp_term_taxonomy`
# --------------------------------------------------------



#
# Delete any existing table `wp_terms`
#

DROP TABLE IF EXISTS `wp_terms`;


#
# Table structure of table `wp_terms`
#

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_terms`
#
INSERT INTO `wp_terms` ( `term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Slider', 'slider', 0),
(4, 'main-menu', 'main-menu', 0),
(6, 'Full Issues', 'issues', 0),
(7, 'The Outdated Issue', 'nov2014', 0),
(8, 'Vol. XXVI', 'volxxvi', 0),
(9, 'cookie dough', 'cookie-dough', 0),
(10, 'ice cream', 'ice-cream', 0),
(11, 'madeline', 'madeline', 0),
(12, 'new stydt', 'new-stydt', 0),
(13, 'News', 'news', 0),
(14, 'personality', 'personality', 0),
(15, 'personality test', 'personality-test', 0),
(16, 'psychology', 'psychology', 0),
(17, 'safiya', 'safiya', 0),
(18, 'iSIS', 'isis', 0),
(19, 'monaco', 'monaco', 0),
(20, 'Obama', 'obama', 0),
(21, 'zamboni', 'zamboni', 0),
(22, 'Al Qaeda', 'al-qaeda', 0),
(23, 'award', 'award', 0),
(24, 'bin laden', 'bin-laden', 0),
(25, 'emmy', 'emmy', 0),
(26, 'lifetime achievement', 'lifetime-achievement', 0),
(27, 'local emmy', 'local-emmy', 0),
(28, 'osama', 'osama', 0),
(29, 'osama bin laden', 'osama-bin-laden', 0),
(30, 'Sayed Rahdmatullah Hashemi', 'sayed-rahdmatullah-hashemi', 0),
(31, 'Taliban', 'taliban', 0),
(32, 'television', 'television', 0),
(33, 'terrorism', 'terrorism', 0),
(34, 'terrorist', 'terrorist', 0),
(35, 'viral video campaign', 'viral-video-campaign', 0),
(36, 'exercise', 'exercise', 0),
(37, 'Features', 'features', 0),
(38, 'looking', 'looking', 0),
(39, 'runners', 'runners', 0),
(40, 'running', 'running', 0),
(41, 'sports', 'sports', 0),
(42, 'apartment', 'apartment', 0),
(43, 'beige', 'beige', 0),
(44, 'burns', 'burns', 0),
(45, 'crimea', 'crimea', 0),
(46, 'eyesore', 'eyesore', 0),
(47, 'fire', 'fire', 0),
(48, 'firechief', 'firechief', 0),
(49, 'fletcher', 'fletcher', 0),
(50, 'medford', 'medford', 0),
(51, 'meth lord', 'meth-lord', 0),
(52, 'paint', 'paint', 0),
(53, 'somerville', 'somerville', 0),
(54, 'student', 'student', 0),
(55, 'tacky', 'tacky', 0),
(56, 'cigarettes', 'cigarettes', 0),
(57, 'homeless', 'homeless', 0),
(58, 'Opinion', 'opinion', 0),
(59, 'pizza', 'pizza', 0),
(60, 'tobacco', 'tobacco', 0),
(61, 'Totino\'s', 'totinos', 0),
(62, 'ballast', 'ballast', 0),
(63, 'boston clubs', 'boston-clubs', 0),
(64, 'club', 'club', 0),
(65, 'erect', 'erect', 0),
(66, 'erected', 'erected', 0),
(67, 'new', 'new', 0),
(68, 'new night club', 'new-night-club', 0),
(69, 'night', 'night', 0),
(70, 'night club', 'night-club', 0),
(71, 'phallus palace', 'phallus-palace', 0),
(72, 'emails', 'emails', 0),
(73, 'GoSafe', 'gosafe', 0),
(74, 'police', 'police', 0),
(75, 'safety', 'safety', 0),
(76, 'safety emails', 'safety-emails', 0),
(77, 'tufts', 'tufts', 0),
(78, 'TUPD', 'tupd', 0),
(79, 'list', 'list', 0),
(80, 'poop', 'poop', 0),
(81, 'shit', 'shit', 0),
(82, 'top 5', 'top-5', 0),
(83, 'arabic', 'arabic', 0),
(84, 'hijab', 'hijab', 0),
(85, 'ignorance', 'ignorance', 0),
(86, 'IR', 'ir', 0),
(87, 'muslim', 'muslim', 0),
(88, 'muslim student', 'muslim-student', 0),
(89, 'offend', 'offend', 0),
(90, 'open-toed', 'open-toed', 0),
(91, 'shoes', 'shoes', 0),
(92, 'acorn head', 'acorn-head', 0),
(93, 'feature', 'feature', 0),
(94, 'haikus', 'haikus', 0),
(95, 'life. tufts', 'life-tufts', 0),
(96, 'poetry', 'poetry', 0),
(97, 'ET', 'et', 0),
(98, 'extraterrestrial', 'extraterrestrial', 0),
(99, 'life', 'life', 0),
(100, 'NASA', 'nasa', 0),
(101, 'space', 'space', 0),
(102, 'space virgin', 'space-virgin', 0) ;
INSERT INTO `wp_terms` ( `term_id`, `name`, `slug`, `term_group`) VALUES
(103, 'virgin', 'virgin', 0),
(104, 'carm', 'carm', 0),
(105, 'carmichael', 'carmichael', 0),
(106, 'chubby', 'chubby', 0),
(107, 'dewick', 'dewick', 0),
(108, 'dining hall', 'dining-hall', 0),
(109, 'icloud', 'icloud', 0),
(110, 'music', 'music', 0),
(111, 'photo booth', 'photo-booth', 0),
(112, 'selfies', 'selfies', 0),
(113, 'advice', 'advice', 0),
(114, 'alcohol', 'alcohol', 0),
(115, 'drunk', 'drunk', 0),
(116, 'girls', 'girls', 0),
(117, '10', '10', 0),
(118, 'counting', 'counting', 0),
(119, 'no nonsense', 'no-nonsense', 0),
(120, 'alumni', 'alumni', 0),
(121, 'joseph stalin', 'joseph-stalin', 0),
(122, 'stalin', 'stalin', 0),
(123, 'tufts alumni association', 'tufts-alumni-association', 0),
(124, 'virgin mary', 'virgin-mary', 0),
(125, 'ABC parties', 'abc-parties', 0),
(126, 'ABC party', 'abc-party', 0),
(127, 'clothes', 'clothes', 0),
(128, 'fashion', 'fashion', 0),
(129, 'parties', 'parties', 0),
(130, 'party', 'party', 0),
(131, 'barack', 'barack', 0),
(132, 'boehner', 'boehner', 0),
(133, 'celebrity', 'celebrity', 0),
(134, 'clinton', 'clinton', 0),
(135, 'omaba', 'omaba', 0),
(136, 'putin', 'putin', 0),
(137, 'tattoo', 'tattoo', 0),
(138, 'Anthony Monaco', 'anthony-monaco', 0),
(139, 'DNA', 'dna', 0),
(140, 'science', 'science', 0),
(141, 'shrooms', 'shrooms', 0),
(142, 'Tony Monaco', 'tony-monaco', 0),
(143, 'yes', 'yes', 0) ;

#
# End of data contents of table `wp_terms`
# --------------------------------------------------------



#
# Delete any existing table `wp_usermeta`
#

DROP TABLE IF EXISTS `wp_usermeta`;


#
# Table structure of table `wp_usermeta`
#

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_usermeta`
#
INSERT INTO `wp_usermeta` ( `umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'wp_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'wp360_locks,wp390_widgets,wp410_dfw'),
(13, 1, 'show_welcome_panel', '0'),
(14, 1, 'session_tokens', 'a:2:{s:64:"5d6ccf57dfac8560bed9f3c8256a245bc1d0ce9ff58403df4ac7e6903e519a43";a:4:{s:10:"expiration";i:1430245867;s:2:"ip";s:12:"89.67.65.194";s:2:"ua";s:108:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36";s:5:"login";i:1430073067;}s:64:"a311cca674c8547ebd2ab157cfc69522f4941daba2a62b22ab02c2538dd3b6f1";a:4:{s:10:"expiration";i:1430251949;s:2:"ip";s:3:"::1";s:2:"ua";s:108:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36";s:5:"login";i:1430079149;}}'),
(15, 1, 'wp_dashboard_quick_press_last_post_id', '320'),
(16, 1, 'wp_user-settings', 'libraryContent=browse&editor=html&hidetb=1'),
(17, 1, 'wp_user-settings-time', '1429650934'),
(18, 2, 'nickname', 'user'),
(19, 2, 'first_name', 'Michał'),
(20, 2, 'last_name', 'Wołczecki-Klim'),
(21, 2, 'description', ''),
(22, 2, 'rich_editing', 'true'),
(23, 2, 'comment_shortcuts', 'false'),
(24, 2, 'admin_color', 'fresh'),
(25, 2, 'use_ssl', '0'),
(26, 2, 'show_admin_bar_front', 'true'),
(27, 2, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(28, 2, 'wp_user_level', '10'),
(29, 2, 'dismissed_wp_pointers', 'wp360_locks,wp390_widgets'),
(30, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";}'),
(31, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:8:"add-post";i:1;s:12:"add-post_tag";}'),
(32, 3, 'nickname', 'Wojciech'),
(33, 3, 'first_name', 'Wojciech'),
(34, 3, 'last_name', 'Sura'),
(35, 3, 'description', ''),
(36, 3, 'rich_editing', 'true'),
(37, 3, 'comment_shortcuts', 'false'),
(38, 3, 'admin_color', 'fresh'),
(39, 3, 'use_ssl', '0'),
(40, 3, 'show_admin_bar_front', 'true'),
(41, 3, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(42, 3, 'wp_user_level', '10'),
(43, 3, 'dismissed_wp_pointers', 'wp360_locks,wp390_widgets,wp410_dfw'),
(44, 2, 'session_tokens', 'a:1:{s:64:"414c0c4213688ae01246f68eebf93116dda3027b6ce3881ada4bf1dfb00a3b26";a:4:{s:10:"expiration";i:1429659445;s:2:"ip";s:14:"213.238.126.31";s:2:"ua";s:72:"Mozilla/5.0 (Windows NT 6.2; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0";s:5:"login";i:1429486645;}}'),
(45, 2, 'wp_dashboard_quick_press_last_post_id', '33'),
(46, 3, 'session_tokens', 'a:2:{s:64:"18aac45babf62b2594507541cd2cd086e3b1c73caf443bd5a676c5ac8a95fbf6";a:4:{s:10:"expiration";i:1429659515;s:2:"ip";s:14:"213.238.126.31";s:2:"ua";s:72:"Mozilla/5.0 (Windows NT 6.2; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0";s:5:"login";i:1429486715;}s:64:"caf4b0622a18bc390850ed95adebbbfa17eddcf142f71053c41e8ea4a2361086";a:4:{s:10:"expiration";i:1430789875;s:2:"ip";s:14:"213.238.126.31";s:2:"ua";s:72:"Mozilla/5.0 (Windows NT 6.2; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0";s:5:"login";i:1429580275;}}'),
(47, 3, 'wp_dashboard_quick_press_last_post_id', '34'),
(48, 4, 'nickname', 'ethan'),
(49, 4, 'first_name', 'Ethan'),
(50, 4, 'last_name', 'Hartzell'),
(51, 4, 'description', ''),
(52, 4, 'rich_editing', 'true'),
(53, 4, 'comment_shortcuts', 'false'),
(54, 4, 'admin_color', 'fresh'),
(55, 4, 'use_ssl', '0'),
(56, 4, 'show_admin_bar_front', 'true'),
(57, 4, 'wp_capabilities', 'a:1:{s:10:"subscriber";b:1;}'),
(58, 4, 'wp_user_level', '0'),
(59, 4, 'dismissed_wp_pointers', 'wp360_locks,wp390_widgets'),
(60, 5, 'nickname', 'Jesse'),
(61, 5, 'first_name', 'Jesse'),
(62, 5, 'last_name', 'Litvin'),
(63, 5, 'description', ''),
(64, 5, 'rich_editing', 'true'),
(65, 5, 'comment_shortcuts', 'false'),
(66, 5, 'admin_color', 'fresh'),
(67, 5, 'use_ssl', '0'),
(68, 5, 'show_admin_bar_front', 'true'),
(69, 5, 'wp_capabilities', 'a:1:{s:10:"subscriber";b:1;}'),
(70, 5, 'wp_user_level', '0'),
(71, 5, 'dismissed_wp_pointers', 'wp360_locks,wp390_widgets'),
(72, 3, 'nav_menu_recently_edited', '4'),
(73, 3, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";}'),
(74, 3, 'metaboxhidden_nav-menus', 'a:4:{i:0;s:8:"add-post";i:1;s:23:"add-gallery-slider-post";i:2;s:21:"add-video-slider-post";i:3;s:12:"add-post_tag";}'),
(75, 1, 'nav_menu_recently_edited', '4') ;

#
# End of data contents of table `wp_usermeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_users`
#

DROP TABLE IF EXISTS `wp_users`;


#
# Table structure of table `wp_users`
#

CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(64) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


#
# Data contents of table `wp_users`
#
INSERT INTO `wp_users` ( `ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BCwRVux73IKU8Ce8.KHGvNC5nfvMeH/', 'admin', 'michal.wklim@gmail.com', '', '2015-04-13 14:45:13', '', 0, 'admin'),
(2, 'user', '$P$BmhLlZEWLb07J0XfSenRw2lzrdi7VX1', 'user', 'michal.klim@jarzemko.com', '', '2015-04-14 22:52:18', '', 0, 'Michał Wołczecki-Klim'),
(3, 'Wojciech', '$P$B8R6Phm/x/CtIdZTkNGCIHO4fR9cpL.', 'wojciech', 'contact@musdesigns.com', '', '2015-04-19 23:37:22', '', 0, 'Wojciech Sura'),
(4, 'ethan', '$P$B9zel/3M09Wh2mhGhNb07vhejC9NTh/', 'ethan', 'ehartz01@gmail.com', '', '2015-04-21 01:45:02', '', 0, 'Ethan Hartzell'),
(5, 'Jesse', '$P$Bu5z82eRvtdkLHvk/T/JyV25p2mCth/', 'jesse', 'jesselitvin@gmail.com', '', '2015-04-21 01:45:03', '', 0, 'Jesse Litvin') ;

#
# End of data contents of table `wp_users`
# --------------------------------------------------------

#
# Add constraints back in
#

