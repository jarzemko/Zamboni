<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 *
 * @package themeHandle
 */

get_header(); ?>

<section class='page__section container'>
	<header class="container__title">
		<h1><?php the_title(); ?></h1>
	</header>
	<div class="container">
		<?php the_content(); ?>
	</div>
</section>
<?php get_footer(); ?>
