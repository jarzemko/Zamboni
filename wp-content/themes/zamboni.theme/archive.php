<?php
/**
 * Main Template File
 *
 * This file is used to display a page when nothing more specific matches a query.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package themeHandle
 */

get_header();
$queried_object = get_queried_object();
?>
<section class="archive__section container">
		<header class="container__title">
			<?php
			if(is_author()) {
				echo "<h1>" . $queried_object->user_nicename . "</h1>";
			} else {
				echo "<h1>" . $queried_object->name . "</h1>";
			}
			?>
		</header>
		<div class="container__wrap tiles tiles--4">
			<?php
			if(is_author())
			{
				$objectQueryKey = 'author_name';
				$objectQueryValue = $queried_object->user_nicename;
			}
			elseif(is_category())
			{
				$objectQueryKey = 'category_name';
				$objectQueryValue = $queried_object->slug;
			}
			elseif(is_tag())
			{
				$objectQueryKey = 'tag';
				$objectQueryValue = $queried_object->slug;
			}

			$objectQuery = array($objectQueryKey => $objectQueryValue);
			?>

			<?php
			$data = get_archive_page_html(0,$objectQuery);
			echo $data['html'];
			?>
		</div>
		<div class="loadmore">
				<a data-object-query-key="<?= $objectQueryKey?>" data-object-query-value="<?= $objectQueryValue ?>" data-next-page="1" onclick="loadMorePosts(this)" href="#">Load more</a>
		</div>
</section>
<?php get_footer(); ?>
