<?php
/**
 * Theme functions
 *
 * Sets up the theme and provides some helper functions.
 *
 * @package themeHandle
 */

/* THEME SETUP
 ========================== */

if (!function_exists('themeFunction_setup')):
	function themeFunction_setup()
	{

		// Available for translation
		load_theme_textdomain('themeTextDomain', get_template_directory() . '/languages');

		// Add default posts and comments RSS feed links to <head>.
		add_theme_support('automatic-feed-links');

		// Add custom nav menu support
		register_nav_menu('primary', __('Primary Menu', 'themeTextDomain'));
		register_nav_menu('footer', __('Footer Menu', 'themeTextDomain'));

		// Add featured image support
		add_theme_support('post-thumbnails');

		// Enable support for HTML5 markup.
		add_theme_support('html5', array(
			'search-form',
			'gallery',
		));

		// Add custom image sizes
		// add_image_size( 'name', 500, 300 );
	}
endif;
add_action('after_setup_theme', 'themeFunction_setup');

add_image_size('slider-image', 9999, 600);
add_image_size('gallery-slider-image', 9999, 300);
add_image_size('cover-image', 1920, 9999);

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function custom_excerpt_length( $length ) {
	return 20;
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

add_filter( 'post_thumbnail_html', 'my_post_thumbnail_fallback', 20, 5 );
function my_post_thumbnail_fallback( $html, $post_id, $post_thumbnail_id, $size, $attr ) {
	if ( empty( $html ) )
	{
		return sprintf('<img src="%s" class="default-post-thumbnail %s"/>',app_get_option('app_default_thumbnail'),$size);
	}
	return $html;
}

function get_the_post_thumbnail_url($postID)
{
	$url = wp_get_attachment_url( get_post_thumbnail_id($postID) );
	if(empty($url))
	{
		$url = app_get_option('app_default_thumbnail');
	}

	return $url;
}

add_action('init', 'galleries_post_type');
function galleries_post_type()
{
	register_post_type('gallery-slider-post',
		array(
			'labels' => array(
				'name' => __( 'Galleries' ),
				'singular_name' => __( 'Gallery' )
			),
			'public' => true,
			'has_archive' => true,
			'supports' => array(
				'title',
				'thumbnail',
				'editor'
			)
		)
	);
}

add_action('init', 'videos_post_type');
function videos_post_type()
{
	register_post_type('video-slider-post',
		array(
			'labels' => array(
				'name' => __( 'Videos' ),
				'singular_name' => __( 'Video' )
			),
			'public' => true,
			'has_archive' => true,
			'supports' => array(
				'title',
				'editor'
			)
		)
	);
}

add_action( 'init', 'create_slider_taxonomy' );

function create_slider_taxonomy()
{
	$args = array(
		'hierarchical' => false,
		'labels' => array(
			'name' => __('Slider'),
			'singular_name' => __('Slider')
		),
		'show_ui' => false,
		'public' => true
	);

	register_taxonomy('slider', 'post', $args);
	wp_insert_term( 'yes', 'slider');
}

function slider_taxonomy_filters() {
	global $typenow;

	$taxonomies = array('slider');

	if( $typenow == 'post' )
	{
		foreach ($taxonomies as $tax_slug)
		{
			$tax_obj = get_taxonomy($tax_slug);
			$tax_name = $tax_obj->labels->name;
			$terms = get_terms($tax_slug);
			if(count($terms) > 0) {
				echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
				echo "<option value=''>Show all</option>";
				foreach ($terms as $term) {
					echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . ($term->name == 'yes' ? 'Slider' : $term->name) .' (' . $term->count .')</option>';
				}
				echo "</select>";
			}
		}
	}
}
add_action( 'restrict_manage_posts', 'slider_taxonomy_filters' );

/* SIDEBARS & WIDGET AREAS
 ========================== */
function themeFunction_widgets_init()
{
	register_sidebar(array(
		'name' => __('Sidebar', 'themeTextDomain'),
		'id' => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
}

add_action('widgets_init', 'themeFunction_widgets_init');


/* ENQUEUE SCRIPTS & STYLES
 ========================== */
function themeFunction_scripts()
{
	// theme style.css file
	wp_enqueue_style('themeTextDomain-style', get_stylesheet_uri());

	// threaded comments
	if (is_singular() && comments_open() && get_option('thread_comments'))
	{
		wp_enqueue_script('comment-reply');
	}
	// vendor scripts
	wp_enqueue_script(
		'vendor',
		get_template_directory_uri() . '/assets/vendor/plugins.js',
		array('jquery')
	);
	// theme scripts
	wp_enqueue_script(
		'theme-init',
		get_template_directory_uri() . '/assets/app.js',
		array('vendor')
	);

	wp_localize_script('theme-init', 'wp', array(
		'ajaxurl' => admin_url('admin-ajax.php'),
		'rooturl' => site_url('/')
	));
}

add_action('wp_enqueue_scripts', 'themeFunction_scripts');


/* MISC EXTRAS
 ========================== */

// Comments & pingbacks display template
include('inc/functions/comments.php');

// Optional Customizations
// Includes: TinyMCE tweaks, admin menu & bar settings, query overrides
include('inc/functions/customizations.php');

/* CMB2 */

if (file_exists(__DIR__ . '/inc/lib/cmb2/init.php'))
{
	require_once __DIR__ . '/inc/lib/cmb2/init.php';
} elseif (file_exists(__DIR__ . '/inc/lib/CMB2/init.php'))
{
	require_once __DIR__ . '/inc/lib/CMB2/init.php';
}
add_action('cmb2_init', 'cmb2_metaboxes');

function cmb2_metaboxes()
{
	$cmb = new_cmb2_box( array(
		'id'            => 'featured-categories-box',
		'title'         => __('Options' ),
		'object_types' 	=> array( 'page' ), // post type
		'show_on'      	=> array( 'key' => 'id', 'value' => array( 26 ) ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true
	) );

	$cmb->add_field( array(
		'name' => __('Featured categories'),
		'desc' => __('Choose featured categories'),
		'id' => 'featured-categories', // no prefix needed since the options are one option array.
		'taxonomy' => 'category', //Enter Taxonomy Slug
		'type' => 'taxonomy_multicheck',
	));

	$cmb = new_cmb2_box( array(
		'id'            => 'option-box',
		'title'         => __('Options' ),
		'object_types' 	=> array( 'post' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true
	));

	$cmb->add_field( array(
		'name' => __('Slider'),
		'desc' => __('Show in home page slider'),
		'id' => 'slider',
		'taxonomy' => 'slider',
		'type' => 'taxonomy_multicheck',
		'select_all_button' => false
	));

	$cmb->add_field( array(
		'name' => __('Author'),
		'desc' => __('Author alias'),
		'id' => 'author-alias',
		'type' => 'text'
	));
	$cmb->add_field( array(
		'name' => __('Cover photo'),
		'desc' => __('Add cover photo. It will be shown on the single page'),
		'object_types' 	=> array( 'post' ),
		'id' => 'cover-photo',
		'type' => 'file',
		'options' => array(
        	'url' => false,
    	),
	));
	$cmb->add_field( array(
		'name' => __('Title position'),
		'desc' => __('Choose title positon'),
		'object_types' 	=> array( 'post' ),
		'id' => 'title_position',
		'type' => 'radio',
		'show_option_none' => false,
		'default'          => 'afterCover',
	    'options'          => array(
	        'afterCover' => __( 'After Cover Photo', 'cmb' ),
	        'onCover'   => __( 'On Cover Photo', 'cmb' ),
	    )
	));
	$cmb->add_field( array(
		'name' => __('Title color'),
		'desc' => __('Choose title color'),
		'object_types' 	=> array( 'post' ),
		'id' => 'title_color',
		'type' => 'colorpicker',
		'default' => '#000000',
	));
}

function cmb2_taxonomy_meta_initiate()
{
	require_once(__DIR__ . '/inc/lib/Taxonomy_MetaData/Taxonomy_MetaData_CMB2.php');
	/**
	 * Semi-standard CMB2 metabox/fields array
	 */
	$category_meta_box = array(
		'id' => 'cat_options',
		// 'key' and 'value' should be exactly as follows
		'show_on' => array('key' => 'options-page', 'value' => array('unknown')),
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => __('Category color'),
				'desc' => __('Choose category color'),
				'id' => 'category-color', // no prefix needed since the options are one option array.
				'type' => 'colorpicker',
				'default' => '#ffffff'
			)
		)
	);

	/**
	 * Instantiate our taxonomy meta class
	 */
	$name_variety = new Taxonomy_MetaData_CMB2('category', $category_meta_box, __('Category Settings', 'taxonomy-metadata'));
}

cmb2_taxonomy_meta_initiate();


/* THEME OPTIONS PAGE
    using CMB2
========================== */

class app_Admin
{
	/**
	 * Option key, and option page slug
	 * @var string
	 */
	private $key = 'app_options';
	/**
	 * Options page metabox id
	 * @var string
	 */
	private $metabox_id = 'app_option_metabox';
	/**
	 * Options Page title
	 * @var string
	 */
	protected $title = '';
	/**
	 * Options Page hook
	 * @var string
	 */
	protected $options_page = '';

	/**
	 * Constructor
	 * @since 0.1.0
	 */
	public function __construct()
	{
		// Set our title
		$this->title = __('Website Settings', 'app');
	}

	/**
	 * Initiate our hooks
	 * @since 0.1.0
	 */
	public function hooks()
	{
		add_action('admin_init', array($this, 'init'));
		add_action('admin_menu', array($this, 'add_options_page'));
		add_action('cmb2_init', array($this, 'add_options_page_metabox'));
	}

	/**
	 * Register our setting to WP
	 * @since  0.1.0
	 */
	public function init()
	{
		register_setting($this->key, $this->key);
	}

	/**
	 * Add menu options page
	 * @since 0.1.0
	 */
	public function add_options_page()
	{
		$this->options_page = add_menu_page($this->title, $this->title, 'manage_options', $this->key, array($this, 'admin_page_display'));
	}

	/**
	 * Admin page markup. Mostly handled by CMB2
	 * @since  0.1.0
	 */
	public function admin_page_display()
	{
		echo '<div class="wrap cmb2_options_page' . $this->key . '">';
		echo '<h1>' . esc_html(get_admin_page_title()) . '</h1>';
		echo cmb2_metabox_form($this->metabox_id, $this->key);
		echo '</div>';
	}

	/**
	 * Add the options metabox to the array of metaboxes
	 * @since  0.1.0
	 */
	function add_options_page_metabox()
	{
		$cmb = new_cmb2_box(array(
			'id' => $this->metabox_id,
			'hookup' => false,
			'show_on' => array(
				// These are important, don't remove
				'key' => 'options-page',
				'value' => array($this->key,)
			),
		));
		// Set our CMB2 fields
		$cmb->add_field( array(
		    'name' => 'General settings',
		    'type' => 'title',
		    'id'   => 'app_general_title',
			'after'  => '<hr>'
		) );
		$cmb->add_field(array(
			'name' => 'Webite logo',
			'desc' => 'It is best to add image in SVG format',
			'id' => 'app_main_logo',
			'type' => 'file',
			'allow' => array('attachment') // limit to just attachments with array( 'attachment' )
		));
		$cmb->add_field(array(
			'name' => 'Credits:',
			'desc' => 'For example: Designed by John Smith',
			'id' => 'app_main_credits',
			'type' => 'text'
		));
		$cmb->add_field(array(
			'name' => 'Show sections:',
			'desc' => 'Which section should be show on the front page',
			'id' => 'app_main_sections',
			'type'    => 'multicheck',
		    'options' => array(
		        'videos' => 'Videos',
		        'galleries' => 'Galleries',
		        'instagram' => 'Instagram',
				'twitter' => 'Twitter',
		    )
		));

		$cmb->add_field( array(
		    'name' => 'Social Media',
		    'type' => 'title',
		    'id'   => 'app_social_title',
			'after'  => '<hr>'
		) );

		$group_field_id = $cmb->add_field( array(
		    'id'          => 'app_repeat_group',
		    'type'        => 'group',
		    'description' => __( 'Add social services like Facebook, Twitter, Instagram. Links will be shown in the footer', 'cmb' ),
		    'options'     => array(
		        'group_title'   => __( 'Social media service {#}', 'cmb' ), // since version 1.1.4, {#} gets replaced by row number
		        'add_button'    => __( 'Add Another Entry', 'cmb' ),
		        'remove_button' => __( 'Remove Entry', 'cmb' ),
		        'sortable'      => true, // beta
		    ),
		) );
		$cmb->add_group_field( $group_field_id, array(
		    'name' => 'Profile page url',
		    'id'   => 'url',
			'type' => 'text_url',
    		'protocols' => array( 'http', 'https'),
		) );

		$cmb->add_group_field( $group_field_id, array(
		    'name' => 'Service icon',
		    'id'   => 'icon',
		    'type' => 'file',
		) );

		$cmb->add_field( array(
		    'name' => 'Twitter Api',
		    'type' => 'title',
		    'id'   => 'app_twitter_title',
			'after'  => '<hr>'
		) );

		$cmb->add_field( array(
		    'name' => 'Username',
		    'desc' => 'TO DO',
		    'type' => 'text',
		    'id'   => 'app_twitter_username'
		) );

		$cmb->add_field( array(
		    'name' => 'Consumer Key',
		    'desc' => 'TO DO',
		    'type' => 'text',
		    'id'   => 'app_twitter_consumer-key'
		) );

		$cmb->add_field( array(
		    'name' => 'Consumer Key Secret',
		    'desc' => 'TO DO',
		    'type' => 'text',
		    'id'   => 'app_twitter_consumer-key-secret'
		) );

		$cmb->add_field( array(
		    'name'             => 'Tweets amount',
		    'desc'             => 'Select amout of showed tweets',
		    'id'               => 'app_twitter_amount',
		    'type'             => 'select',
		    'default'          => '0',
		    'options'          => array(
				'0' => __( '0', 'cmb' ),
		        '1' => __( '1', 'cmb' ),
		        '2' => __( '2', 'cmb' ),
		        '3' => __( '3', 'cmb' ),
				'4' => __( '4', 'cmb' ),
				'5' => __( '5', 'cmb' ),
				'6' => __( '6', 'cmb' ),
				'7' => __( '7', 'cmb' ),
				'8' => __( '8', 'cmb' ),
				'9' => __( '9', 'cmb' ),
				'10' => __( '10', 'cmb' )
		    ),
		));

		$cmb->add_field( array(
			'name' => 'Instagram Api',
			'type' => 'title',
			'id'   => 'app_instagram_title',
			'after'  => '<hr>'
		) );

		$cmb->add_field( array(
			'name' => 'Client-ID',
			'desc' => 'http://www.blueprintinteractive.com/blog/how-instagram-api-fancybox-simplified',
			'type' => 'text',
			'id'   => 'app_instagram_client-id'
		) );

		$cmb->add_field( array(
			'name' => 'Access token',
			'desc' => 'http://www.blueprintinteractive.com/blog/how-instagram-api-fancybox-simplified',
			'type' => 'text',
			'id'   => 'app_instagram_access-token'
		) );

		$cmb->add_field( array(
			'name'             => 'Instagram amount',
			'desc'             => 'Select amout of showed instagram photos',
			'id'               => 'app_instagram_amount',
			'type'             => 'select',
			'default'          => '0',
			'options'          => array(
				'0' => __( '0', 'cmb' ),
				'1' => __( '1', 'cmb' ),
				'2' => __( '2', 'cmb' ),
				'3' => __( '3', 'cmb' ),
				'4' => __( '4', 'cmb' ),
				'5' => __( '5', 'cmb' ),
				'6' => __( '6', 'cmb' ),
				'7' => __( '7', 'cmb' ),
				'8' => __( '8', 'cmb' ),
				'9' => __( '9', 'cmb' ),
				'10' => __( '10', 'cmb' )
			),
		));

		$cmb->add_field( array(
			'name' => 'Posts',
			'type' => 'title',
			'id'   => 'app_thumbnail_title',
			'after'  => '<hr>'
		) );

		$cmb->add_field( array(
			'name'    => 'Default thumbnail',
			'desc'    => 'Upload an image or enter an URL.',
			'id'      => 'app_default_thumbnail',
			'type'    => 'file'
		) );
	}

	/**
	 * Public getter method for retrieving protected/private variables
	 * @since  0.1.0
	 * @param  string $field Field to retrieve
	 * @return mixed          Field value or exception is thrown
	 */
	public function __get($field)
	{
		// Allowed fields to retrieve
		if (in_array($field, array('key', 'metabox_id', 'title', 'options_page'), true))
		{
			return $this->{$field};
		}
		throw new Exception('Invalid property: ' . $field);
	}
}

// Get it started
$GLOBALS['app_Admin'] = new app_Admin();
$GLOBALS['app_Admin']->hooks();
/**
 * Helper function to get/return the app_Admin object
 * @since  0.1.0
 * @return app_Admin object
 */
function app_Admin()
{
	global $app_Admin;
	return $app_Admin;
}

/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string $key Options array key
 * @return mixed        Option value
 */
function app_get_option($key = '')
{
	global $app_Admin;
	return cmb2_get_option($app_Admin->key, $key);
}

/* HELPERS
 ========================== */
function get_tweets()
{
	//TODO use composer for lib?
	require_once("inc/lib/twitteroauth/autoload.php");

	# Fetch constants
	$TWITTER_USERNAME = app_get_option('app_twitter_username');
	$TWITTER_CONSUMER_KEY = app_get_option('app_twitter_consumer-key');
	$TWITTER_CONSUMER_KEY_SECRET = app_get_option('app_twitter_consumer-key-secret');
	$TWITTER_AMOUNT = app_get_option('app_twitter_amount');


	# Create the connection
	$twitter = new Abraham\TwitterOAuth\TwitterOAuth($TWITTER_CONSUMER_KEY, $TWITTER_CONSUMER_KEY_SECRET); //, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

	# Migrate over to SSL/TLS
	$twitter->ssl_verifypeer = true;

	# Load the Tweets
	$tweets = $twitter->get('statuses/user_timeline', array('screen_name' => $TWITTER_USERNAME, 'exclude_replies' => 'true', 'include_rts' => 'false', 'count' => $TWITTER_AMOUNT));

	return $tweets;
}

function get_instagram()
{
	$access_token = app_get_option('app_instagram_access-token');
	$client_id = app_get_option('app_instagram_client-id');
	$count = app_get_option('app_instagram_amount');

	$url = sprintf("https://api.instagram.com/v1/users/%s/media/recent/?access_token=%s&count=%s",$client_id,$access_token,$count);
	$result = file_get_contents($url);
	$result = json_decode($result);

	return $result;
}

/* AJAX HOOKS
 ========================== */
add_action('wp_ajax_nopriv_ajax_load_more_stories', 'ajax_load_more_stories');
add_action('wp_ajax_ajax_load_more_stories', 'ajax_load_more_stories');

function ajax_load_more_stories()
{
	if (!isset($_GET['pageNo']))
	{
		wp_send_json_error("No page specified");
	}

	$pageNo = intval($_GET['pageNo']);

	if(isset($_GET['objectQueryKey']) && isset($_GET['objectQueryValue']))
	{
		$objectQuery = array($_GET['objectQueryKey'] => $_GET['objectQueryValue']);
	}

	if(isset($objectQuery))
	{
		$data = get_archive_page_html($pageNo,$objectQuery);
	}
	else
	{
		$data = get_recent_stories_page_html($pageNo);
	}

	$response = array(
		'page' => $pageNo,
		'html' => $data['html'],
		'maxPage' => $data['maxPage']
	);

	wp_send_json($response);
}

function get_recent_stories_page_html($pageNo)
{
	$_pageItemsCount = 8;
	$posts = array();

	if (!isset($pageNo))
		return $posts;

	$html = '';

	$query = new WP_Query(array(
		'posts_per_page' => $_pageItemsCount,
		'offset' => $pageNo * $_pageItemsCount
	));

	while ($query->have_posts())
	{
		$query->the_post();

		$post_date = get_the_date('F d, Y');
		$post_image = get_the_post_thumbnail_url(get_the_ID());
		$post_permalink = get_the_permalink();
		$post_title = get_the_title();

		$post_content = get_the_excerpt();

		$category = get_the_category();
		$category = $category[0];

		$categoryName = $category->name;
		$categoryColor = Taxonomy_MetaData::get('category', $category->term_id, 'category-color');

		$html .= "<article class=\"recent-stories__item tiles__tile tiles__item\" style=\"background-color: {$categoryColor}\">";
		$html .= "	<a href=\"{$post_permalink}\">";
		$html .= "	<small class=\"separated-content\"><span>{$categoryName}</span>&nbsp;<span>{$post_date}</span></small>";
		$html .= "	<div class=\"tiles__image\" style=\"background-image: url({$post_image})\">";
		$html .= "	</div>";
		$html .= "	<div class=\"tiles__content\">";
		$html .= "		<h4>{$post_title}</h4>";
		$html .= "		<p>{$post_content}</p>";
		$html .= "	</div>";
		$html .= "	</a>";
		$html .= "</article>";

	}
	wp_reset_postdata();

	return array(
		'html' => $html,
		'maxPage' => $query->max_num_pages
	);
}

function get_archive_page_html($pageNo,$objectQuery)
{
	$_pageItemsCount = 8;
	$posts = array();

	if (!isset($pageNo))
		return $posts;

	$html = '';

	$args = array(
		'posts_per_page' => $_pageItemsCount,
		'offset' => $pageNo * $_pageItemsCount
	);

	$args += $objectQuery;

	$query = new WP_Query($args);

	while ($query->have_posts())
	{
		$query->the_post();

		$post_date = get_the_date('F d, Y');
		$post_image = get_the_post_thumbnail_url(get_the_ID());
		$post_permalink = get_the_permalink();
		$post_title = get_the_title();

		$post_content = get_the_excerpt();

		$category = get_the_category();
		$category = $category[0];

		$categoryName = $category->name;
		$categoryColor = Taxonomy_MetaData::get('category', $category->term_id, 'category-color');

		$html .= "<article class=\"tiles__tile tiles__item\" style=\"background-color: {$categoryColor}\">";
		$html .= "	<a href=\"{$post_permalink}\">";
		$html .= "	<small class=\"separated-content\"><span>{$categoryName}</span>&nbsp;<span>{$post_date}</span></small>";
		$html .= "	<div class=\"tiles__image\" style=\"background-image: url({$post_image})\">";
		$html .= "	</div>";
		$html .= "	<div class=\"tiles__content\">";
		$html .= "		<h4>{$post_title}</h4>";
		$html .= "		<p>{$post_content}</p>";
		$html .= "	</div>";
		$html .= "	</a>";
		$html .= "</article>";

	}
	wp_reset_postdata();

	return array(
		'html' => $html,
		'maxPage' => $query->max_num_pages
	);
}

class ColorItem_Walker extends Walker_Nav_Menu{

   function start_el(&$output, $item, $depth, $args) {

      global $wp_query;
      $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

      $class_names = $value = '';
      $classes = empty( $item->classes ) ? array() : (array) $item->classes;
      $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
      $class_names = ' class="'. esc_attr( $class_names ) . '"';
	  $category = $item->object_id;
	  $categoryColor = Taxonomy_MetaData::get('category', $category , 'category-color');
	  $color = ' style="color:'. esc_attr( $categoryColor ) . '"';
      $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names . $color . '>';

      $attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
      $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
      $attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
      $attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';

      $item_output = $args->before;
      $item_output .= '<a'. $attributes .'>';
      $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID );
      $item_output .= '</a>';
      $item_output .= $args->after;

      $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
   }

}

function show_section($section)
{
	return in_array($section,app_get_option('app_main_sections'));
}
