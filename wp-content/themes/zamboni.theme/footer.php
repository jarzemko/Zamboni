<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package themeHandle
 */
?>

	</div><!-- #main -->

</div>
<footer class="site-footer container" role="contentinfo">
	<div class="site-footer__designby site-footer__section">
		<?php echo app_get_option('app_main_credits'); ?>
	</div>
	<div class="site-footer__menu site-footer__section">
	<?php

	$defaults = array(
		'theme_location'  => 'footer',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => '',
		'container_id'    => '',
		'menu_class'      => '',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
	);

	wp_nav_menu( $defaults );

	?>
	</div>
	<div class="social site-footer__section">
		<div class="social__wrap">
			<?php
			$social = app_get_option('app_repeat_group');

			foreach($social as $s) : ?>
			<a target="_blank" class="social__link" href="<?= $s['url'] ?>"><img class="social__icon" src="<?= $s['icon'] ?>" /> </a>
			<?php endforeach; ?>
		</div>
	</div>
</footer>
<div class="loader">
	<svg xmlns="http://www.w3.org/2000/svg" width="26.349" height="26.35" viewBox="0 0 26.349 26.35"><circle cx="13.792" cy="3.082" r="3.082"/><circle cx="13.792" cy="24.501" r="1.849"/><circle cx="6.219" cy="6.218" r="2.774"/><circle cx="21.365" cy="21.363" r="1.541"/><circle cx="3.082" cy="13.792" r="2.465"/><circle cx="24.501" cy="13.791" r="1.232"/><path d="M4.694 19.84c-.843.843-.843 2.207 0 3.05.842.843 2.208.843 3.05 0 .843-.843.843-2.207 0-3.05-.842-.844-2.207-.852-3.05 0z"/><circle cx="21.364" cy="6.218" r=".924"/></svg>
</div>
<?php wp_footer(); ?>
</body>
</html>
