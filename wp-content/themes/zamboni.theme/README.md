Wordpress Starter Theme
=======================

###Workflow
Basic workflow is based upon [http://gulpjs.com/](Gulp.js). All of the gulp dependencies are listed in "package.json"

To start project you must have installed node.js and gulp.js as a global npm package. In order to install Gulp type in your console:

`npm install gulp -g`

Next, enter your local development url in gulpfile.js, enter the console and type

`gulp watch`

Now you have automatic

    - Livereload with external address
    - Sass
      - compilation
      - autoprefixer
      - minification
      - combining media queries
    - Bower support, and scipts handling in general
    - Svg minification
    - Images minification

###SASS

#### Configuration
"config.rb" is a configuration file
#### Sass plugins
All of necessary sass plugins are listed also in config.re file


###Bower, js script
When you install bower dependencies it is important that main script file is listed in package bower.json file as `main: ['main_file']`. It is also important that main bower.json file is updated after changes are made. Typically when we install new script with:

`bower install script --save`

It will automatically start the minifaction and concatation(output file will be in assets/vendor folder as plugins.js)  

Your main js script is in dev/js. It will be proccesed after each save and output file will be in assets folder as app.js file

###SVG, Images
to do
