Modernizr.load([{
    test: Modernizr.csscolumns,
    nope: ['polyfills/css3-multi-column.min.js']
}]);

jQuery(document).ready(function ($) {

    window.GLOBALS = {
        windowW: $(window).width(),
        windowH: $(window).height(),
        scrollController: new ScrollMagic.Controller(),
        $siteHeader: $('.site-header'),
        $menu: $('.menu'),
        $loader: $('.loader'),
        scene: this.scene
    };

    var APP = {
        InitSite: function () {
            this.Layout();
            this.Nav();
            this.Sliders();
            this.RecentStories();
            this.ArchiveInfiniteScroll();
            if (Modernizr.mq('only screen and (min-width: 1024px)')) {
                this.MainSlider();
                //this.SingleParralax();
            }
        },
        Layout: function () {
            GLOBALS.$loader.height(GLOBALS.windowH);

            $.ajaxSetup({
                beforeSend: function () {
                    GLOBALS.$loader.addClass('loader--loading');
                },
                success: function () {
                    GLOBALS.$loader.removeClass('loader--loading');
                },
                error: function () {
                    GLOBALS.$loader.removeClass('loader--loading');
                }
            });
        },
        Nav: function () {
            if (Modernizr.mq('only screen and (min-width: 1024px)')) {
                new ScrollMagic.Scene({
                        offset: GLOBALS.$menu.offset().top
                    })
                    .setPin(".menu")
                    .on("start", function (e) {
                        if (e.progress == 1) {
                            GLOBALS.$menu.addClass('menu--scrolled');
                        } else {
                            GLOBALS.$menu.removeClass('menu--scrolled');
                        }
                    })
                    .addTo(GLOBALS.scrollController);
            }
            $('.site-header__mobile-btn').on('click', function () {
                GLOBALS.$menu.toggleClass('menu--expanded');
            })
        },
        Sliders: function () {
            $('.swiper-container').each(function () {
                var mySwiper = new Swiper($(this)[0], {
                    //pagination: $(this).find('.swiper-pagination')[0],
                    nextButton: $(this).find('.swiper-button-next')[0],
                    prevButton: $(this).find('.swiper-button-prev')[0],
                    slidesPerView: 'auto',
                    paginationClickable: true,
                    spaceBetween: 0
                });
            });
        },
        MainSlider: function () {
            $('.main-slider__item').each(function () {
                var $content = $(this).find('.main-slider__content'),
                    titleHeight = $content.find('.main-slider__content-title').outerHeight();

                var translateVal = $content.innerHeight() - titleHeight + 1;

                $content.css({
                    "-webkit-transform": "translateY(" + translateVal + "px)",
                    "-ms-transform": "translateY(" + translateVal + "px)",
                    "-moz-transform": "translateY(" + translateVal + "px)",
                    "transform": "translateY(" + translateVal + "px)"
                });

                $(this).on('mouseenter', function () {
                    $content.css({
                        "-webkit-transform": "translateY(0px)",
                        "-ms-transform": "translateY(0px)",
                        "-moz-transform": "translateY(0px)",
                        "transform": "translateY(0px)"
                    });
                });
                $(this).on('mouseleave', function () {
                    $content.css({
                        "-webkit-transform": "translateY(" + translateVal + "px)",
                        "-ms-transform": "translateY(" + translateVal + "px)",
                        "-moz-transform": "translateY(" + translateVal + "px)",
                        "transform": "translateY(" + translateVal + "px)"
                    });
                });
            });
        },
        RecentStories: function () {
            var _this = this;
            _this.recent_stories_page_count = 0;

            $('.recent-stories .loadmore').on('click', function (event) {
                _this.recent_stories_page_count += 1;
                var $this = $(this);
                $.get(
                    wp.ajaxurl, {
                        'action': 'ajax_load_more_stories',
                        'pageNo': _this.recent_stories_page_count
                    },
                    function (e) {
                        if (e.html) {
                            $('.recent-stories__items').append(e.html);
                        }

                        if(_this.recent_stories_page_count + 1 >= e.maxPage)
                        {
                            $this.hide();
                        }
                        GLOBALS.$loader.removeClass('loader--loading');
                    }
                );

                event.preventDefault();
            });
        },
        SingleParralax: function () {
            var $coverImageContainer = $('.single__cover-image'),
                coverImageContainerHeight = $coverImageContainer.height(),
                $coverImage = $coverImageContainer.children('img'),
                coverImageHeight = $coverImage.height();

            if ($coverImageContainer.length) {
                new ScrollMagic.Scene({
                        offset: $coverImageContainer.offset().top,
                        duration: $coverImageContainer.height()
                    })
                    .on("progress", function (e) {
                        var val = (coverImageHeight - coverImageContainerHeight) * e.progress;

                        $coverImage.css({
                            "-webkit-transform": "translate3d(0,-" + val + "px,0)",
                            "-ms-transform": "translate3d(0,-" + val + "px,0)",
                            "-moz-transform": "translate3d(0,-" + val + "px,0)",
                            "transform": "translate3d(0,-" + val + "px,0)"
                        })
                    })
                    .addTo(GLOBALS.scrollController);
            }

        },
        ArchiveInfiniteScroll: function () {
            var $archiveContainer = $('.archive__section');

            if ($archiveContainer.length) {
                GLOBALS.scene = new ScrollMagic.Scene({
                        triggerElement: ".loadmore",
                        triggerHook: "onEnter"
                    })
                    .addTo(GLOBALS.scrollController)
                    .on("enter", function (e) {
                        $('.loadmore').children('a').trigger('click');
                    });
            }
        }

    };

    APP.InitSite();
});

function loadMorePosts(el) {
    var objectQueryKey = $(el).data('object-query-key'),
        objectQueryValue = $(el).data('object-query-value'),
        nextPage = $(el).data('next-page');

    $.get(
        wp.ajaxurl, {
            'action': 'ajax_load_more_stories',
            'pageNo': nextPage,
            'objectQueryKey': objectQueryKey,
            'objectQueryValue': objectQueryValue
        },
        function (e) {
            if (e.html) {
                $('.archive__section .container__wrap').append(e.html);
            }
            GLOBALS.$loader.removeClass('loader--loading');

            if(nextPage + 1 >= e.maxPage)
            {
                GLOBALS.scene.destroy();
                $(el).remove();
            }
            else
            {
                $(el).data('next-page', nextPage + 1);
            }
        }
    );
}
