<?php
/**
 * Twitter Section
 *
 * @package Zamboni
 */
?>

<?php if(show_section('twitter')) : ?>
<section class="twitter container">
    <header class="container__title">
        <h3>Twitter</h3>
    </header>
    <div class="twitter__wrap swiper-container">
        <div class="swiper-wrapper">
            <!-- Slides -->

			<?php
			$tweets = get_tweets();

			foreach($tweets as $tweet) :
			?>
            <div class="twitter__slide swiper-slide">
                <p><?= $tweet->text ?></p>
            </div>
			<?php endforeach; ?>
        </div>

        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</section>
<?php endif; ?>