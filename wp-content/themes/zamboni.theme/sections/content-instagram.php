<?php
/**
 * Galleries Section
 *
 * @package Zamboni
 */
?>

<?php if(show_section('instagram')) : ?>
<section class="instagram container container--gray">
    <div class="container__wrap">
        <header class="container__title">
            <h3>Instagram</h3>
        </header>
        <div class="instagram__items tiles tiles--6">
            <?php
    		$instagram = get_instagram();

    		foreach($instagram->data as $i) :
    		?>
            <div class="instagram__item tiles__tile">
                <a href="<?= $i->link ?>" target="_blank" title="<?= $i->caption->text ?>">
                    <img src="<?= $i->images->low_resolution->url ?>" />
                </a>
            </div>
    		<?php
    			endforeach;
    			wp_reset_query();
    		?>
        </div>
    </div>
</section>
<?php endif; ?>
