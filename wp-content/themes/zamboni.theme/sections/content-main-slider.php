<?php
/**
 * Slider Section
 *
 * @package Zamboni
 */
?>

<section class="main-slider swiper-container container">
	<div class="main-slider__wrap swiper-wrapper">
		<!-- Slides -->
		<?php

		//$slider = Taxonomy_MetaData::get('slider', $category->term_id, 'category-color');

		$query = new WP_Query( array( 'slider' => 'yes' ) );
		while ($query->have_posts()) :
			$query->the_post();

			$category = get_the_category();

			//TODO check for other categories if 0 index is enough
			$category = $category[0];

			$categoryName = $category->name;
			$categoryUrl = get_category_link($category->cat_ID);
			?>
			<figure class="main-slider__item swiper-slide">
				<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('slider-image') ?></a>
				<figcaption class="main-slider__content">
					<div class="main-slider__content-title">
						<h4><?php the_title() ?></h4>
						<small class="separated-content">
							<span><a href="<?= esc_url($categoryUrl) ?>"><?= $categoryName ?></a></span>
							<span><?= get_the_date('F d, Y') ?></span>
						</small>
					</div>
					<div class="main-slider__excerpt">
						<?php the_excerpt() ?>
					</div>
				</figcaption>
			</figure>
		<?php
		endwhile;
		wp_reset_postdata();
		?>
	</div>

	<!-- If we need navigation buttons -->
	<div class="swiper-button-prev"></div>
	<div class="swiper-button-next"></div>
</section>
