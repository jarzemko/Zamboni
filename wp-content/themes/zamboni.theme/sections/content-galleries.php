<?php
/**
 * Galleries Section
 *
 * @package Zamboni
 */
?>

<?php if(show_section('galleries')) : ?>
<section class="galleries container">
    <header class="container__title">
        <h3>Galleries</h3>
    </header>
    <div class="galleries__wrap swiper-container">
        <div class="swiper-wrapper">
            <!-- Slides -->
			<?php
				$query = new WP_Query('post_type=gallery-slider-post');

				while($query->have_posts()) :
				$query->the_post();
			?>
            <div class="galleries__slide swiper-slide">
               <?php the_post_thumbnail('gallery-slider-image') ?>
                <div class="galleries__title">
                    <h4><?php the_title() ?></h4>
                </div>
            </div>
			<?php
				endwhile;
				wp_reset_query();
			?>
        </div>
        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</section>
<?php endif; ?>