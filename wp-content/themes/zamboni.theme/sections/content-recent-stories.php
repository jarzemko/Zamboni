<?php
/**
 * Contact Section
 *
 * @package Jarzemko
 */
?>

<section class="recent-stories container">
	<div class="container__wrap">
		<header class="container__title">
			<?php
			$query = new WP_Query('pagename=recent-stories');

			while ($query->have_posts()) :
				$query->the_post();
				?>
				<h3><?php the_title() ?></h3>
			<?php
			endwhile;
			wp_reset_query();
			?>
		</header>

		<div class="recent-stories__items tiles tiles--4">
			<?php
			$data = get_recent_stories_page_html(0);
			echo $data['html'];
			?>
		</div>

		<div class="loadmore">
			<a href="#">Load more</a>
		</div>
	</div>
</section>
