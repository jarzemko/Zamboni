<?php
/**
 * Categories section
 * @package Zamboni.theme
 */
?>
<section class="categories container container--gray">
	<div class="container__wrap">
		<header class="container__title">
			<?php
			$categoriesPageID = '';

			$query = new WP_Query("pagename=categories");

			while ($query->have_posts()) :
				$query->the_post();

				$categoriesPageID = get_the_ID();
				?>
				<h3><?php the_title() ?></h3>
			<?php
			endwhile;
			wp_reset_postdata();
			?>
		</header>
		<div class="categories__items tiles tiles--4">
			<?php
			$featuredCategories = get_post_meta($categoriesPageID, 'featured-categories');

			$featuredCategories = $featuredCategories[0];

			foreach ($featuredCategories as $featuredCategory) :

				$query = new WP_Query(sprintf('cat=%s&showposts=2', $featuredCategory));
				$categoryColor = Taxonomy_MetaData::get('category', $featuredCategory , 'category-color');

				echo '<div class="tiles__tile">';
				echo '<small class="tiles__title separated-content" style="background-color:' . $categoryColor . '">' . get_the_category_by_ID( $featuredCategory ) . '</small>';
				while ($query->have_posts()) :
					$query->the_post();

					$postID = get_the_ID();

					$post_image = get_the_post_thumbnail_url(get_the_ID());
					$post_permalink = get_permalink($postID );

					$category = wp_get_post_categories($postID , array('fields' => 'all'));
					$category = $category[0];

					$categoryName = $category->name;
					?>
					<article class="categories__item tiles__item" style="background-color: <?= $categoryColor ?>">
						<a href="<?= $post_permalink ?>">
							<div class="tiles__image" style="background-image: url(<?= $post_image ?>);">

							</div>
							<div class="tiles__content">
								<h4><?php the_title() ?></h4>

								<p><?php the_excerpt() ?></p>
							</div>
						</a>
					</article>
				<?php
				endwhile;

				echo '</div>';
				wp_reset_postdata();
			endforeach;
			?>

		</div>
	</div>
</section>
