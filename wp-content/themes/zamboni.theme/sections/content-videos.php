<?php
/**
 * Videos Section
 *
 * @package Zamboni
 */
?>

<?php if(show_section('videos')) : ?>
<section class="videos container">
    <header class="container__title">
        <h3>Videos</h3>
    </header>
    <div class="videos__wrap swiper-container">
        <div class="swiper-wrapper">
            <!-- Slides -->
			<?php
				$query = new WP_Query('post_type=video-slider-post');

			while($query->have_posts()) :
			$query->the_post();
			?>
            <div class="videos__slide swiper-slide"><?php the_content() ?></div>
			<?php
			endwhile;
			wp_reset_query();
			?>
        </div>

        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</section>
<?php endif; ?>