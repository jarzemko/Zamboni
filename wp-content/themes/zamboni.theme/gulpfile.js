var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    compass = require('gulp-compass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    newer = require('gulp-newer'),
    imagemin = require('gulp-imagemin'),
    lr = require('tiny-lr'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    cmq = require('gulp-combine-media-queries'),
    webp = require('gulp-webp'),
    svgstore = require('gulp-svgstore'),
    svgmin = require('gulp-svgmin'),
    notify = require('gulp-notify'),
    mainBowerFiles = require('main-bower-files'),
    size = require('gulp-size');

var imgSrc = 'assets/images/originals*';
var imgDest = 'assets/images';

gulp.task('browser-sync', function() {
    browserSync({
        proxy: "zamboni"
    });
});


gulp.task('styles', function() {
    var s = size();
    return gulp.src('dev/scss/style.scss')
        .pipe(plumber({errorHandler: notify.onError("Błąd: <%= error.message %>")}))
        .pipe(compass({
            config_file: 'config.rb',
            css: '',
            sass: 'dev/scss',
            debug: false,
            sourcemap: true
        }))
        .pipe(cmq({
            log: true
        }))
        .pipe(autoprefixer('last 4 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(minifycss())
        .pipe(s)
        .pipe(gulp.dest(''))
        .pipe(notify({
           title: '<%= file.relative %>',
           message: function () {
               return 'Proccesed! Total size: ' + s.prettySize;
           },
           sound: false
        }))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('images', function() {
    var s = size();
    return gulp.src('assets/images/originals/**')
        .pipe(newer('assets/images'))
        .pipe(plumber({errorHandler: notify.onError("Błąd: <%= error.message %>")}))
        .pipe(imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest('assets/images'))
        .pipe(notify({
           title: 'Images',
           message: 'Compressed: <%= file.relative %>!',
           sound: false
        }))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('svg', function() {
    var s = size();
    return gulp.src('assets/svg/originals/**')
        .pipe(newer('assets/svg'))
        .pipe(plumber({errorHandler: notify.onError("Błąd: <%= error.message %>")}))
        .pipe(svgmin())
        .pipe(svgstore({
            fileName: 'icons.svg',
            prefix: 'icon-',
            inlineSvg: true
        }))
        .pipe(gulp.dest('assets/svg'))
        .pipe(notify({
           title: 'Svg',
           message: 'Processed: <%= file.relative %>!',
           sound: false
        }))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('vendor-scripts', function() {
    var s = size();
    return gulp.src(mainBowerFiles())
        .pipe(plumber({errorHandler: notify.onError("Błąd: <%= error.message %>")}))
        .pipe(concat('plugins.js'))
        .pipe(uglify({
            mangle: false
        }))
        .pipe(s)
        .pipe(gulp.dest('assets/vendor'))
        .pipe(notify({
            title: '<%= file.relative %>',
            message: function () {
                return 'Proccesed! Total size: ' + s.prettySize;
            },
           sound: false
        }))
        .pipe(reload({
            stream: true
        }));
});
gulp.task('theme-script', function() {
    var s = size();
    return gulp.src(['dev/js/*.js'], {
            base: 'dev/js/'
        })
        .pipe(plumber({errorHandler: notify.onError("Błąd: <%= error.message %>")}))
        .pipe(concat('app.js'))
        //.pipe(uglify())
        .pipe(s)
        .pipe(gulp.dest('assets/'))
        .pipe(notify({
           title: 'Script <%= file.relative %>',
           message: function () {
               return 'Proccesed! Total size: ' + s.prettySize;
           },
           sound: false
        }))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('default', ['styles', 'images', 'svg', 'vendor-scripts', 'theme-script']);

gulp.task('watch', ['browser-sync'], function() {
    gulp.watch('dev/scss/**', ['styles']);
    gulp.watch('assets/images/originals/**', ['images']);
    gulp.watch('bower.json', ['vendor-scripts']);
    gulp.watch('dev/js/*.js', ['theme-script']);
    gulp.watch('assets/svg/originals/**', ['svg']);
    gulp.watch('**/*.php', browserSync.reload);

});
