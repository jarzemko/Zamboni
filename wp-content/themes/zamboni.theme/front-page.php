<?php
/**
 * Home Page
 *
 * @package themeHandle
 */
get_header();

get_template_part( 'sections/content-main-slider');
get_template_part( 'sections/content-recent-stories');
get_template_part( 'sections/content-categories');
get_template_part( 'sections/content-videos');
get_template_part( 'sections/content-galleries');
get_template_part( 'sections/content-instagram');
get_template_part( 'sections/content-twitter');
?>

<?php get_footer(); ?>
