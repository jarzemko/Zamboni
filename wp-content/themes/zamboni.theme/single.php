<?php
/**
 * Single post template
 *
 * @package themeHandle
 */

get_header(); ?>

<section class='single__section container'>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php $postID = get_the_ID();

		$coverPhoto =  get_post_meta( $postID, 'cover-photo_id', 1 );
		if(!empty($coverPhoto) || !isset($coverPhoto)) {
			$titlePosition = get_post_meta($postID, 'title_position', true);
			$titleColor = get_post_meta($postID, 'title_color', true);

			if($titlePosition == "afterCover" || empty($titlePosition)) {
				$html .= "<div class=\"single__cover-image\">";
				$html .= wp_get_attachment_image( get_post_meta( $postID, 'cover-photo_id', 1 ), 'cover-image' );
				$html .= "</div>";
				$html .= "<article class=\"single__wrap\">";
				$html .= "<header class=\"single__head\">";
				$html .= "<h1 style=\"color:" . $titleColor . "\" class=\"single__main-title\">" . get_the_title() . "</h1>";

				echo $html;

			} else if($titlePosition === "onCover") {
				$html .= "<div class=\"single__cover-image\">";
				$html .= wp_get_attachment_image( get_post_meta( $postID, 'cover-photo_id', 1 ), 'cover-image' );
				$html .= "<h1 style=\"color:" . $titleColor . "\" class=\"single__main-title single__main-title--oncover\">" . get_the_title() . "</h1>";
				$html .= "</div>";
				$html .= "<article class=\"single__wrap\">";
				$html .= "<header class=\"single__head\">";
				echo $html;
			}

		} else {
			$html .= "<article class=\"single__wrap\">";
			$html .= "<header class=\"single__head\">";
			$html .= "<h1 style=\"color:" . $titleColor . "\" class=\"single__main-title\">" . get_the_title() . "</h1>";

			echo $html;
		}

			$category = get_the_category();
			$categoryColor = Taxonomy_MetaData::get('category', $category[0]->term_id, 'category-color');
		?>
			<small class="single__meta separated-content" style="background-color:<?php echo $categoryColor ?>">
				<span><?php echo $category[0]->name ?></span>
				<span><?php the_date( 'F j, Y'); ?></span>
			</small>
			<div class="row">
				<?php
					$authorAlias = get_post_meta($postID, 'author-alias', true);
					$fakeAuthorLink = "<a href='" . get_author_posts_url( get_the_author_meta( 'ID' ) ) . "'>" . $authorAlias . "</a>";
					$author = !empty($authorAlias) ? $fakeAuthorLink : get_the_author_link();

					echo $author;
				?>
			</div>
			<hr>
		</header>
		<?php the_content() ?>
	</article>
	<?php endwhile; ?>
</section>
<section class="related-articles container">
	<div class="container__wrap">
		<header class="container__title">
			<h3>Related articles</h3>
		</header>

		<div class="related-articles__items tiles tiles--4">
			<?php
				$query = new WP_Query(array(
					'post__not_in' => array($postID),
					'cat' => $category[0]->cat_ID,
					'posts_per_page' => 4
				));

			while($query->have_posts()) :
				$query->the_post();
			?>
			<article class="related-articles__item tiles__tile tiles__item" style="background-color: <?php echo $categoryColor ?>">
				<a href="<?php the_permalink() ?>">
					<small class="separated-content"><span><?php echo $category[0]->name ?></span>&nbsp;<span><?php echo get_the_date('F d, Y'); ?></span></small>
					<div class="tiles__image" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>)"></div>
					<div class="tiles__content">
						<h4><?php echo the_title(); ?></h4>
						<p><?php echo get_the_excerpt(); ?></p>
					</div>
				</a>
			</article>
			<?php
			endwhile;
			wp_reset_postdata();
			?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
